/*
 * rc-connection.h
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RC_CONN_H__
#define __RC_CONN_H__

#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

enum {
  RC_FORMAT_INVALID = 0x00,
  RC_FORMAT_JPEG    = 0x01,
  RC_FORMAT_PNG     = 0x02,
  RC_FORMAT_TIFF    = 0x03,
  RC_FORMAT_HEIF    = 0x04
};

/* type declaration */
#define RC_TYPE_CONN rc_conn_get_type()
G_DECLARE_FINAL_TYPE(RcConn, rc_conn, RC, CONN, GObject);

/* method declaration */
RcConn *rc_conn_new(void);

void rc_conn_connect_async(RcConn *self, const gchar *server, const gchar *name,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback, gpointer user_data);
gboolean rc_conn_connect_finish(RcConn *self, GAsyncResult *result,
                                GError **error);

guint8 rc_conn_get_supported_formats(RcConn *self, guint8 **formats);

void rc_conn_capture_async(RcConn *self, guint8 format,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback, gpointer user_data);
gssize rc_conn_capture_finish(RcConn *self, GAsyncResult *result, guint32 *id,
                              GError **error);

void rc_conn_get_async(RcConn *self, guint32 id, guint64 offset, guint32 length,
                  gchar *buffer, GCancellable *cancellable,
                  GAsyncReadyCallback callback, gpointer user_data);
gssize rc_conn_get_finish(RcConn *self, GAsyncResult *result, GError **error);

void rc_conn_delete(RcConn *self, guint32 id);

void rc_conn_shutdown_async(RcConn *self, GCancellable *cancellable,
                            GAsyncReadyCallback callback, gpointer user_data);
void rc_conn_shutdown_finish(RcConn *self, GAsyncResult *result,
                             GError **error);

void rc_conn_close(RcConn *self, GError *error);


/* errors */
#define RC_CONN_ERROR rc_conn_error_quark()
GQuark rc_conn_error_quark(void);
typedef enum {
  RC_CONN_ERROR_BAD_IMPLEMENTATION,
  RC_CONN_ERROR_ALREADY_OPEN,
  RC_CONN_ERROR_CONNECTION_CLOSED,
  RC_CONN_ERROR_VERSION_UNSUPPORTED,
  RC_CONN_ERROR_NOT_ACCEPTED,
  RC_CONN_ERROR_CAPTURE_PENDING,
  RC_CONN_ERROR_CAPTURE_FAILED,
  RC_CONN_ERROR_RESOURCE_UNAVAILABLE
} RcConnError;


G_END_DECLS

#endif /* !__RC_CONN_H__ */

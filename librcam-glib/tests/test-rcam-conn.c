/*
 * test-rcam-conn.c
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <rc-conn.h>
#include <stdio.h>
#include <gio/gunixinputstream.h>

#define DOWNLOAD_THREADS 2


static void term_read_cb(GObject *source_object, GAsyncResult *res,
                         gpointer user_data);
static void connected_cb(GObject *source_object, GAsyncResult *res,
                         gpointer user_data);
static void capture_cb(GObject *source_object, GAsyncResult *res,
                       gpointer user_data);
static void try_start_dl(void);
static void file_open_cb(GObject *source_object, GAsyncResult *res,
                         gpointer user_data);
static void get_cb(GObject *source_object, GAsyncResult *res,
                   gpointer user_data);
static void file_write_cb(GObject *source_object, GAsyncResult *res,
                          gpointer user_data);
static void file_close_cb(GObject *source_object, GAsyncResult *res,
                          gpointer user_data);

gchar supported_formats[] = { /* first is preferred */
  RC_FORMAT_PNG,
  RC_FORMAT_TIFF,
  RC_FORMAT_JPEG,
  RC_FORMAT_INVALID /* last is default is invalid */
};

gchar *format_extensions[] = {
  "", /* invalid */
  "jpg",
  "png",
  "tif",
  "heif"
};

typedef struct {
  guint32 id;
  guint64 size;
} ServerResource;

typedef struct {
  gboolean used;
  guint32 id;
  guint64 size;
  guint64 offset;
  guint32 chunk_len;
  guint32 chunk_offset;
  gchar buf[512 * 1024];
  GFile *file;
  GOutputStream *ostream;
} Download;


RcConn *conn;

guint8 capture_format;
guint32 save_id = 0;

GQueue *dl_queue;
Download dls[DOWNLOAD_THREADS];

GInputStream *term;
gchar term_buf[80];


void connected_cb(GObject *source_object, GAsyncResult *res,
                  gpointer user_data) {
  guint8 srv_fmts_count;
  guint8 *srv_fmts;
  guint8 best_format; /* index of #supported_formats */
  GError *err = NULL;

  if (!rc_conn_connect_finish(conn, res, &err)) {
    fprintf(stderr, "Connecting to server failed: %s\n", err->message);
    g_error_free(err);
    exit(EXIT_FAILURE);
    return;
  }

  /* preselect “invalid”, lowest rank */
  best_format = sizeof (supported_formats) - 1;
  /* get all available formats – do not free the result */
  srv_fmts_count = rc_conn_get_supported_formats(conn, &srv_fmts);
  /* iterate over all the server supported formats */
  srv_fmts_count--; /* use this as index */
  while (TRUE) {
    /* iterate over all the client supported formats, highest rank (= lowest
     * value) to lowest rank (= highest value), ignore formats worse than the
     * current best */
    for (guint i = 0; i < best_format; i++) {
      if (srv_fmts[srv_fmts_count] == supported_formats[i]) {
        best_format = i;
        break;
      }
    }
    if (srv_fmts_count == 0)
      break;
    srv_fmts_count--;
  }
  capture_format = supported_formats[best_format];
  printf("Chosen format: %s\n", format_extensions[capture_format]);

  /* init download queue */
  dl_queue = g_queue_new();
  for (guint i = 0; i < DOWNLOAD_THREADS; i++)
    dls[i].used = FALSE;

  puts("Connection accepted! You can take a photo by typing ENTER. If you want \
to close the connection, type CTRL-D.");
  term = g_unix_input_stream_new(STDIN_FILENO, TRUE);
  g_input_stream_read_async(term, term_buf, 80, G_PRIORITY_DEFAULT, NULL,
                            term_read_cb, NULL);

}

void capture_cb(GObject *source_object, GAsyncResult *res, gpointer user_data) {
  GError *err = NULL;
  ServerResource *sr = g_slice_new(ServerResource);

  sr->size = rc_conn_capture_finish(conn, res, &sr->id, &err);
  if (err) {
    fprintf(stderr, "Could not capture: %s\n", err->message);
    g_error_free(err);
    g_slice_free(ServerResource, sr);
    return;
  }

  /* If the server does not behave correctly and returns the same id for several
   * captures, the downloads are likely to be corrupt.  In that case the images
   * are likely to be corrupt, too, so a check that an id is only used once
   * would not help much. */

  g_queue_push_tail(dl_queue, sr);
  try_start_dl();
}

void try_start_dl(void) {
  for (guint i = 0; i < DOWNLOAD_THREADS; i++) {
    if (dls[i].used == FALSE) {
      ServerResource *sr;
      gchar fname[14]; /* XXXXXXXX.xxxx\0 */

      if ((sr = g_queue_pop_head(dl_queue)) == NULL)
        return;

      dls[i].used = TRUE;
      dls[i].id = sr->id;
      dls[i].size = sr->size;
      dls[i].offset = 0;
      sprintf(fname, "%08x.%s", save_id++, format_extensions[capture_format]);
      dls[i].file = g_file_new_for_path(fname);
      g_file_replace_async(dls[i].file, NULL, FALSE, G_FILE_CREATE_NONE,
                           G_PRIORITY_DEFAULT, NULL, file_open_cb, &dls[i]);
      g_slice_free(ServerResource, sr);
    }
  }
}

void file_open_cb(GObject *source_object, GAsyncResult *res,
                  gpointer user_data) {
  Download *dl = user_data;
  GError *err = NULL;

  dl->ostream = G_OUTPUT_STREAM(g_file_replace_finish(dl->file, res, &err));
  g_object_unref(dl->file);
  if (err) {
    fprintf(stderr, "Could not open “%s”: %s\n", g_file_peek_path(dl->file),
            err->message);
    g_error_free(err);
    dl->used = FALSE;
    try_start_dl();
    return;
  }

  rc_conn_get_async(conn, dl->id, 0, sizeof (dl->buf), dl->buf, NULL, get_cb,
                    dl);
}

void get_cb(GObject *source_object, GAsyncResult *res, gpointer user_data) {
  Download *dl = user_data;
  GError *err = NULL;

  dl->chunk_len = rc_conn_get_finish(conn, res, &err);
  if (err) {
    fprintf(stderr, "Could not download resource 0x%08x: %s\n", dl->id,
            err->message);
    g_error_free(err);
    g_output_stream_close_async(dl->ostream, G_PRIORITY_DEFAULT, NULL,
                                file_close_cb, dl);
    return;
  }

  dl->chunk_offset = 0;
  g_output_stream_write_async(dl->ostream, dl->buf, dl->chunk_len,
                              G_PRIORITY_DEFAULT, NULL, file_write_cb, dl);
}

void file_write_cb(GObject *source_object, GAsyncResult *res,
                   gpointer user_data) {
  Download *dl = user_data;
  GError *err = NULL;

  dl->chunk_offset += g_output_stream_write_finish(dl->ostream, res, &err);
  if (err) {
    fprintf(stderr, "Could not write to file for resource 0x%08x: %s\n", dl->id,
            err->message);
    g_error_free(err);
    g_output_stream_close_async(dl->ostream, G_PRIORITY_DEFAULT, NULL,
                                file_close_cb, dl);
    return;
  }

  if (dl->chunk_offset < dl->chunk_len) { /* chunk was not completely written */
    g_output_stream_write_async(dl->ostream, dl->buf,
                                dl->chunk_len - dl->chunk_offset,
                                G_PRIORITY_DEFAULT, NULL, file_write_cb, dl);
    return;
  }

  dl->offset += dl->chunk_len; /* should be ok, since we request the chunks in
                                * order */
  if (dl->offset < dl->size) { /* get the next chunk */
    rc_conn_get_async(conn, dl->id, dl->offset, sizeof (dl->buf), dl->buf, NULL,
                      get_cb, dl);
  } else { /* finished */
    g_output_stream_close_async(dl->ostream, G_PRIORITY_DEFAULT, NULL,
                                file_close_cb, dl);
  }
}

void file_close_cb(GObject *source_object, GAsyncResult *res,
                   gpointer user_data) {
  Download *dl = user_data;
  GError *err = NULL;

  if (!g_output_stream_close_finish(dl->ostream, res, &err)) {
    fprintf(stderr, "Could not close file for resource 0x%08x: %s\n", dl->id,
            err->message);
    g_error_free(err);
    /* errors actually do not matter anymore -> no return */
  }

  rc_conn_delete(conn, dl->id);

  g_object_unref(dl->ostream);
  dl->used = FALSE;
  try_start_dl();
}

void term_read_cb(GObject *source_object, GAsyncResult *res,
                  gpointer user_data) {
  if (g_input_stream_read_finish(term, res, NULL) > 0) {
    rc_conn_capture_async(conn, capture_format, NULL, capture_cb, NULL);
    g_input_stream_read_async(term, term_buf, 80, G_PRIORITY_DEFAULT, NULL,
                              term_read_cb, NULL);
  } else {
    rc_conn_close(conn, NULL); // TODO: replace this with the async variant
    exit(EXIT_SUCCESS);
  }
}

int main(int argc, char *argv[]) {
  GMainLoop *loop;
  gchar *client_name = NULL;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s <hostname> [<client name>]\n", argv[0]);
    return EXIT_FAILURE;
  }
  if (argc > 2)
    client_name = argv[2];

  conn = rc_conn_new();
  rc_conn_connect_async(conn, argv[1], client_name, NULL, connected_cb, NULL);

  loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(loop);

  return EXIT_SUCCESS;
}

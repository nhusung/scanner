/*
 * rc-conn.c
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <rc-conn.h>
#include <stdio.h> // debug

#define SRVPORT 1708
#define RCAMVER 2

/**
 * SECTION:rc-conn
 * @title: rCam connections
 * @short_description: client and server side implementation of the rCam
 *                     protocol
 * @stability: Unstable
 *
 * The detailed description is not finished by now.
 */

/**
 * RC_CONN_ERROR:
 *
 * The error domain for rCam connections
 **/
G_DEFINE_QUARK(rc-conn-error-quark, rc_conn_error);

enum {
  _CODE_NONE                 = 0x00,
  _CODE_VERSION              = 0x01,
  _CODE_SEND_NAME            = 0x10,
  _CODE_SUPPORTED_FORMATS    = 0x11,
  _CODE_ACCEPT               = 0x1f,
  _CODE_CAPTURE              = 0x20,
  _CODE_CAPTURE_FINISHED     = 0x2e,
  _CODE_CAPTURE_FAILED       = 0x2f,
  _CODE_GET                  = 0x30,
  _CODE_SEND                 = 0x31,
  _CODE_DELETE_RESOURCE      = 0x32,
  _CODE_RESOURCE_UNAVAILABLE = 0x3f
};

const gchar _version_cmd[2] = {_CODE_VERSION, RCAMVER};

/*
 * _GetTaskData:
 * @dest:   pointer to the current write position in the buffer
 * @id:     resource identifier
 * @offset: offset of the chunk being downloaded
 * @length: length of the chunk being downloaded
 *
 * A struct for the data of a GET task
 */
typedef struct {
  gchar *dest;
  guint32 id;
  guint64 offset;
  guint32 length;
} _GetTaskData;


static void rc_conn_class_init(RcConnClass *klass);
static void rc_conn_init(RcConn *self);
static void rc_conn_dispose(GObject *gobject);
static void rc_conn_finalize(GObject *gobject);

/*
 * _close_ne:
 * @self: an #RcConn
 * @code: an #RcConnError
 * @...:  a string with format characters followed by a list of values to insert
 *        into it
 *
 * Shorthand macro for closing a connection with a new connection error
 */
#define _close_ne(conn, code, ...)                                    \
  rc_conn_close(conn, g_error_new(RC_CONN_ERROR, code, __VA_ARGS__))
/*
 * _close_nel:
 * @self: an #RcConn
 * @code: an #RcConnError
 * @msg:  error message
 *
 * Shorthand macro for closing a connection with a new connection error literal
 */
#define _close_nel(conn, code, msg)                                   \
  rc_conn_close(conn, g_error_new_literal(RC_CONN_ERROR, code, msg))

static void _tcp_conn_estab_cb(GObject *source_object, GAsyncResult *res,
                               gpointer user_data);
static void _send_ver_cb(GObject *source_object, GAsyncResult *res,
                         gpointer user_data);
static void _output_at_idle(RcConn *self);
static void _output_cb(GObject *source_object, GAsyncResult *res,
                       gpointer user_data);
static void _input_cb(GObject *source_object, GAsyncResult *res,
                      gpointer user_data);
static void _data_cb(GObject *source_object, GAsyncResult *res,
                     gpointer user_data);
//static void _conn_close_cb(GObject *source_object, GAsyncResult *res,
//                           gpointer user_data);
static void _get_task_data_free(gpointer mem);

/**
 * RcConn:
 *
 * Class for rCam connections
 */
struct _RcConn {
  GObject parent_instance;

  /* private(set) data */
  guint8 supported_formats_len;
  guint8 *supported_formats;

  /* private data */
  GSocketConnection *sock_conn;
  GInputStream *istream;
  GOutputStream *ostream;

  gpointer server; /* %NULL for client mode, TODO: should be RcServer */
  guint8 conn_version;

  GTask *conn_task;
  GQueue *oqueue;
  GTask *current_out_task;
  GTask *capture_task;
  GSList *get_tasks;
  GTask *current_get_task;
  GSList *deletions;
  GTask *shutdown_task;

  guint8 current_cmd;
  gchar recv_buf[128];
  gchar arg_buf[16];
  guint arg_bytes_to_read;
  guint32 data_bytes_to_read;
  gchar send_buf[17]; /* only for tasks after connection establishment */
  gssize send_buf_written;
  gssize send_buf_len;
};

G_DEFINE_TYPE(RcConn, rc_conn, G_TYPE_OBJECT);

void rc_conn_class_init(RcConnClass *klass) {
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->dispose = rc_conn_dispose;
  object_class->finalize = rc_conn_finalize;

  // TODO: properties?
}

/**
 * rc_conn_new:
 *
 * Returns: a new #RcConn. Free with g_object_unref().
 */
RcConn *rc_conn_new(void) {
  return g_object_new(RC_TYPE_CONN, NULL);
}

void rc_conn_init(RcConn *self) {
  self->sock_conn = NULL;
  self->istream = NULL;
  self->ostream = NULL;

  self->supported_formats = NULL;
  self->supported_formats_len = 0;
  /* other information about the connection is set in rc_connect_async() */

  self->conn_task = NULL;
  self->oqueue = g_queue_new();
  self->current_out_task = NULL;
  self->capture_task = NULL;
  self->get_tasks = NULL;
  /* @self->current_get_task is set at SEND */
  self->deletions = NULL;
  self->shutdown_task = NULL;
}

void rc_conn_dispose(GObject *gobject) {
  RcConn *self = RC_CONN(gobject);

  /* unreferencing happens in rc_conn_close() */
  if (self->sock_conn || self->conn_task)
    rc_conn_close(self, NULL);

  G_OBJECT_CLASS(rc_conn_parent_class)->dispose(gobject);
}
void rc_conn_finalize(GObject *gobject) {
  RcConn *self = RC_CONN(gobject);

  g_queue_free(self->oqueue);

  G_OBJECT_CLASS(rc_conn_parent_class)->finalize(gobject);
}


/**
 * rc_conn_connect_async:
 * @self:                    connection object to perform the operation on
 * @server:                  server name/ip and optionally the port
 * @name: (nullable):        client name
 * @cancellable: (nullable): optional #GCancellable object, %NULL to ignore
 * @callback: (scope async): a #GAsyncReadyCallback
 * @user_data: (closure):    user data passed to callback
 *
 * Asynchronously connect to an rCam server.  When the operation is finished,
 * @callback will be called.  You should call rc_conn_connect_finish() to get
 * the results.
 *
 * Note that @self has to be an initialized but not open #RcConn, else
 * @RC_CONN_ERROR_ALREDY_OPEN will be emitted. @server and @name have to stay
 * valid until the task finishes.
 */
void rc_conn_connect_async(RcConn *self, const gchar *server, const gchar *name,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback, gpointer user_data) {
  GSocketClient *client;

  g_return_if_fail(RC_IS_CONN(self));

  if (self->sock_conn || self->conn_task) {
    g_task_report_new_error(self, callback, user_data, NULL, RC_CONN_ERROR,
                            RC_CONN_ERROR_ALREADY_OPEN, "The connection is \
already open%s.", self->conn_task ? " (or still opening)" : "");
    return;
  }

  self->conn_task = g_task_new(self, cancellable, callback, user_data);
  g_task_set_task_data(self->conn_task, (gpointer) name, NULL);

  /* initialize connection information */
  self->server = NULL;
  self->conn_version = 0;
  self->current_cmd = _CODE_NONE;
  self->arg_bytes_to_read = 0;
  self->data_bytes_to_read = 0;
  /* @self->send_buf_written & @self->send_buf_len are managed by _output_*() */

  client = g_socket_client_new();
  printf("connecting to %s...\n", server); // debug
  g_socket_client_connect_to_host_async(client, server, SRVPORT, cancellable,
                                        _tcp_conn_estab_cb, self);

  /* task continues in _tcp_conn_estab_cb */
}

void _tcp_conn_estab_cb(GObject *source_object, GAsyncResult *res,
                        gpointer user_data) {
  RcConn *self = user_data;
  GError *err = NULL;
  gchar *name = g_task_get_task_data(self->conn_task);

  self->sock_conn
      = g_socket_client_connect_to_host_finish(G_SOCKET_CLIENT(source_object),
                                               res, &err);
  if (err) {
    if (self->conn_task) {
      g_task_return_error(self->conn_task, err);
      self->conn_task = NULL;
      g_object_unref(self->conn_task);
    }
    return;
  }

  self->istream = g_io_stream_get_input_stream(G_IO_STREAM(self->sock_conn));
  self->ostream = g_io_stream_get_output_stream(G_IO_STREAM(self->sock_conn));

  g_input_stream_read_async(self->istream, self->recv_buf,
                            sizeof (self->recv_buf), G_PRIORITY_DEFAULT, NULL,
                            _input_cb, self);

  if (name) {
    gsize length = 2 + 1 + strlen(name) + 1; /* ver, name, +1 -> \0 */
    gchar *msg = g_malloc(length);
    msg[0] = _CODE_VERSION;
    msg[1] = RCAMVER;
    msg[2] = _CODE_SEND_NAME;
    memcpy(&msg[3], name, length - 3);
    g_task_set_task_data(self->conn_task, msg, g_free);
    g_output_stream_write_all_async(self->ostream, msg, length,
                                    G_PRIORITY_DEFAULT,
                                    g_task_get_cancellable(self->conn_task),
                                    _send_ver_cb, self);
  } else {
    g_output_stream_write_all_async(self->ostream, _version_cmd, 2,
                                    G_PRIORITY_DEFAULT,
                                    g_task_get_cancellable(self->conn_task),
                                    _send_ver_cb, self);
  }

  /* task continues at _send_ver_cb */
}

void _send_ver_cb(GObject *source_object, GAsyncResult *res,
                  gpointer user_data) {
  RcConn *self = user_data;
  GError *err = NULL;

  if (!g_output_stream_write_all_finish(G_OUTPUT_STREAM(source_object), res,
                                        NULL, &err)) {
    rc_conn_close(self, err);
  }

  /* task continues at ACCEPT */
}


/**
 * rc_conn_connect_finish:
 * @self:   a #RcConn
 * @result: the #GAsyncResult
 * @error:  #GError for error reporting or %NULL to ignore
 *
 * Finishes an asynchronous connect operation.
 *
 * Returns: %TRUE if the connection was succesfully established, %FALSE if not
 */
gboolean rc_conn_connect_finish(RcConn *self, GAsyncResult *result,
                                GError **error) {
  g_return_val_if_fail(g_task_is_valid(result, self), FALSE);
  return g_task_propagate_boolean(G_TASK(result), error);
}


/*
 * _output_at_idle:
 * @self: a #RcConn
 *
 * Write the first element of @self->oqueue to @self->ostream.
 */
void _output_at_idle(RcConn *self) {
  GTask *task = g_queue_pop_head(self->oqueue);

  if (self->ostream == NULL || g_output_stream_has_pending(self->ostream))
    return;

  if (task) {
    if (g_task_get_source_tag(task) == rc_conn_capture_async) {
      self->send_buf_len = 2;
      self->send_buf[0] = _CODE_CAPTURE;
      self->send_buf[1] = GPOINTER_TO_UINT(g_task_get_task_data(task));
    } else { /* g_task_get_source_tag(task) = rc_conn_get_async */
      _GetTaskData *task_data = g_task_get_task_data(task);
      self->send_buf_len = 17;
      self->send_buf[0] = _CODE_GET;
      *((guint32 *) &self->send_buf[1]) = task_data->id;
      *((guint64 *) &self->send_buf[5]) = GUINT64_TO_BE(task_data->offset);
      *((guint32 *) &self->send_buf[13]) = GUINT32_TO_BE(task_data->length);
      self->get_tasks = g_slist_prepend(self->get_tasks, task);
    }

    self->current_out_task = task;
  } else if (self->deletions) {
    GSList *del = self->deletions;
    self->deletions = del->next;

    self->send_buf_len = 5;
    self->send_buf[0] = _CODE_DELETE_RESOURCE;
    *((guint32 *) &self->send_buf[1]) = GPOINTER_TO_UINT(del->data);

    g_slist_free_1(del);
  } else {
    return;
  }

  self->send_buf_written = 0;
  /* Do not make the write task cancellable, since cancellation would lead to
   * connection errors. */
  g_output_stream_write_async(self->ostream, self->send_buf, self->send_buf_len,
                              G_PRIORITY_DEFAULT, NULL, _output_cb, self);
}

void _output_cb(GObject *source_object, GAsyncResult *res, gpointer user_data) {
  RcConn *self = user_data;
  GError *err = NULL;
  gssize len = g_output_stream_write_finish(G_OUTPUT_STREAM(source_object), res,
                                            &err);
  if (err) {
    /* If the ouput fails, it is also likely that the input fails.  Treat this
     * as a fatal error. */
    rc_conn_close(self, err);
    return;
  }

  self->send_buf_written += len;
  if (self->send_buf_written < self->send_buf_len) {
    g_output_stream_write_async(G_OUTPUT_STREAM(source_object),
                                &self->send_buf[self->send_buf_written],
                                self->send_buf_written - self->send_buf_len,
                                G_PRIORITY_DEFAULT, NULL, _output_cb, self);
    return;
  }

  self->current_out_task = NULL;
  _output_at_idle(self);
}

/* simplify reading n bytes to argument buffer */
#define READ_TO_ARG_BUF(n)                                              \
  if (self->arg_bytes_to_read == 0)                                     \
    self->arg_bytes_to_read = n;                                        \
  /* fill arg_buf */ {                                                  \
    gsize step_size = MIN(len - i, self->arg_bytes_to_read);            \
    memcpy(&self->arg_buf[n - self->arg_bytes_to_read], &self->recv_buf[i], \
           step_size);                                                  \
    self->arg_bytes_to_read -= step_size;                               \
    if (self->arg_bytes_to_read != 0)                                   \
      goto recv_buf_refill;                                             \
    i += step_size;                                                     \
  }
/* macros to extract numbers from byte array */
#define GET_NUINT32(s,i) (*((guint32 *) &s[i])) /* network byte order */
#define GET_UINT32(s,i) GUINT32_FROM_BE(*((guint32 *) &s[i]))
#define GET_UINT64(s,i) GUINT64_FROM_BE(*((guint64 *) &s[i]))

void _input_cb(GObject *source_object, GAsyncResult *res,
                 gpointer user_data) {
  RcConn *self = user_data;
  GError *err = NULL;
  gssize len = g_input_stream_read_finish(G_INPUT_STREAM(source_object), res,
                                          &err);
  guint i = 0;

  if (err) {
    rc_conn_close(self, err);
    return;
  } else if (len == 0) {
    _close_nel(self, RC_CONN_ERROR_CONNECTION_CLOSED,
               "Peer closed the connection.");
    return;
  }

  while (i < len) {
    if (self->current_cmd == _CODE_NONE)
      self->current_cmd = self->recv_buf[i++];
    switch (self->current_cmd) {
      case _CODE_NONE:
        break; /* in case the other side actually sends NONE */


      case _CODE_VERSION:
        if (i >= len)
          goto recv_buf_refill;

        printf("< VERSION %i\n", self->recv_buf[i]); // debug
        if (self->conn_version != 0) {
          i++; /* version has already been set, ignore */
          self->current_cmd = _CODE_NONE;
          break;
        }

        self->conn_version = MIN(RCAMVER, self->recv_buf[i]);
        i++; /* DO NOT INCREMENT IN MACRO! */
        if (self->conn_version != 2) {
          /* only v2 is supported */
          /* @self->conn_task has to exist, since the connection is only
           * established if @self->conn_version != 0 and this callback is only
           * called if there is an active (perhaps not established)
           * connection. */
          _close_nel(self, RC_CONN_ERROR_VERSION_UNSUPPORTED, "The connection \
could not be established due to version incompatibilities.");
          return;
        }
        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_SEND_NAME:
        /* server side implementation, TODO */
        puts("< SEND NAME"); // debug
        if (!self->server) {
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (SEND NAME is disallowed for servers). Closing connection.");
          return;
        }
        while (i < len) {
          gchar c = self->recv_buf[i++];
          if (c == '\0') {
            self->current_cmd = _CODE_NONE;
            break;
          }
        }
        break;


      case _CODE_SUPPORTED_FORMATS:
        if (self->server) {
          puts("< SUPPORTED FORMATS"); // debug
          rc_conn_close(self, NULL);
          // TODO: server error handling
          return;
        }

        if (self->arg_bytes_to_read == 0) {
          puts("< SUPPORTED FORMATS …"); // debug
          self->arg_bytes_to_read = self->supported_formats_len
                                  = self->recv_buf[i++];
          self->supported_formats = g_malloc(self->arg_bytes_to_read);
        }
        /* copy data */ {
          gsize step_size = MIN(len - i, self->arg_bytes_to_read);
          memcpy(&self->supported_formats[self->supported_formats_len
                                          - self->arg_bytes_to_read],
                 &self->recv_buf[i], step_size);
          self->arg_bytes_to_read -= step_size;
          if (self->arg_bytes_to_read > 0)
            goto recv_buf_refill;
          i += step_size;
        }

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_ACCEPT:
        puts("< ACCEPT"); // debug
        if (self->server) {
          rc_conn_close(self, NULL);
          // TODO: server error handling
          return;
        }
        if (!self->conn_task) {
          self->current_cmd = _CODE_NONE; /* has already been accepted */
          break;
        }

        if (self->conn_version == 0) {
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (sending ACCEPT before VERSION). Closing.");
          return;
        }
        if (!self->supported_formats) {
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (sending ACCEPT before SUPPORTED FORMATS). Closing.");
          return;
        }

        g_task_return_boolean(self->conn_task, TRUE);
        g_object_unref(self->conn_task);
        self->conn_task = NULL;

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_CAPTURE:
        if (!self->server) {
          puts("< CAPTURE …"); // debug
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (CAPTURE is disallowed for servers). Closing connection.");
          return;
        }

        if (i >= len)
          goto recv_buf_refill;

        printf("< CAPTURE 0x%02x\n", self->recv_buf[i++]); // debug
        // TODO: server side implementation

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_CAPTURE_FINISHED:
        if (self->server) {
          puts("< CAPTURE FINISHED …"); // debug
          rc_conn_close(self, NULL);
          // TODO: error handling (server)
          return;
        }

        READ_TO_ARG_BUF(12);
        /* process the args */ {
          guint32 id = GET_NUINT32(self->arg_buf, 0);
          guint64 size = GET_UINT64(self->arg_buf, 4);
          printf("< CAPTURE FINISHED 0x%08x 0x%016lx\n", id, size); // debug
          g_task_set_task_data(self->capture_task, GUINT_TO_POINTER(id), NULL);
          g_task_return_int(self->capture_task, size);
          g_object_unref(self->capture_task);
          self->capture_task = NULL;
        }

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_CAPTURE_FAILED:
        if (self->server) {
          rc_conn_close(self, NULL);
          // TODO: error handling (server)
          return;
        }

        g_task_return_new_error(self->capture_task, RC_CONN_ERROR,
                                RC_CONN_ERROR_CAPTURE_FAILED, "The remote \
camera was unable to capture an image.");
        g_object_unref(self->capture_task);
        self->capture_task = NULL;

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_GET:
        if (!self->server) {
          puts("< GET"); // debug
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (GET is disallowed for servers). Closing connection.");
          return;
        }

        READ_TO_ARG_BUF(16);
        /* process args */ {
          guint32 id = GET_NUINT32(self->arg_buf, 0);
          guint64 offset = GET_UINT64(self->arg_buf, 4);
          guint32 length = GET_UINT32(self->arg_buf, 12);
          printf("< GET 0x%08x 0x%016lx 0x%08x\n", id, offset, length); // debug
          // TODO: server implementation
        }

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_SEND:
        if (self->server) {
          puts("< SEND");
          rc_conn_close(self, NULL);
          // TODO: error handling (server)
          return;
        }

        READ_TO_ARG_BUF(16);

        /* process args */ {
          /* From here on the code will only be run once per SEND */
          guint32 id = GET_NUINT32(self->arg_buf, 0);
          guint64 offset = GET_UINT64(self->arg_buf, 4);
          self->data_bytes_to_read = GET_UINT32(self->arg_buf, 12);

          printf("< SEND 0x%08x 0x%016lx 0x%08x …\n", id, offset,
                 self->data_bytes_to_read); // debug

          self->current_get_task = NULL;
          for (GSList *l = self->get_tasks; l != NULL; l = l->next) {
            GTask *task = l->data;
            _GetTaskData *task_data = g_task_get_task_data(task);
            if (task_data->id == id && task_data->offset == offset) {
              self->current_get_task = task;
              break;
            }
          }
        }
        if (self->data_bytes_to_read > 0) {
          _GetTaskData *data = NULL;
          if (self->current_get_task) {
            data = g_task_get_task_data(self->current_get_task);

            if (data->length < self->data_bytes_to_read) {
              /* There might be insufficient memory, anyway the server does not
               * follow the specification. */
              _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server \
is badly implemented (it attemted to send more bytes than requested). Closing \
connection.");
              return;
            }
            data->length = self->data_bytes_to_read;
            /* else there would be no possibility to determine, which length
             * the server sent */
          }

          if (i < len) {
            gsize step_size = MIN(len - i, self->data_bytes_to_read);
            if (self->current_get_task) {
              memcpy(data->dest, &self->recv_buf[i], step_size);
              data->dest = &data->dest[step_size];
            }
            i += step_size;
            self->data_bytes_to_read -= step_size;
          }
          if (self->data_bytes_to_read > 0) {
            if (data)
              g_input_stream_read_async(self->istream, data->dest,
                                        self->data_bytes_to_read,
                                        G_PRIORITY_DEFAULT, NULL, _data_cb,
                                        self);
            else
              g_input_stream_skip_async(self->istream, self->data_bytes_to_read,
                                        G_PRIORITY_DEFAULT, NULL, _data_cb,
                                        self);
            return;
          }
        }

        if (self->current_get_task) {
          _GetTaskData *data = g_task_get_task_data(self->current_get_task);
          g_task_return_int(self->current_get_task, data->length);
          self->get_tasks = g_slist_remove(self->get_tasks,
                                           self->current_get_task);
          g_object_unref(self->current_get_task);
          self->current_get_task = NULL;
        }

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_DELETE_RESOURCE:
        if (!self->server) {
          puts("< DELETE RESOURCE …");
          _close_nel(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The server is \
badly implemented (DELETE RESOURCE is disallowed for servers). Closing \
connection.");
          return;
        }

        READ_TO_ARG_BUF(4);
        /* process args */ {
          guint32 id = GET_NUINT32(self->arg_buf, 0);
          printf("< DELETE RESOURCE 0x%02x\n", id); // debug
          // TODO: server side implementation
        }

        self->current_cmd = _CODE_NONE;
        break;


      case _CODE_RESOURCE_UNAVAILABLE:
        if (self->server) {
          puts("< RESOURCE UNAVAILABLE …");
          rc_conn_close(self, NULL);
          // TODO: error handling (server)
          return;
        }

        READ_TO_ARG_BUF(4);
        /* process args */ {
          guint32 id = GET_NUINT32(self->arg_buf, 0);
          GSList **l = &self->get_tasks;
          printf("< RESOURCE UNAVAILABLE 0x%02x\n", id); // debug

          while (*l) {
            GTask *task = (*l)->data;
            if (((_GetTaskData *) g_task_get_task_data(task))->id == id) {
              GSList *del_el = (*l);
              *l = del_el->next;
              g_slist_free_1(del_el);

              g_task_return_new_error(task, RC_CONN_ERROR,
                                      RC_CONN_ERROR_RESOURCE_UNAVAILABLE, "The \
requested resource is unavailable.");
              g_object_unref(task);
              continue;
            }
            l = &(*l)->next;
          }
        }

        self->current_cmd = _CODE_NONE;
        break;


      default:
        printf("< 0x%02x\n", self->current_cmd); // debug
        _close_ne(self, RC_CONN_ERROR_BAD_IMPLEMENTATION, "The %s is badly \
implemented (unsupported code 0x%02x). Closing connection.",
                  self->server ? "server" : "client", self->current_cmd);
        return;
    }
  }

recv_buf_refill:
  g_input_stream_read_async(self->istream, self->recv_buf,
                            sizeof (self->recv_buf), G_PRIORITY_DEFAULT,  NULL,
                            _input_cb, self);
}

void _data_cb(GObject *source_object, GAsyncResult *res, gpointer user_data) {
  RcConn *self = user_data;
  GError *err = NULL;
  gssize len;
  if (self->current_get_task)
    len = g_input_stream_read_finish(G_INPUT_STREAM(source_object), res, &err);
  else
    len = g_input_stream_skip_finish(G_INPUT_STREAM(source_object), res, &err);

  if (err) {
    rc_conn_close(self, err);
    return;
  } else if (len <= 0) {
    _close_nel(self, RC_CONN_ERROR_CONNECTION_CLOSED, "The connection was \
closed while there were tasks waiting for completion.");
    return;
  }

  self->data_bytes_to_read -= len;

  if (self->current_get_task) {
    _GetTaskData *data = g_task_get_task_data(self->current_get_task);
    if (self->data_bytes_to_read > 0) {
      data->dest = &data->dest[len];
      g_input_stream_read_async(G_INPUT_STREAM(source_object), data->dest,
                                self->data_bytes_to_read, G_PRIORITY_DEFAULT,
                                NULL, _data_cb, self);
      return;
    } else {
      g_task_return_int(self->current_get_task, data->length);
      self->get_tasks = g_slist_remove(self->get_tasks, self->current_get_task);
      g_object_unref(self->current_get_task);
      self->current_get_task = NULL;
    }
  } else if (self->data_bytes_to_read > 0) {
    g_input_stream_skip_async(G_INPUT_STREAM(source_object),
                              self->data_bytes_to_read, G_PRIORITY_DEFAULT,
                              NULL, _data_cb, self);
    return;
  }

  self->current_cmd = _CODE_NONE;
  g_input_stream_read_async(G_INPUT_STREAM(source_object), self->recv_buf,
                            sizeof (self->recv_buf), G_PRIORITY_DEFAULT, NULL,
                            _input_cb, self);
}


/**
 * rc_conn_get_supported_formats:
 * @self: a #RcConn
 * @formats: reference to a variable in which to store the formats array
 *
 * Gets all the formats supported by the server.  Note that the server sends the
 * formats somewhere between the opening of the TCP connection and the
 * acceptance of the client.
 *
 * Returns: count of supported formats, %0 on error
 */
guint8 rc_conn_get_supported_formats(RcConn *self, guint8 **formats) {
  g_return_val_if_fail(RC_IS_CONN(self), 0);

  *formats = self->supported_formats;
  return self->supported_formats_len;
}


/**
 * rc_conn_capture_async:
 * @self:                    a #RcConn
 * @format:                  format in which to capture, has to be one of the
 *                           supported formats
 * @cancellable: (nullable): optional #GCancellable object, %NULL to ignore
 * @callback: (scope async): a #GAsyncReadyCallback
 * @user_data: (closure):    user data passed to callback
 *
 * Capture an image asynchronously.  When the operation is finished @callback
 * will be called.  You should then call rc_conn_get_finish() to get the
 * results.
 */
void rc_conn_capture_async(RcConn *self, guint8 format,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback, gpointer user_data) {
  GTask *task;

  g_return_if_fail(RC_IS_CONN(self));

  task = g_task_new(self, cancellable, callback, user_data);
  if (!self->sock_conn || self->conn_task) {
    g_task_return_new_error(task, RC_CONN_ERROR, RC_CONN_ERROR_NOT_ACCEPTED,
                            "Calling CAPTURE on a connection that has not been \
accepted is disallowed.");
    g_object_unref(task);
    return;
  }
  if (self->capture_task) {
    g_task_return_new_error(task, RC_CONN_ERROR, RC_CONN_ERROR_CAPTURE_PENDING,
                            "There is already a capture pending.");
    g_object_unref(task);
    return;
  }

  printf("> CAPTURE 0x%02x\n", format); // debug
  g_task_set_source_tag(task, rc_conn_capture_async);
  g_task_set_task_data(task, GUINT_TO_POINTER(format), NULL);
  self->capture_task = task;

  g_queue_push_tail(self->oqueue, task);
  _output_at_idle(self);
}

/**
 * rc_conn_capture_finish:
 * @self:   a #RcConn
 * @result: the #GAsyncResult
 * @id:     reference to a variable in which to store the id
 * @error:  #GError for error reporting or %NULL to ignore
 *
 * Finish a rc_conn_capture_async() operation.
 *
 * Returns: the size of the file to download
 */
gssize rc_conn_capture_finish(RcConn *self, GAsyncResult *result, guint32 *id,
                              GError **error) {
  GTask *task = G_TASK(result);
  g_return_val_if_fail(g_task_is_valid(task, self), FALSE);
  if (!g_task_had_error(task))
    *id = (guint32) GPOINTER_TO_UINT(g_task_get_task_data(task));
  // TODO: large files ok on 32 bit systems?
  return g_task_propagate_int(task, error);
}


/**
 * rc_conn_get_async:
 * @self:                    a #RcConn
 * @id:                      id of the resource to download
 * @offset:                  offset in the resource
 * @length:                  length of the chunk to download
 * @buffer:                  buffer to store the data in
 * @cancellable: (nullable): optional #GCancellable object, %NULL to ignore
 * @callback: (scope async): a #GAsyncReadyCallback
 * @user_data: (closure):    user data passed to callback
 *
 * Asynchronously invoke a GET on an #RcConn.  When the operation is finished
 * @callback will be called.  You should then call rc_conn_get_finish() to get
 * the results.
 */
void rc_conn_get_async(RcConn *self, guint32 id, guint64 offset, guint32 length,
                       gchar *buffer, GCancellable *cancellable,
                       GAsyncReadyCallback callback, gpointer user_data) {
  GTask *task;
  _GetTaskData *data;

  g_return_if_fail(RC_IS_CONN(self));

  task = g_task_new(self, cancellable, callback, user_data);
  if (!self->sock_conn || self->conn_task) {
    g_task_return_new_error(task, RC_CONN_ERROR, RC_CONN_ERROR_NOT_ACCEPTED,
                            "Calling GET on a connection that has not been \
accepted is disallowed.");
    g_object_unref(task);
    return;
  }

  printf("> GET 0x%08x 0x%016lx 0x%08x\n", id, offset, length); // debug
  data = g_slice_new(_GetTaskData);
  data->dest = buffer;
  data->id = id;
  data->offset = offset;
  data->length = length;
  g_task_set_source_tag(task, rc_conn_get_async);
  g_task_set_task_data(task, data, _get_task_data_free);

  g_queue_push_tail(self->oqueue, task);
  _output_at_idle(self);
}

/**
 * rc_conn_get_finish:
 * @self:   a #RcConn
 * @result: the #GAsyncResult
 * @error:  #GError for error reporting or %NULL to ignore
 *
 * Finishes a rc_conn_get_async() operation.
 *
 * Returns: the count of bytes gotten from the server
 */
gssize rc_conn_get_finish(RcConn *self, GAsyncResult *result, GError **error) {
  g_return_val_if_fail(g_task_is_valid(result, self), -1);
  return g_task_propagate_int(G_TASK(result), error);
}


/**
 * rc_conn_delete:
 * @self: a #RcConn
 * @id: id to delete
 *
 * Triggers a DELETE RESOURCE request.  Note that this should only be called
 * after the resource is completely downloaded.
 */
void rc_conn_delete(RcConn *self, guint32 id) {
  g_return_if_fail(RC_IS_CONN(self));
  g_return_if_fail(self->sock_conn && self->conn_task == NULL);

  printf("> DELETE RESOURCE 0x%08x\n", id);
  self->deletions = g_slist_prepend(self->deletions, GUINT_TO_POINTER(id));
  _output_at_idle(self);
}


/**
 * rc_conn_close:
 * @self:              an #RcConn
 * @error: (nullable): a #GError describing the reason for closing
 *
 * Closes an #RcConn and passes @error to all tasks.  This function should only
 * be used in error cases, in order to properly shutdown the connection use
 * rc_conn_shutdown_async().
 *
 * Note that ownership of @error is taken.  If you need a local copy of @error,
 * you need to call g_error_copy() before.
 */
void rc_conn_close(RcConn *self, GError *error) {
  GTask *task;

  g_return_if_fail(RC_IS_CONN(self));

  self->istream = NULL;
  self->ostream = NULL;

  if (self->sock_conn)
    g_io_stream_close(G_IO_STREAM(self->sock_conn), NULL, NULL);
  self->sock_conn = NULL;

  if (self->server) {
    /* Servers work different at error handling. Anyway –
     * TODO: server implementation */
    return;
  }

  if (self->supported_formats) {
    g_free(self->supported_formats);
    self->supported_formats = NULL;
    self->supported_formats_len = 0;
  }

  /* free the tasks */
  if (!error)
    error = g_error_new_literal(RC_CONN_ERROR, RC_CONN_ERROR_CONNECTION_CLOSED,
                                "The connection was closed.");

  if ((task = self->conn_task)) {
    self->conn_task = NULL;
    g_task_return_error(task, error);
    g_object_unref(task);
    return; /* there may not be any other tasks by now */
  }

  /*
   * @self->current_get_task IS in @self->get_tasks
   * @self->current_get_task CAN be @self->current_out_task
   * @self->current_get_task CANNOT be in @self->oqueue
   * @self->current_get_task CANNOT be the only reference
   */
  self->current_get_task = NULL;

  /* @self->oqueue CAN contain @self->capture_task */
  while ((task = g_queue_pop_head(self->oqueue))) {
    if (task == self->capture_task)
      self->capture_task = NULL;
    g_task_return_error(task, g_error_copy(error));
    g_object_unref(task);
  }

  /*
   * @self->get_tasks CAN contain @self->current_out_task
   * @self->get_tasks CANNOT contain @self->current_get_task (now)
   */
  for (GSList *del = self->get_tasks; self->get_tasks;) {
    self->get_tasks = del->next;
    if (del->data == self->current_out_task)
      self->current_out_task = NULL;
    g_task_return_error(del->data, g_error_copy(error));
    g_object_unref(del->data);
    g_slist_free_1(del);
  }

  /*
   * @self->current_out_task CAN be @self->capture_task
   * @self->current_out_task CANNOT be in @self->get_tasks (now)
   * @self->current_out_task CANNOT be @self->current_get_task (now)
   * @self->current_out_task CANNOT be in @self->oqueue
   */
  if ((task = self->current_out_task)) {
    self->current_out_task = NULL;
    g_task_return_error(task, g_error_copy(error));
    if (task == self->capture_task)
      self->capture_task = NULL;
    g_object_unref(task);
  }

  /*
   * @self->capture_task CANNOT be in @self->oqueue (now)
   * @self->capture_task CANNOT be @self->current_out_task (now)
   * @self->capture_task IS the only reference (now)
   */
  if ((task = self->capture_task)) {
    self->capture_task = NULL;
    g_task_return_error(task, g_error_copy(error));
    g_object_unref(task);
  }

  g_slist_free(self->deletions);
  self->deletions = NULL;

  if ((task = self->shutdown_task)) {
    self->shutdown_task = NULL;
    g_task_return_error(task, error);
    g_object_unref(task);
  } else {
    g_error_free(error);
  }
}

// TODO: implement rc_conn_shutdown_* functions
//g_io_stream_close_async(G_IO_STREAM(self->sock_conn), G_PRIORITY_LOW, NULL,
//                        _conn_close_cb, NULL);
//g_tcp_connection_set_graceful_disconnect(G_TCP_CONNECTION(self->sock_conn),
//                                         TRUE);

//void _close_cb(GObject *source_object, GAsyncResult *res,
//               gpointer user_data) {
//  g_io_stream_close_finish(G_IO_STREAM(source_object), res, NULL);
//  g_object_unref(source_object);
//}

/*
 * _get_task_data_free:
 * @mem: pointer to a #_GetTaskData
 *
 * Call g_slice_free1() on @mem.
 */
void _get_task_data_free(gpointer mem) {
  g_slice_free1(sizeof (_GetTaskData), mem);
}

#include "stepper.h"

void stepper_init(stepper_t *stepper, unsigned int steps_per_revolution, uint8_t pin_1, uint8_t pin_2, uint8_t pin_3, uint8_t pin_4) {
  stepper->pin_1 = pin_1;
  stepper->pin_2 = pin_2;
  stepper->pin_3 = pin_3;
  stepper->pin_4 = pin_4;

  pinMode(pin_1, OUTPUT);
  pinMode(pin_2, OUTPUT);
  pinMode(pin_3, OUTPUT);
  pinMode(pin_4, OUTPUT);

  stepper->steps_per_revolution = steps_per_revolution;
  stepper->step_delay = 60UL * 1000UL * 1000UL / (steps_per_revolution * 30);

  stepper->steps_to_go = 0;
  stepper->step_nr = 0;
}

void stepper_do_steps(stepper_t *stepper) {
  if (stepper->steps_to_go == 0)
    return;
  if (!TIMER_RUN(stepper->timer))
    return;
  
  if (stepper->steps_to_go > 0) { /* forwards */
    if (stepper->steps_to_go != INFINITY_FORWARDS)
      stepper->steps_to_go--;
    if (stepper->step_nr++ == 3)
      stepper->step_nr = 0;
  } else { /* backwards */
    if (stepper->steps_to_go != INFINITY_BACKWARDS)
      stepper->steps_to_go++;
    if (stepper->step_nr-- == 0)
      stepper->step_nr = 3;
  }
  
  switch (stepper->step_nr) {
    case 0: // 1010
      digitalWrite(stepper->pin_1, HIGH);
      digitalWrite(stepper->pin_2, LOW);
      digitalWrite(stepper->pin_3, HIGH);
      digitalWrite(stepper->pin_4, LOW);
      break;
    case 1: // 0110
      digitalWrite(stepper->pin_1, LOW);
      digitalWrite(stepper->pin_2, HIGH);
      digitalWrite(stepper->pin_3, HIGH);
      digitalWrite(stepper->pin_4, LOW);
      break;
    case 2: // 0101
      digitalWrite(stepper->pin_1, LOW);
      digitalWrite(stepper->pin_2, HIGH);
      digitalWrite(stepper->pin_3, LOW);
      digitalWrite(stepper->pin_4, HIGH);
      break;
    case 3: // 1001
      digitalWrite(stepper->pin_1, HIGH);
      digitalWrite(stepper->pin_2, LOW);
      digitalWrite(stepper->pin_3, LOW);
      digitalWrite(stepper->pin_4, HIGH);
      break;
  }

  if (stepper->steps_to_go != 0)
    TIMER_SET(stepper->timer, stepper->step_delay);
}


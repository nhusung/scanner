#ifndef __TIMER_H__
#define __TIMER_H__

#include <Arduino.h>

/* highest bit of an unsigned long */
#define ULONG_H (1UL << (8 * sizeof (unsigned long) - 1))
#define TIMER_RUN(t) (micros() - (t) < ULONG_H)
#define TIMER_SET(t, us) (t = micros() + (us))

#endif /* !__TIMER_H__ */

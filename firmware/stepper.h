#ifndef __STEPPER_H__
#define __STEPPER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <Arduino.h>
#include "timer.h"
#include <stdint.h>

#define INFINITY_FORWARDS INT16_MAX
#define INFINITY_BACKWARDS INT16_MIN

typedef struct stepper {
  uint8_t pin_1;
  uint8_t pin_2;
  uint8_t pin_3;
  uint8_t pin_4;

  unsigned int steps_per_revolution;
  unsigned long step_delay;

  uint8_t step_nr;
  int16_t steps_to_go;

  unsigned long timer;
} stepper_t;

void stepper_init(stepper_t *stepper, unsigned int steps_per_revolution, uint8_t pin_1, uint8_t pin_2, uint8_t pin_3, uint8_t pin_4);

inline void stepper_set_speed(stepper_t *stepper, unsigned int rpm) {
  stepper->step_delay = 60UL * 1000UL * 1000UL / (stepper->steps_per_revolution * rpm);
}

void stepper_do_steps(stepper_t *stepper);
inline void stepper_step(stepper_t *stepper, int16_t steps) {
  stepper->steps_to_go = steps;
  TIMER_SET(stepper->timer, 0);
}

inline void stepper_off(stepper_t *stepper) {
  stepper->steps_to_go = 0;
  digitalWrite(stepper->pin_1, LOW);
  digitalWrite(stepper->pin_2, LOW);
  digitalWrite(stepper->pin_3, LOW);
  digitalWrite(stepper->pin_4, LOW);
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !__STEPPER_H__ */

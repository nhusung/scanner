#include "timer.h"
unsigned long timer;
#include "stepper.h"

#define EMERGENCY_OFF 2 // interrupt pin, HIGH -> down
#define PAPER_SENSOR  3 // interrupt pin, HIGH -> paper

#define INDENT_MOTOR  4
stepper_t stepper;

#define CONN_READY 0x0f
#define CONN_EXIT  0x00
#define CONN_PAUSE 0x01
#define CONN_OK    0x02
#define CONN_PHOTO 0x0a


#define RUN_STATE_EXIT  0b0001
#define RUN_STATE_PAUSE 0b0010
uint8_t run_state;

enum {
  s_indent_start, s_paper_detect, s_paper_detected_check, s_indent_stop,
  s_paper_free, s_take_photo,
  s_paper_left
} state;


#define PAPER_CHECK_INTERVAL 200000 /* µs */
#define PAPER_CHECK_STEPS    12
#define INDENT_STOP_INTERVAL 450000 /* µs */
#define PAPER_REMOVE_STEPS   700


void setup() {
  //pinMode(LED_BUILTIN, OUTPUT); // DEBUG
  
  pinMode(INDENT_MOTOR, OUTPUT);
  stepper_init(&stepper, 200, 5, 6, 7, 8);
  stepper_set_speed(&stepper, 200);
  
  pinMode(PAPER_SENSOR, INPUT);
  pinMode(EMERGENCY_OFF, INPUT);
  attachInterrupt(digitalPinToInterrupt(EMERGENCY_OFF), emergency_off_isr, RISING);

  Serial.begin(9600);
}

void emergency_off_isr() {
  digitalWrite(INDENT_MOTOR, LOW);
  stepper_off(&stepper);
  run_state |= RUN_STATE_EXIT;
}

bool pause_if_needed() {
  if (run_state & RUN_STATE_PAUSE) {
    stepper_off(&stepper);
    digitalWrite(INDENT_MOTOR, LOW);
    
    Serial.write(CONN_OK);
    Serial.flush();
    
    while (run_state & RUN_STATE_PAUSE) {
      if (Serial.available() > 0) {
        char c = Serial.read();
        if (c == CONN_PAUSE) {
          Serial.write(CONN_OK);
          Serial.flush();
          run_state &= ~RUN_STATE_PAUSE;
        } else if (c == CONN_EXIT) {
          run_state |= RUN_STATE_EXIT;
          break;
        }
      }
      if (run_state & RUN_STATE_EXIT) /* for ISR (button) */
        break;
    }
    return true;
  }
  return false;
}

void loop() {
  /* empty buffer */
  while (Serial.available())
    Serial.read();

  /* wait for connection */
  TIMER_SET(timer, 0); /* in case the timer is run after restart */
  while (true) {
    if (TIMER_RUN(timer)) {
      Serial.write(CONN_READY);
      Serial.flush();
      TIMER_SET(timer, 500000);
    }
    if (Serial.available() && Serial.read() == CONN_READY)
      break; /* connection established */
  }

  /* main loop */
  run_state = 0;
  state = s_indent_start;
  while (true) {
    if (Serial.available()) {
      char c = Serial.read();
      if (c == CONN_EXIT)
        break;
      if (c == CONN_PAUSE)
        run_state |= RUN_STATE_PAUSE;
    }
    stepper_do_steps(&stepper);

    if (state == s_indent_start) {
      digitalWrite(INDENT_MOTOR, HIGH);
      stepper_step(&stepper, INFINITY_FORWARDS);
      state = s_paper_detect;
    } else if (state == s_paper_detect) {
      if (pause_if_needed())
        state = s_indent_start;
      if (digitalRead(PAPER_SENSOR) == HIGH) {
        state = s_paper_detected_check;
        TIMER_SET(timer, PAPER_CHECK_INTERVAL);
      }
    } else if (state == s_paper_detected_check && TIMER_RUN(timer)) {
      if (digitalRead(PAPER_SENSOR) == HIGH) {
        state = s_indent_stop; /* still paper -> go on */
        TIMER_SET(timer, INDENT_STOP_INTERVAL);
      } else {
        state = s_paper_detect; /* retry */
      }
    } else if (state == s_indent_stop && TIMER_RUN(timer)) {
      digitalWrite(INDENT_MOTOR, LOW);
      state = s_paper_free;
    } else if (state == s_paper_free && digitalRead(PAPER_SENSOR) == LOW) {
      state = s_take_photo;
      stepper_step(&stepper, PAPER_CHECK_STEPS);
    } else if (state == s_take_photo && stepper.steps_to_go != 0) {
      if (digitalRead(PAPER_SENSOR) == LOW) {
        stepper_off(&stepper); /* still low -> go on */

        /* take photo */
        Serial.write(CONN_PHOTO);
        Serial.flush();
        while (true) {
          if (Serial.available() > 0) {
            char c = Serial.read();
            if (c == CONN_OK)
              break;
            if (c == CONN_EXIT) {
              run_state |= RUN_STATE_EXIT;
              break;
            }
            if (c == CONN_PAUSE)
              run_state |= RUN_STATE_PAUSE;
          }
          if (run_state & RUN_STATE_EXIT) /* for ISR (button) */
            break;
        }

        /* now's the time to pause */
        pause_if_needed();

        /* photo taken */
        state = s_indent_start;
      } else {
        state = s_paper_free; /* retry */
        stepper_step(&stepper, INFINITY_FORWARDS);
      }
    }

    if (run_state & RUN_STATE_EXIT)
      break;
  }
  
  stepper_off(&stepper);
  digitalWrite(INDENT_MOTOR, LOW);
  Serial.write(CONN_EXIT); /* TODO: wait for OK signal */
  Serial.flush();
  delay(1000);
}

#include <glib.h>
#include <gio/gio.h>
#include <gio/gunixinputstream.h> /* for terminal input */

#include <stdio.h>

#define SRVPORT 1708

#define RCAM_CODE_NONE                 0x00
#define RCAM_CODE_VERSION              0x01
#define RCAM_CODE_SEND_NAME            0x10
#define RCAM_CODE_SUPPORTED_FORMATS    0x11
#define RCAM_CODE_SELECT_FORMAT        0x12
#define RCAM_CODE_ACCEPT               0x1f
#define RCAM_CODE_CAPTURE_STILL_IMAGE  0x20
#define RCAM_CODE_CAPTURE_FINISHED     0x2f
#define RCAM_CODE_GET                  0x30
#define RCAM_CODE_SEND                 0x31
#define RCAM_CODE_DELETE_RESOURCE      0x32
#define RCAM_CODE_RESOURCE_UNAVAILABLE 0xf0
#define RCAM_CODE_UNSUPPORTED_FORMAT   0xf1
#define RCAM_CODE_CAPTURE_FAILED       0xf2

#define RCAM_FORMAT_JPEG    0x00
#define RCAM_FORMAT_PNG     0x01
#define RCAM_FORMAT_TIFF    0x02
#define RCAM_FORMAT_HEIF    0x03
#define RCAM_FORMAT_INVALID 0xff

gchar rcam_supported_formats[] = { /* first is preferred */
  RCAM_FORMAT_PNG,
  RCAM_FORMAT_TIFF,
  RCAM_FORMAT_JPEG,
  RCAM_FORMAT_INVALID /* last is default is invalid */
};

gchar *format_extensions[4] = {
  "jpg",
  "png",
  "tif",
  "heif"
};

gchar omsgs[] = {
  RCAM_CODE_VERSION,
  1, /* protocol version */

  RCAM_CODE_SEND_NAME,
  0, 0, 0, 0, 0, 0, 0, 0, /* 32 B for the name */
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  RCAM_CODE_SELECT_FORMAT,
  0, /* format */

  RCAM_CODE_CAPTURE_STILL_IMAGE,

  RCAM_CODE_GET,
  0, 0, 0, 0, /* id */
  0, 0, 0, 0, 0, 0, 0, 0, /* offset */
  0, 0, 0, 0, /* length */

  RCAM_CODE_DELETE_RESOURCE,
  0, 0, 0, 0 /* id */
};
enum {
  OMSGS_IDX_VERSION = 0,
  OMSGS_IDX_VERSION_ATTR = 1,
  OMSGS_IDX_SEND_NAME = 2,
  OMSGS_IDX_SEND_NAME_ATTR = 3,
  OMSGS_IDX_SELECT_FORMAT = 35, /* 3 + 32 */
  OMSGS_IDX_SELECT_FORMAT_ATTR = 36,
  OMSGS_IDX_CAPTURE_STILL_IMAGE = 37,
  OMSGS_IDX_GET = 38,
  OMSGS_IDX_GET_ID = 39,
  OMSGS_IDX_GET_OFFSET = 43, /* 39 + 4 */
  OMSGS_IDX_GET_LENGTH = 51, /* 43 + 8 */
  OMSGS_IDX_DELETE_RESOURCE = 55, /* 51 + 4 */
  OMSGS_IDX_DELETE_RESOURCE_ID = 56
};


typedef struct oqueue_s oqueue_t;
/**
 * oqueue_el_t:
 * @n: pointer to the next element in the queue
 * @idx: index of an output message stored in @omesgs
 * @length: length of the message
 *
 * An element of an output queue.
 */
struct oqueue_s {
  oqueue_t *n;
  gint idx;
  gsize length;
};

typedef struct dload_queue_s dload_queue_t;
/**
 * dload_queue_t:
 * @n: pointer to the next element in the queue
 * @id: id of the resource being downloaded
 * @f: the #GFileOutputStream to write to
 * @size: total size of the download
 * @cur: cursor, pointer to the current position in the file
 *
 * An element of a download queue.
 */
struct dload_queue_s {
  dload_queue_t *n;
  guint32 id;
  GOutputStream *f;
  guint64 size;
  guint64 cur;
};
dload_queue_t *queue = NULL;

/* macros to extract numbers from binary stream */
#define GET_NUINT32(s,i) (*((guint32 *) &s[i])) /* network byte order */
#define GET_UINT32(s,i) GUINT32_FROM_BE(*((guint32 *) &s[i]))
#define GET_UINT64(s,i) GUINT64_FROM_BE(*((guint64 *) &s[i]))


GInputStream *istream;
GOutputStream *ostream;

oqueue_t *oqueue_head = NULL;
oqueue_t *oqueue_tail = NULL;
oqueue_t *oqueue_unused = NULL;

int accepted = 0;
gchar recv_buf[512 * 1024 + 17]; /* big enough to store a usual send command */
guint32 save_id = 0;

guint8 current_cmd;
gchar arg_buf[16];
guint arg_bytes_to_read = 0;


static void enqueue_out(gint idx, gsize length);
static void istream_read_callback(GObject *source_object, GAsyncResult *res,
                                  gpointer user_data);
static void ostream_write_callback(GObject *source_object, GAsyncResult *res,
                                   gpointer user_data);

/*
 * ostream_write_if_idle:
 *
 * Write a message to @ostream if no operation is pending.
 */
static void ostream_write_if_idle(void) {
  if (g_output_stream_has_pending(ostream) || oqueue_head == NULL)
    return;

  g_output_stream_write_all_async(ostream, &omsgs[oqueue_head->idx],
                                  oqueue_head->length, G_PRIORITY_DEFAULT, NULL,
                                  ostream_write_callback, NULL);
}

/*
 * enqueue_out:
 * @idx: index of an output message stored in @omesgs
 * @length: length of the message
 *
 * Enqeueue an output message and write if no operation is pending.
 */
void enqueue_out(gint idx, gsize length) {
  oqueue_t *new_tail;
  if (oqueue_unused == NULL) {
    new_tail = g_slice_new(oqueue_t);
  } else {
    new_tail = oqueue_unused;
    oqueue_unused = new_tail->n;
  }
  new_tail->n = NULL;
  new_tail->idx = idx;
  new_tail->length = length;
  if (oqueue_head == NULL)
    oqueue_head = new_tail;
  else
    oqueue_tail->n = new_tail;
  oqueue_tail = new_tail;

  ostream_write_if_idle();
}


void ostream_write_callback(GObject *source_object, GAsyncResult *res,
                            gpointer user_data) {
  GError *err = NULL;
  if (!g_output_stream_write_all_finish(ostream, res, NULL, &err)) {
    g_error("writing to socket failed: %s", err->message);
    g_output_stream_close(ostream, NULL, NULL);
    g_input_stream_close(istream, NULL, NULL);
    return;
  }

  /* remove the current output chunk */
  oqueue_t *old_head = oqueue_head;
  oqueue_head = old_head->n;
  old_head->n = oqueue_unused;
  oqueue_unused = old_head;

  ostream_write_if_idle();
}


int rcam_connect(const gchar *server, const gchar *name) {
  GError *err = NULL;
  GSocketConnection *conn;
  GSocketClient *client = g_socket_client_new();

  printf("connecting to %s...\n", server);
  conn = g_socket_client_connect_to_host(client, server, SRVPORT, NULL, &err);
  if (err != NULL) {
    g_error("%s", err->message);
    return -1;
  }

  istream = g_io_stream_get_input_stream(G_IO_STREAM(conn));
  ostream = g_io_stream_get_output_stream(G_IO_STREAM(conn));

  g_input_stream_read_async(istream, recv_buf, sizeof (recv_buf),
                            G_PRIORITY_DEFAULT, NULL, istream_read_callback,
                            NULL);

  puts("> VERSION 1");
  if (name) {
    gsize length = g_strlcpy(&omsgs[OMSGS_IDX_SEND_NAME_ATTR], name,
                             32);
    printf("> SEND NAME %s\n", &omsgs[OMSGS_IDX_SEND_NAME_ATTR]);
    enqueue_out(OMSGS_IDX_VERSION, 2 + 1 + length + 1); /* +1 -> \0 */
  } else {
    enqueue_out(OMSGS_IDX_VERSION, 2);
  }

  return 0;
}

void rcam_select_format(guint8 fmt) {
  omsgs[OMSGS_IDX_SELECT_FORMAT_ATTR] = fmt;
  printf("> SELECT FORMAT 0x%02x\n", fmt);
  enqueue_out(OMSGS_IDX_SELECT_FORMAT, 2);
}

void rcam_capture_still_image(void) {
  if (accepted == 0) {
    g_warning("Calling CAPTURE STILL IMAGE on a connection that has not been \
accepted yet is not allowed!");
    //write(sd, &msg, 1); // msg = 0
    return;
  }
  puts("> CAPTURE STILL IMAGE");
  enqueue_out(OMSGS_IDX_CAPTURE_STILL_IMAGE, 1);
}

void rcam_get(guint32 id, guint64 offset, guint32 length) {
  if (accepted == 0) {
    g_warning("Calling GET on a connection that has not been accepted yet is \
not allowed!");
    return;
  }
  *((guint32 *) &omsgs[OMSGS_IDX_GET_ID]) = id;
  *((guint64 *) &omsgs[OMSGS_IDX_GET_OFFSET]) = GUINT64_TO_BE(offset);
  *((guint32 *) &omsgs[OMSGS_IDX_GET_LENGTH]) = GUINT32_TO_BE(length);
  printf("> GET 0x%08x 0x%016lx 0x%08x\n", id, offset, length);
  enqueue_out(OMSGS_IDX_GET, 17);
}

void rcam_delete_resource(guint32 id) {
  if (accepted == 0) {
    fputs("Calling DELETE RESOURCE on a connection that has not been accepted \
yet is not allowed!\n", stderr);
    return;
  }
  *((guint32 *) &omsgs[OMSGS_IDX_DELETE_RESOURCE_ID]) = id;
  printf("> DELETE RESOURCE 0x%08x\n", id);
  enqueue_out(OMSGS_IDX_DELETE_RESOURCE, 5);
}


static void file_open_callback(GObject *source_object, GAsyncResult *res,
                               gpointer user_data) {
  GTask *task = G_TASK(user_data);
  dload_queue_t *new = g_task_get_task_data(task);
  GError *err = NULL;
  if ((new->f = G_OUTPUT_STREAM(g_file_replace_finish(G_FILE(source_object),
                                                      res, &err))) == NULL) {
    g_task_return_error(task, err);
    g_object_unref(task);
  } else {
    g_task_return_boolean(task, TRUE);
    g_object_unref(task);
  }
}
static void dload_init_async(struct dload_queue_s **queue, guint32 id,
                             const gchar *name, guint64 size,
                             GCancellable *cancellable,
                             GAsyncReadyCallback callback,
                             gpointer user_data) {
  dload_queue_t *new = g_slice_new(dload_queue_t);
  GFile *file = g_file_new_for_path(name);
  GTask *task = g_task_new(NULL, cancellable, callback, user_data);
  g_task_set_task_data(task, new, NULL);

  new->id = id;
  new->size = size;
  g_file_replace_async(file, NULL, FALSE, G_FILE_CREATE_NONE,
                       G_PRIORITY_DEFAULT, cancellable, file_open_callback,
                       task);
  new->cur = 0;
  new->n = *queue;
  *queue = new;
}
static gboolean dload_init_finish(GAsyncResult *result, GError **error) {
  g_return_val_if_fail(g_task_is_valid(result, NULL), FALSE);
  return g_task_propagate_boolean(G_TASK(result), error);
}


static void dload_init_callback(GObject *source_object, GAsyncResult *res,
                                gpointer user_data) {
  GError *err = NULL;
  dload_queue_t *queue = *((dload_queue_t **) user_data);
  if (!dload_init_finish(res, &err)) {
    g_error("initializing download failed: %s", err->message);
    g_input_stream_close(istream, NULL, NULL);
    g_output_stream_close(ostream, NULL, NULL);
    exit(EXIT_FAILURE);
  }
  rcam_get(queue->id, 0, MIN(512 * 1024, queue->size));
}


static void dload_write_callback(GObject *source_object, GAsyncResult *res,
                                 gpointer user_data) {
  dload_queue_t **queue_el = user_data;
  dload_queue_t *dl = *queue_el;
  GError *err = NULL;
  if (!g_output_stream_write_all_finish(G_OUTPUT_STREAM(source_object), res,
                                        NULL, &err)) {
    g_error("writing download to file failed: %s", err->message);
    g_input_stream_close(istream, NULL, NULL);
    g_output_stream_close(ostream, NULL, NULL);
    exit(EXIT_FAILURE);
  }

  /* refill recv_buf */
  g_input_stream_read_async(istream, recv_buf, sizeof (recv_buf),
                            G_PRIORITY_DEFAULT, NULL, istream_read_callback,
                            NULL);

  if (current_cmd != RCAM_CODE_SEND) {
    if (dl->cur >= dl->size) {
      /* end of download -> close */
      rcam_delete_resource(dl->id);
      g_object_unref(dl->f);
      *queue_el = dl->n;
      g_slice_free(dload_queue_t, dl);
    } else {
      /* get the next part */
      rcam_get(dl->id, dl->cur, MIN(512 * 1024, dl->size - dl->cur));
    }
  }
}


void istream_read_callback(GObject *source_object, GAsyncResult *res,
                           gpointer user_data) {
  GError *err = NULL;
  gssize len = g_input_stream_read_finish(istream, res, &err);
  guint i = 0;
  gboolean recv_buf_refill_elsewhere = FALSE;

  if (len == 0 || (len == -1 && err->code == G_IO_ERROR_BROKEN_PIPE)) {
    printf("connection closed by foreign host\n");
    exit(EXIT_SUCCESS);
  } else if (len == -1) {
    g_error("reading from socket failed: (%i) %s", err->code, err->message);
    g_input_stream_close(istream, NULL, NULL);
    g_output_stream_close(ostream, NULL, NULL);
    exit(EXIT_FAILURE);
  }

  while (i < len) {
    if (current_cmd == RCAM_CODE_NONE)
      current_cmd = recv_buf[i++];
    switch (current_cmd) {
      case RCAM_CODE_VERSION:
        printf("< VERSION 0x%02x\n", recv_buf[i++]);
        /* ignore version number, this is the lowest */
        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_SEND_NAME:
        /* server side implementation, TODO */
        printf("< SEND NAME …\n");
        while (i < len) {
          if (recv_buf[i++] == '\0') {
            current_cmd = RCAM_CODE_NONE;
            break;
          }
        }
        break;
      case RCAM_CODE_SUPPORTED_FORMATS:
        if (arg_bytes_to_read == 0) {
          printf("< SUPPORTED FORMATS");
          arg_bytes_to_read = recv_buf[i++];
          /* preselect “invalid”, lowest rank */
          arg_buf[0] = sizeof (rcam_supported_formats) - 1;
        }
        for (; arg_bytes_to_read > 0; arg_bytes_to_read--) {
          guint8 fmt;

          if (i >= len)
            goto recv_buf_refill;

          fmt = recv_buf[i++];
          if (fmt < sizeof (format_extensions))
            printf(" %s", format_extensions[fmt]);
          else
            printf(" 0x%02x", fmt);

          for (guint j = 0; j < arg_buf[0]; j++) {
            /* arg_buf[0] -> loop iterates only to the best match */
            if (fmt == rcam_supported_formats[j]) {
              arg_buf[0] = j;
              break;
            }
          }
        }
        puts("");

        /* select the format */ {
          guint8 fmt = rcam_supported_formats[arg_buf[0]];
          if (fmt == RCAM_FORMAT_INVALID) {
            g_error("no suitable format found");
            g_input_stream_close(istream, NULL, NULL);
            g_output_stream_close(ostream, NULL, NULL);
            exit(EXIT_FAILURE);
          } else {
            rcam_select_format(fmt);
            current_cmd = RCAM_CODE_NONE;
          }
        }
        break;
      case RCAM_CODE_SELECT_FORMAT:
        /* server side implementation, TODO */
        printf("< SELECT FORMAT 0x%02x\n", recv_buf[i++]);
        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_ACCEPT:
        puts("< ACCEPT");
        accepted = 1;
        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_CAPTURE_STILL_IMAGE:
        /* server side implementation, TODO */
        puts("< CAPTURE STILL IMAGE");
        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_CAPTURE_FINISHED:
        if (arg_bytes_to_read == 0) {
          printf("< CAPTURE FINISHED ");
          arg_bytes_to_read = 13;
        }
        /* fill arg_buf */ {
          gsize step_size = MIN(len - i, arg_bytes_to_read);
          memcpy(&arg_buf[13 - arg_bytes_to_read], &recv_buf[i], step_size);
          arg_bytes_to_read -= step_size;
          if (arg_bytes_to_read != 0)
            goto recv_buf_refill;
          i += step_size;
        }
        /* process the args */ {
          gchar fname[14]; /* XXXXXXXX.xxxx\0 */
          guint32 id = GET_NUINT32(arg_buf, 0);
          guint8 fmt = arg_buf[4];
          guint64 size = GET_UINT64(arg_buf, 5);
          printf("0x%08x 0x%02x 0x%016lx\n", id, fmt, size);

          sprintf(fname, "%08x.%s", save_id++, format_extensions[fmt]);
          dload_init_async(&queue, id, fname, size, NULL, dload_init_callback,
                           &queue);
          current_cmd = RCAM_CODE_NONE;
        }
        break;
      case RCAM_CODE_GET:
        /* server side implementation, TODO */
        if (arg_bytes_to_read == 0) {
          printf("< GET");
          arg_bytes_to_read = 16;
        }
        /* fill arg_buf */ {
          guint step_size = MIN(len - i, arg_bytes_to_read);
          memcpy(&arg_buf[16 - arg_bytes_to_read], &recv_buf[i], step_size);
          arg_bytes_to_read -= step_size;
          if (arg_bytes_to_read != 0)
            goto recv_buf_refill;
          i += step_size;
        }
        /* process the args */ {
          guint32 id = GET_NUINT32(arg_buf, 0);
          guint64 offset = GET_UINT64(arg_buf, 4);
          guint32 length = GET_UINT32(arg_buf, 12);
          printf("0x%08x 0x%016lx 0x%08x\n", id, offset, length);

          current_cmd = RCAM_CODE_NONE;
        }
        break;
      case RCAM_CODE_SEND:
        if (arg_bytes_to_read == 0) {
          printf("< SEND ");
          arg_bytes_to_read = 16;
        }
        if (arg_bytes_to_read < 255) { /* fill arg_buf */
          guint step_size = MIN(len - i, arg_bytes_to_read);
          memcpy(&arg_buf[16 - arg_bytes_to_read], &recv_buf[i], step_size);
          arg_bytes_to_read -= step_size;
          if (arg_bytes_to_read != 0)
            goto recv_buf_refill;
          i += step_size;
          arg_bytes_to_read = 255; /* signalize, that data is already there */
        }
        if (arg_bytes_to_read == 255 && i < len) { /* process the args, only
                                                    * worth if there are still
                                                    * bytes left */
          dload_queue_t **dl; /* use a pointer to a pointer in order to be able
                               * to delete an element */
          guint32 id = GET_NUINT32(arg_buf, 0);
          guint64 offset = GET_UINT64(arg_buf, 4);
          guint32 length = GET_UINT32(arg_buf, 12);
          guint32 step_size = MIN(len - i, length);
          printf("0x%08x 0x%016lx 0x%08x <data>\n", id, offset, length);

          /* get download from the queue */
          for (dl = &queue; *dl != NULL; dl = &(*dl)->n) {
            if ((*dl)->id == id)
              break;
          }

          /* Does the response match our requests? */
          if (*dl == NULL) {
            g_error("The resource 0x%08x has not been requested!", id);
            g_input_stream_close(istream, NULL, NULL);
            g_output_stream_close(ostream, NULL, NULL);
            exit(EXIT_FAILURE);
          }
          if (offset != (*dl)->cur) {
            g_error("The offset 0x%016lx has not been requested!", offset);
            g_input_stream_close(istream, NULL, NULL);
            g_output_stream_close(ostream, NULL, NULL);
            exit(EXIT_FAILURE);
          }

          printf("starting at recv_buf[%i], reading %i\n", i, step_size);
          g_output_stream_write_all_async((*dl)->f, &recv_buf[i], step_size,
                                          G_PRIORITY_HIGH, NULL,
                                          dload_write_callback, dl);
          i += step_size;
          (*dl)->cur += step_size;
          if (offset + length == (*dl)->cur) {
            arg_bytes_to_read = 0;
            current_cmd = RCAM_CODE_NONE;
          }
          recv_buf_refill_elsewhere = TRUE;
        }
        break;
      case RCAM_CODE_DELETE_RESOURCE:
        /* server side implementation, TODO */
        if (arg_bytes_to_read == 0) {
          printf("< RESOURCE UNAVAILABLE");
          arg_bytes_to_read = 4;
        }
        /* fill arg_buf */ {
          guint step_size = MIN(len - i, arg_bytes_to_read);
          memcpy(&arg_buf[16 - arg_bytes_to_read], &recv_buf[i], step_size);
          arg_bytes_to_read -= step_size;
          if (arg_bytes_to_read != 0)
            goto recv_buf_refill;
          i += step_size;
        }
        /* process the args */ {
          printf("0x%08x\n", GET_NUINT32(arg_buf, 0));
          current_cmd = RCAM_CODE_NONE;
        }
        break;
      case RCAM_CODE_RESOURCE_UNAVAILABLE:

        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_UNSUPPORTED_FORMAT:
        printf("< UNSUPPORTED FORMAT 0x%02x\n", recv_buf[i++]);
        current_cmd = RCAM_CODE_NONE;
        break;
      case RCAM_CODE_CAPTURE_FAILED:
        puts("< CAPTURE FAILED");
        current_cmd = RCAM_CODE_NONE;
        break;
      default:
        printf("unsupported code 0x%02x\n", current_cmd);
        current_cmd = RCAM_CODE_NONE;
    }
  }
recv_buf_refill:
  if (!recv_buf_refill_elsewhere)
    g_input_stream_read_async(istream, recv_buf, sizeof (recv_buf),
                              G_PRIORITY_DEFAULT, NULL, istream_read_callback,
                              NULL);
}


GInputStream *term;
gchar term_buf[80];
void term_read_callback(GObject *source_object, GAsyncResult *res, gpointer
                        user_data) {
  if (g_input_stream_read_finish(term, res, NULL) > 0) {
    rcam_capture_still_image();
    g_input_stream_read_async(term, term_buf, 80, G_PRIORITY_DEFAULT, NULL,
                              term_read_callback, NULL);
  } else {
    /* TODO: close while operations pending? */
    g_input_stream_close(istream, NULL, NULL);
    g_output_stream_close(ostream, NULL, NULL);

    exit(EXIT_SUCCESS);
  }
}

int main(int argc, char *argv[]) {
  char *client_name = NULL;
  int err, c;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s <hostname> [<client name>]\n", argv[0]);
    return EXIT_FAILURE;
  }
  if (argc > 2)
    client_name = argv[2];

  if (rcam_connect(argv[1], client_name) < 0)
    return EXIT_FAILURE;

  term = g_unix_input_stream_new(STDIN_FILENO, TRUE);
  g_input_stream_read_async(term, term_buf, 80, G_PRIORITY_DEFAULT, NULL,
                            term_read_callback, NULL);
  puts("Entering main loop… After the connection has been accepted you can take\
 a photo using ENTER. If you want to close the connection, type CTRL-D.");

  GMainLoop *loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(loop);

  return EXIT_SUCCESS;
}

#define _FILE_OFFSET_BITS 64 /* large file support */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>
#include <arpa/inet.h> /* inet_ntop */
#include <stdint.h>

#ifdef __linux__
# include <endian.h>
# define ntohll(x) be64toh(x)
# define htonll(x) htobe64(x)
#endif


#define NODENAME  NULL    /* listen on any name/interface */
#define SRVPORT   "1708"  /* port the server listens on */
#define BACKLOG   32      /* length of the listen queue */

#define RCAM_CODE_VERSION              0x01
#define RCAM_CODE_SEND_NAME            0x10
#define RCAM_CODE_SUPPORTED_FORMATS    0x11
#define RCAM_CODE_SELECT_FORMAT        0x12
#define RCAM_CODE_ACCEPT               0x1f
#define RCAM_CODE_CAPTURE_STILL_IMAGE  0x20
#define RCAM_CODE_CAPTURE_FINISHED     0x2f
#define RCAM_CODE_GET                  0x30
#define RCAM_CODE_SEND                 0x31
#define RCAM_CODE_DELETE_RESOURCE      0x32
#define RCAM_CODE_RESOURCE_UNAVAILABLE 0xf0
#define RCAM_CODE_UNSUPPORTED_FORMAT   0xf1

#define RCAM_FORMAT_JPEG 0x00
#define RCAM_FORMAT_PNG  0x01
#define RCAM_FORMAT_TIFF 0x02
#define RCAM_FORMAT_HEIF 0x03

#define RCAM_PROTO_VERSION 1

/* test data */
#define TEST_IMG     "testimg.png"
#define TEST_IMG_FMT RCAM_FORMAT_PNG
FILE *test_img_fd = NULL;

/* test function */
static FILE *test_fopen(const char *restrict path __attribute__((unused)),
                        const char *restrict mode __attribute__((unused))) {
  FILE *f;
  if (test_img_fd != NULL)
    return test_img_fd; /* already opened the file */
  if ((f = fopen(TEST_IMG, "r")) != NULL)
    return f;
  fprintf(stderr, "Could not open file “%s”: %s\n", TEST_IMG, strerror(errno));
  exit(EXIT_FAILURE);
}

struct handle_info_s {
  uint32_t id;
  uint8_t fmt;
  FILE *f;
  uint64_t size;
  struct handle_info_s *n;
};

/* macros to extract numbers from binary stream */
#define GET_NUINT32(s,i) (*((uint32_t *) &s[i])) /* network byte order */
#define GET_UINT32(s,i) ntohl(*((uint32_t *) &s[i]))
#define GET_UINT64(s,i) ntohll(*((uint64_t *) &s[i]))

#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define MAX(a,b) ((a) > (b) ? (a) : (b))


int srv_quit = 0;
char send_buf[512 * 1024];
struct handle_info_s *resources = NULL;
uint32_t capture_id = 0;

static int tcp_listen(const char *nodename, const char *servname, int backlog) {
  int sd, reuseaddr, status;
  struct addrinfo hints, *ai, *aptr;

  /* init hints adress structure */
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = AI_PASSIVE;     /* passive mode -> server */
  hints.ai_family = AF_INET6;      /* IPv6 is backwards compatible to IPv4,
                                    * for older systems -> AF_UNSPEC */
  hints.ai_socktype = SOCK_STREAM; /* TCP socket */

  /* get the address structure(s) for the socket */
  if ((status = getaddrinfo(nodename, servname, &hints, &ai)) == 0) {
    for (aptr = ai; aptr != NULL; aptr = aptr->ai_next) {
      /* create the TCP socket */
      if ((sd = socket(aptr->ai_family, aptr->ai_socktype, aptr->ai_protocol))
          < 0)
        continue; /* error, try the next address structure */

      /* avoid “address already in use” */
      reuseaddr = 1;
      setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof (int));

      /* bind socket to socket address */
      if (bind(sd, aptr->ai_addr, aptr->ai_addrlen) == 0
          && listen(sd, backlog) >= 0)
        break; /* success! */

      /* an error occured -> close socket */
      close(sd);
    }

    /* free results list */
    freeaddrinfo(ai);

    if (aptr == NULL) {
      /* tried all address structures but none of them works
       *  -> consult errno for an error description */
      fprintf(stderr, "Cannot listen on port %s: %s\n", servname,
              strerror(errno));
      return -1;
    }
  } else {
    fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(status));
    return -1;
  }

  return sd;
}

void sig_handler(int sig __attribute__((unused))) {
  srv_quit = 1;
  return;
}

int ask_permission(int allow_wait) {
  int c;
  if (allow_wait)
    printf("Accept the connection? [y/n/w(ait)]: ");
  else
    printf("Accept the connection? [y/n]: ");
  fflush(stdout);
  for (;;) {
    /* FIXME: getchar() blocks so there is no information flow in background */
    if ((c = getchar()) < 0) {
      fputs("EOF (stdin)\n", stderr);
      return -1;
    }
    if (c == 'y')
      return 1;
    if (c == 'n')
      return 0;
    if (allow_wait && c == 'w')
      return 2;
  }
}

int rcam_accept(int client) {
  send_buf[0] = RCAM_CODE_ACCEPT;
  puts("> ACCEPT");
  if (write(client, send_buf, 1) != 1) {
    perror("Could not write to socket");
    return -1;
  }
  return 0;
}

int rcam_send_version(int client) {
  send_buf[0] = RCAM_CODE_VERSION;
  send_buf[1] = 1;
  printf("> VERSION %i\n", send_buf[1]);
  if (write(client, send_buf, 2) != 2) {
    perror("Could not write to socket");
    return -1;
  }
  return 0;
}

int rcam_send_supported_formats(int client) {
  send_buf[0] = RCAM_CODE_SUPPORTED_FORMATS;
  send_buf[1] = 1;
  send_buf[2] = TEST_IMG_FMT;
  printf("> SUPPORTED FORMATS 0x01 0x%02x\n", TEST_IMG_FMT);
  if (write(client, send_buf, 3) != 3) {
    perror("Could not write to socket");
    return -1;
  }
  return 0;
}

int rcam_capture_finished(int client) {
  char fname[13]; /* XXXXXXXX.png\0 */
  struct handle_info_s *new = malloc(sizeof (struct handle_info_s));
  if (new == NULL) {
    perror("Memory allocation failed");
    return -1;
  }

  /* init */
  new->id = capture_id;
  sprintf(fname, "%08x.png", capture_id++);
  new->f = test_fopen(fname, "w+");
  fseeko(new->f, 0, SEEK_END);
  new->size = ftello(new->f);
  new->fmt = TEST_IMG_FMT;
  /* add to available resources */
  new->n = resources;
  resources = new;

  send_buf[0] = RCAM_CODE_CAPTURE_FINISHED;
  *((uint32_t *) &send_buf[1]) = new->id;
  send_buf[5] = new->fmt;
  *((uint64_t *) &send_buf[6]) = htonll(new->size);
  printf("> CAPTURE FINISHED 0x%08x 0x%02x 0x%016lx\n", new->id, new->fmt,
         new->size);
  if (write(client, send_buf, 14) != 14) {
    perror("Could not write to socket");
    return -1;
  }

  return 0;
}

int rcam_send(int client, uint32_t resource_id, uint64_t offset,
              uint32_t length) {
  struct handle_info_s *res;
  char hdr[17];

  for (res = resources; res != NULL; res = res->n) {
    if (res->id == resource_id)
      break;
  }
  if (res == NULL) { /* resource unavailable */
    send_buf[0] = RCAM_CODE_RESOURCE_UNAVAILABLE;
    *((uint32_t *) &send_buf[1]) = resource_id;
    printf("> RESOURCE UNAVAILABLE 0x%08x\n", resource_id);
    if (write(client, send_buf, 5) != 5) {
      perror("Could not write to socket");
      return -1;
    }
    return 0;
  }

  if (offset > res->size) {
    length = 0;
  } else {
    fseeko(res->f, offset, SEEK_SET);
    length = fread(send_buf, 1, MIN(length, 512 * 1024), res->f);
    if (ferror(res->f)) {
      fprintf(stderr, "Could not read from file associated with 0x%08x: %s\n",
              res->id, strerror(errno));
      return -1;
    }
  }

  hdr[0] = RCAM_CODE_SEND;
  *((uint32_t *) &hdr[1]) = resource_id;
  *((uint64_t *) &hdr[5]) = htonll(offset);
  *((uint32_t *) &hdr[13]) = htonl(length);
  printf("> SEND 0x%08x 0x%016lx 0x%08x <data>\n", resource_id, offset, length);
  if (write(client, hdr, 17) != 17) {
    perror("Could not write to socket");
    return -1;
  }
  while (length > 0) {
    ssize_t status;
    if ((status = write(client, send_buf, length)) == -1) {
      perror("Could not write to socket");
      return -1;
    }
    length -= status;
  }

  return 0;
}

void rcam_delete_resource(uint32_t resource_id) {
  struct handle_info_s **res, *del_res;
  for (res = &resources; *res != NULL; res = &(*res)->n) {
    if ((*res)->id == resource_id)
      break;
  }
  del_res = *res;
  if (del_res == NULL)
    return; /* does not exist -> ignore */
  //fclose(delete_res->f);
  *res = del_res->n;
  free(del_res);
}

int rcam_send_error_unsupported_format(int client, uint8_t format) {
  char msg[2] = { RCAM_CODE_UNSUPPORTED_FORMAT, format };
  printf("> UNSUPPORTED FORMAT 0x%02x\n", format);
  if (write(client, msg, 2) != 2) {
    perror("Could not write to socket");
    return -1;
  }
  return 0;
}

void handle_client(int cl) {
  int c, accepted = 0;
  FILE *fp = fdopen(cl, "r");
  if (fp == NULL) {
    perror("fdopen() on socket failed");
    return;
  }

  if (rcam_send_version(cl) == -1)
    return;

  /* FIXME: fgetc() blocks, which might interfere with the blocking of
   * rcam_accept() */
  while ((c = fgetc(fp)) > 0) {
    switch (c) {
      case RCAM_CODE_VERSION:
        printf("< VERSION ");
        if ((c = fgetc(fp)) <= 0)
          goto handle_client_check_error;
        printf("%i\n", c);
        /* do nothing with the version number, this is already the oldest */
        if (accepted)
          break;

        if (rcam_send_supported_formats(cl) == -1)
          return;

        switch (ask_permission(1)) {
          case 0:
          case -1:
            return;
          case 1:
            accepted = 1;
            if (rcam_accept(cl) == -1)
              return;
            break;
        }
        break;
      case RCAM_CODE_SEND_NAME: {
        printf("< SEND NAME ");
        while ((c = fgetc(fp)) > 0)
          putchar(c);
        if (c == 0) {
          putchar('\n');
        } else {
          if (c == EOF)
            fputs("EOF\n", stderr);
          else
            perror("Could not read from socket");
          return;
        }

        if (accepted)
          break;
        switch (ask_permission(0)) {
          case 0:
          case -1:
            return;
          case 1:
            accepted = 1;
            if (rcam_accept(cl) == -1)
              return;
            break;
        }
        break;
      }
      case RCAM_CODE_SELECT_FORMAT:
        printf("< SELECT FORMAT ");
        if ((c = fgetc(fp)) < 0)
          goto handle_client_check_error;
        printf(" 0x%02x\n", c);
        if (c != TEST_IMG_FMT) {
          puts("> UNSUPPORTED FORMAT");
          rcam_send_error_unsupported_format(cl, c);
          return;
        }
        /* do nothing, we provide only one format */
        break;
      case RCAM_CODE_CAPTURE_STILL_IMAGE: {
        puts("< CAPTURE STILL IMAGE");
        if (!accepted) {
          puts("not accepted");
          return;
        }
        if (rcam_capture_finished(cl) == -1)
          return;
        break;
      }
      case RCAM_CODE_GET: {
        char buf[16];
        uint32_t id, length;
        uint64_t offset;

        printf("< GET ");
        if (fread(buf, 1, 16, fp) != 16) {
          if (feof(fp))
            fputs("EOF\n", stderr);
          else
            perror("Could not read from client");
          return;
        }
        id = GET_NUINT32(buf, 0);
        offset = GET_UINT64(buf, 4);
        length = GET_UINT32(buf, 12);
        printf("0x%08x 0x%016lx 0x%08x\n", id, offset, length);

        if (rcam_send(cl, id, offset, length) == -1)
          return;
        break;
      }
      case RCAM_CODE_DELETE_RESOURCE: {
        uint32_t id;
        printf("< DELETE RESOURCE ");
        if (fread((char *) &id, 1, 4, fp) != 4) {
          if (feof(fp))
            fputs("EOF\n", stderr);
          else
            perror("Could not read from client");
          return;
        }
        printf("0x%08x\n", id);
        rcam_delete_resource(id);
        break;
      }
      default:
        fprintf(stderr, "Error: unsupported code 0x%02x\n", c);
        return;
    }
  }
handle_client_check_error:
  if (c == EOF)
    fputs("EOF\n", stderr);
  else
    perror("Reading from socket failed");
}

int main(void) {
  int sd, client;
  socklen_t slen;
  struct sockaddr_storage sa;
  struct sigaction action;

  /* open socket */
  if ((sd = tcp_listen(NODENAME, SRVPORT, BACKLOG)) < 0)
    return EXIT_FAILURE;

  /* catch SIGTERM */
  action.sa_handler = sig_handler;
  sigemptyset(&action.sa_mask);
  action.sa_flags = 0;

  if (sigaction(SIGTERM, &action, NULL) < 0) {
    fprintf(stderr, "sigaction() failed: %s\n", strerror(errno));
    close(sd);
    return EXIT_FAILURE;
  }

  printf("Server listening on %s:%s\n", NODENAME == NULL ? "*" : NODENAME,
         SRVPORT);

  for (;;) {
    slen = sizeof (sa);

    /* accept new socket connections */
    if ((client = accept(sd, (struct sockaddr *) &sa, &slen)) < 0) {
      if (srv_quit)
        break;

      printf("accept() failed: %s\n", strerror(errno));

      continue;
    }

    /* get client ip address */
    if (sa.ss_family == AF_INET) {
      char addr[INET_ADDRSTRLEN];
      inet_ntop(AF_INET, &((struct sockaddr_in *) &sa)->sin_addr, addr,
                INET_ADDRSTRLEN);
      puts(addr);
    } else if (sa.ss_family == AF_INET6) {
      char addr[INET6_ADDRSTRLEN];
      inet_ntop(AF_INET6, &((struct sockaddr_in6 *) &sa)->sin6_addr, addr,
                INET6_ADDRSTRLEN);
      puts(addr);
    } else {
      puts("unknown");
    }

    /* handle client */
    handle_client(client);

    /* close connection */
    puts("close");
    shutdown(client, SHUT_RDWR);
    close(client);
  }

  return EXIT_SUCCESS;
}

#define _FILE_OFFSET_BITS 64 /* large file support */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <sys/socket.h>
#include <pthread.h>
#include <stdint.h>

/* FIXME: this implementation does not “listen” on TCP FIN */

#ifdef __linux__
# include <endian.h>
# define ntohll(x) be64toh(x)
# define htonll(x) htobe64(x)
#endif

#define SRVPORT "1708"

#define RCAM_CODE_VERSION              0x01
#define RCAM_CODE_SEND_NAME            0x10
#define RCAM_CODE_SUPPORTED_FORMATS    0x11
#define RCAM_CODE_SELECT_FORMAT        0x12
#define RCAM_CODE_ACCEPT               0x1f
#define RCAM_CODE_CAPTURE_STILL_IMAGE  0x20
#define RCAM_CODE_CAPTURE_FINISHED     0x2f
#define RCAM_CODE_GET                  0x30
#define RCAM_CODE_SEND                 0x31
#define RCAM_CODE_DELETE_RESOURCE      0x32
#define RCAM_CODE_RESOURCE_UNAVAILABLE 0xf0
#define RCAM_CODE_UNSUPPORTED_FORMAT   0xf1

#define RCAM_FORMAT_JPEG 0x00
#define RCAM_FORMAT_PNG  0x01
#define RCAM_FORMAT_TIFF 0x02
#define RCAM_FORMAT_HEIF 0x03

char rcam_supported_formats[] = { /* first is preferred */
  RCAM_FORMAT_PNG,
  RCAM_FORMAT_TIFF,
  RCAM_FORMAT_JPEG
};

char *format_extensions[4] = {
  "jpg",
  "png",
  "tif",
  "heif"
};

#define RCAM_PROTO_VERSION 1


struct dload_queue_s {
  uint32_t id;
  FILE *f;
  uint64_t size;
  struct dload_queue_s *n;
};

/* macros to extract numbers from binary stream */
#define GET_NUINT32(s,i) (*((uint32_t *) &s[i])) /* network byte order */
#define GET_UINT32(s,i) ntohl(*((uint32_t *) &s[i]))
#define GET_UINT64(s,i) ntohll(*((uint64_t *) &s[i]))

#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define MAX(a,b) ((a) > (b) ? (a) : (b))


int sd, length, accepted = 0;
char recv_buf[512 * 1024];
uint32_t save_id = 0;


static int tcp_connect(const char *nodename, const char *servname) {
  int sd, status;
  struct addrinfo hints, *ai, *aptr;

  /* init hints adress structure */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;     /* try all available (IPv4 and IPv6) */
  hints.ai_socktype = SOCK_STREAM; /* TCP socket */

  if ((status = getaddrinfo(nodename, servname, &hints, &ai)) == 0) {
    for (aptr = ai; aptr != NULL; aptr = aptr->ai_next) {
      /* create the TCP socket */
      if ((sd = socket(aptr->ai_family, aptr->ai_socktype, aptr->ai_protocol))
          < 0)
        continue; /* error, try the next address structure */

      if (connect(sd, aptr->ai_addr, aptr->ai_addrlen) < 0) {
        close(sd);
        continue;
      }

      break;
    }

    /* free results list */
    freeaddrinfo(ai);

    if (aptr == NULL) {
      /* tried all address structures but none of them works
       *  -> consult errno for an error description */
      fprintf(stderr, "Cannot listen on port %s: %s\n", servname,
              strerror(errno));
      return -1;
    }
  } else {
    fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(status));
    return -1;
  }

  return sd;
}

int rcam_connect(const char *server, const char *name) {
  char msg[3] = { RCAM_CODE_VERSION, RCAM_PROTO_VERSION, RCAM_CODE_SEND_NAME };
  int optval = 1;

  printf("connecting to %s...\n", server);
  if ((sd = tcp_connect(server, SRVPORT)) < 0)
    return -1;

  if (setsockopt(sd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof (int)) == -1)
    perror("setsockopt SO_KEEPALIVE failed");

  puts("> VERSION 1");
  if (name)
    printf("> SEND NAME %s\n", name);
  if (write(sd, msg, name == NULL ? 2 : 3) < 0
      || (name && write(sd, name, strlen(name) + 1) < 0)) { // +1 -> write '\0'
    perror("Writing to socket failed");
    return -1;
  }

  return 0;
}

void rcam_capture_still_image(void) {
  char msg = RCAM_CODE_CAPTURE_STILL_IMAGE;
  if (accepted == 0) {
    char msg = 0;
    fputs("Calling CAPTURE STILL IMAGE on a connection that has not been \
accepted yet is not allowed!\n", stderr);
    write(sd, &msg, 1);
    return;
  }
  puts("> CAPTURE STILL IMAGE");
  if (write(sd, &msg, 1) == 1)
    return;
  close(sd);
  perror("Writing to socket failed");
  exit(EXIT_FAILURE);
}

void rcam_get(uint32_t id, uint64_t offset, uint32_t length) {
  char msg[17];
  if (accepted == 0) {
    fputs("Calling GET on a connection that has not been accepted yet is not \
allowed!\n", stderr);
    return;
  }
  msg[0] = RCAM_CODE_GET;
  *((uint32_t *) &msg[1]) = id;
  *((uint64_t *) &msg[5]) = htonll(offset);
  *((uint32_t *) &msg[13]) = htonl(length);
  printf("> GET 0x%08x 0x%016lx 0x%08x\n", id, offset, length);
  if (write(sd, msg, 17) == 17)
    return;
  close(sd);
  perror("Writing to socket failed");
  exit(EXIT_FAILURE);
}

void rcam_delete_resource(uint32_t id) {
  char msg[5];
  if (accepted == 0) {
    fputs("Calling DELETE RESOURCE on a connection that has not been accepted \
yet is not allowed!\n", stderr);
    return;
  }
  msg[0] = RCAM_CODE_DELETE_RESOURCE;
  *((uint32_t *) &msg[1]) = id;
  printf("> DELETE RESOURCE 0x%08x\n", id);
  if (write(sd, msg, 5) == 5)
    return;
  close(sd);
  perror("Writing to socket failed");
  exit(EXIT_FAILURE);
}

static void dload_init(struct dload_queue_s **queue, uint32_t id,
                       const char *name, uint64_t size) {
  struct dload_queue_s *new = malloc(sizeof (struct dload_queue_s));
  if (new == NULL) {
    perror("Memory allocation failed");
    close(sd);
    exit(EXIT_FAILURE);
  }

  new->id = id;
  if ((new->f = fopen(name, "w")) == NULL) {
    fprintf(stderr, "Could not open file %s: %s\n", name, strerror(errno));
    close(sd);
    exit(EXIT_FAILURE);
  }
  new->size = size;
  new->n = *queue;

  *queue = new;
}

void *rcam_listen_thread(void *data __attribute__((unused))) {
  uint8_t c;
  int status;
  struct dload_queue_s *queue = NULL;

  while ((status = read(sd, &c, 1)) == 1) {
    switch (c) {
      case RCAM_CODE_VERSION:
        printf("< VERSION ");
        if (read(sd, recv_buf, 1) != 1) { /* free stream */
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        printf("%d\n", recv_buf[0]);
        /* do nothing, our version is already the oldest */
        break;
      case RCAM_CODE_SUPPORTED_FORMATS: {
        printf("< SUPPORTED FORMATS");
        char length;
        if (read(sd, &length, 1) != 1 || read(sd, recv_buf, length) != length) {
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        printf(" 0x%02x", length);
        for (int i = 0; i < length; i++)
          printf(" 0x%02x", recv_buf[i]);
        puts("");

        /* select the format */
        for (unsigned int i = 0; i < sizeof (rcam_supported_formats); i++) {
          for (int j = 0; j < length; j++) {
            if (rcam_supported_formats[i] == recv_buf[j]) {
              char out[2] = { RCAM_CODE_SELECT_FORMAT,
                              rcam_supported_formats[i] };
              if (write(sd, out, 2) == 2)
                goto found_supported_format;
              perror("Writing to socket failed");
              close(sd);
              exit(EXIT_FAILURE);
            }
          }
        }
        fputs("No supported format\n", stderr);
        return NULL;
       found_supported_format:
        break;
      }
      case RCAM_CODE_ACCEPT:
        puts("< ACCEPT");
        accepted = 1;
        break;
      case RCAM_CODE_CAPTURE_FINISHED: {
        char fname[14]; /* XXXXXXXX.xxxx\0 */
        uint32_t id;
        uint8_t fmt;
        uint64_t size;

        printf("< CAPTURE FINISHED ");
        if (read(sd, recv_buf, 13) != 13) {
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        id = GET_NUINT32(recv_buf, 0);
        fmt = recv_buf[4];
        size = GET_UINT64(recv_buf, 5);
        printf("0x%08x 0x%02x 0x%016lx\n", id, fmt, size);

        sprintf(fname, "%08x.%s", save_id++, format_extensions[fmt]);
        dload_init(&queue, id, fname, size);
        rcam_get(queue->id, 0, MIN(512 * 1024, queue->size));
        break;
      }
      case RCAM_CODE_SEND: {
        uint32_t id, length;
        uint64_t offset;
        struct dload_queue_s **dl; /* use a pointer to a pointer in order to be
                                    * able to delete an element */

        printf("< SEND ");
        /* read header */
        if (read(sd, recv_buf, 16) != 16) {
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        id = GET_NUINT32(recv_buf, 0);
        offset = GET_UINT64(recv_buf, 4);
        length = GET_UINT32(recv_buf, 12);
        printf("0x%08x 0x%016lx 0x%08x <data>\n", id, offset, length);

        /* get download from the queue */
        for (dl = &queue; *dl != NULL; dl = &(*dl)->n) {
          if ((*dl)->id == id)
            break;
        }

        /* Does the response match our requests? */
        if (*dl == NULL) {
          fprintf(stderr, "The resource 0x%08x has not been requested!\n", id);
          close(sd);
          exit(EXIT_FAILURE);
        }
        if ((off_t) offset != ftello((*dl)->f)) {
          fprintf(stderr, "The offset 0x%016lx has not been requested!\n",
                  offset);
          close(sd);
          exit(EXIT_FAILURE);
        }

        /* fetch
         *
         * This part could be much more efficient when using sendfile(), which
         * sadly does not work on all systems. */
        while (length > 0) {
          ssize_t l = read(sd, recv_buf, length);
          if (l < 0) {
            perror("Reading from socket failed");
            close(sd);
            exit(EXIT_FAILURE);
          }
          length -= l;
          if ((ssize_t) fwrite(recv_buf, 1, l, (*dl)->f) != l) {
            perror("Writing to file failed");
            close(sd);
            exit(EXIT_FAILURE);
          }
        }

        /* Are we at the end of the file? */
        offset = ftello((*dl)->f);
        if (offset >= (*dl)->size) {
          struct dload_queue_s *delete_ref = *dl;
          rcam_delete_resource(id);
          fclose(delete_ref->f);
          *dl = delete_ref->n;
          free(delete_ref);
        } else {
          /* get next part! */
          rcam_get((*dl)->id, offset, MIN(512 * 1024, queue->size - offset));
        }
        break;
      }
      case RCAM_CODE_RESOURCE_UNAVAILABLE:
        printf("< RESOURCE UNAVAILABLE ");
        if (read(sd, recv_buf, 4) != 4) {
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        printf("0x%08x\n", GET_NUINT32(recv_buf, 0));
        break;
      case RCAM_CODE_UNSUPPORTED_FORMAT:
        printf("< UNSUPPORTED FORMAT ");
        if (read(sd, recv_buf, 1) != 1) {
          perror("Reading from socket failed");
          close(sd);
          exit(EXIT_FAILURE);
        }
        printf("0x%02x\n", recv_buf[0]);
        break;
      default:
        fprintf(stderr, "Error: unsupported code 0x%02x\n", c);
        close(sd);
        exit(EXIT_FAILURE);
    }
  }
  if (status < 0) {
    perror("Reading from socket failed");
    close(sd);
    exit(EXIT_FAILURE);
  }

  return NULL;
}

int main(int argc, char *argv[]) {
  char *client_name = NULL;
  pthread_t listen_thread;
  int err, c;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s <hostname> [<client name>]\n", argv[0]);
    return EXIT_FAILURE;
  }
  if (argc > 2)
    client_name = argv[2];

  if (rcam_connect(argv[1], client_name) < 0) {
    close(sd);
    return EXIT_FAILURE;
  }

  if ((err = pthread_create(&listen_thread, NULL, rcam_listen_thread, NULL))
      < 0) {
    perror("Creating thread failed");
    close(sd);
    return EXIT_FAILURE;
  }

  puts("Entering main loop… After the connection has been accepted you can take\
 a photo using ENTER. If you want to close the connection, type CTRL-D.");
  while ((c = getchar()) >= 0)
    rcam_capture_still_image();

  close(sd);
  pthread_join(listen_thread, NULL);

  return EXIT_SUCCESS;
}

circle_resolution = 120;

cog_count = 20;
cog_length = 0.5; // 0 ... 1; 1 means no space between cogs
cog_width = 0.5;
inner_circle_diameter = 13;
outer_circle_diameter = 20;
height = 6;

linear_extrude(height = height) {
    difference() {
        union() {
            difference() {
                circle(d = outer_circle_diameter, $fn = circle_resolution);
                circle(d = inner_circle_diameter, $fn = circle_resolution);
            }
            
            for (i = [0:cog_count - 1]) {
                rotate(i * 360 / cog_count, [0, 0, 1]) translate([0, inner_circle_diameter / 2, 0]) {
                    square([inner_circle_diameter * PI / cog_count * cog_length, cog_width * 2], true);
                }
            }
            
            for (i = [0:cog_count - 1]) {
                rotate(i * 360 / cog_count, [0, 0, 1]) translate([0, outer_circle_diameter / 2, 0]) {
                    square([outer_circle_diameter * PI / cog_count * cog_length, cog_width * 2], true);
                }
            }
        }
        translate([0, -(outer_circle_diameter / 2 + cog_width)]) square([outer_circle_diameter / 2 + cog_width, outer_circle_diameter + cog_width * 2], false);
    }
}
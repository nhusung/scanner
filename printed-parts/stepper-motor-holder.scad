m_hole_outer_distance = 33.5;
m_hole_diameter = 2.5;
m_hole_distance = m_hole_outer_distance - m_hole_diameter;
m_front_plate_size = 42;
m_front_plate_circle_diameter = 22;
m_front_plate_corner_radius = (m_front_plate_size - m_hole_distance) / 2;
m_front_plate_height = 2;

screw_head_diameter = 5.3;

ft_brick_edge_length = 15;
ft_big_brick_height = ft_brick_edge_length * 2;
ft_axis_diameter = 4;
ft_brick_hole_open_width = 3;
ft_gear_cog_depth = 3;
ft_m_gear_diameter = ft_brick_edge_length * 2;

m_axis_height = ft_big_brick_height - ft_axis_diameter / 2;
brick_height = m_axis_height - m_front_plate_size / 2;

module m_plate_2d() {
    difference() {
        translate([0, -0.5])
            square([m_front_plate_size, m_front_plate_size + 1], true);
        
        // middle gap
        circle(d = m_front_plate_circle_diameter, $fn = 120);
        translate([0, m_front_plate_size / 4, 0])
            square([m_front_plate_circle_diameter, m_front_plate_size / 2], true);
        
        // holes
        translate([m_hole_distance / 2, m_hole_distance / 2])
            circle(d = m_hole_diameter + 0.5, $fn = 60);
        translate([-m_hole_distance / 2, m_hole_distance / 2])
            circle(d = m_hole_diameter + 0.5, $fn = 60);
        translate([m_hole_distance / 2, -m_hole_distance / 2])
            circle(d = m_hole_diameter + 0.5, $fn = 60);
        translate([-m_hole_distance / 2, -m_hole_distance / 2])
            circle(d = m_hole_diameter + 0.5, $fn = 60);
        
        // rounded corners
        rounded_corner_translation = m_front_plate_size / 2 - m_front_plate_corner_radius;
        for (angle = [0, 90]) {
            rotate([0, 0, angle]) {
                translate([rounded_corner_translation, rounded_corner_translation]) {
                    difference() {
                        square(m_front_plate_corner_radius);
                        circle(m_front_plate_corner_radius, $fn = 120);
                    }
                }
            }
        }
    }
}

module ft_holder() {
    difference() {
        cylinder(ft_brick_edge_length, d = ft_axis_diameter, $fn = 60);
        translate([0, -ft_axis_diameter / 4, ft_brick_edge_length / 2])
            cube([ft_axis_diameter, ft_axis_diameter / 2, ft_brick_edge_length], true);
    }
    translate([0, ft_axis_diameter / 4, ft_brick_edge_length / 2])
        cube([ft_brick_hole_open_width, ft_axis_diameter / 2, ft_brick_edge_length], true);
}

module special_brick() {
    // "special" brick
    translate([0, brick_height / 2, ft_brick_edge_length / 2])
        cube([ft_brick_edge_length * 3, brick_height, ft_brick_edge_length], true);
        
    translate([ft_brick_edge_length, -ft_axis_diameter / 2])
        ft_holder();
    translate([-ft_brick_edge_length, -ft_axis_diameter / 2])
        ft_holder();
}

module connector() {
    difference() {
        polyhedron([
                [ft_brick_edge_length * 3 / 2, brick_height, 0],
                [-ft_brick_edge_length * 3 / 2, brick_height, 0],
                [ft_brick_edge_length * 3 / 2, brick_height, ft_brick_edge_length],
                [-ft_brick_edge_length * 3 / 2, brick_height, ft_brick_edge_length],
                [m_front_plate_size / 2, m_axis_height - m_hole_distance / 2, 0],
                [-m_front_plate_size / 2, m_axis_height - m_hole_distance / 2, 0],
                [m_front_plate_size / 2, m_axis_height - m_hole_distance / 2, m_front_plate_height],
                [-m_front_plate_size / 2, m_axis_height - m_hole_distance / 2, m_front_plate_height]
            ], [
                [0, 1, 3, 2], // bottom
                [0, 2, 6, 4], // right
                [5, 7, 3, 1], // left
                [2, 3, 7, 6], // front
                [4, 5, 1, 0], // back
                [6, 7, 5, 4], // top
        ]);
        
        
        translate([0, m_axis_height, m_front_plate_height / 2])
            cube([m_front_plate_size, m_front_plate_size, m_front_plate_height], true);
        
        translate([m_hole_distance / 2, m_axis_height - m_hole_distance / 2])
            cylinder(ft_brick_edge_length, d = screw_head_diameter + 1, $fn = 60);
        translate([-m_hole_distance / 2, m_axis_height - m_hole_distance / 2])
            cylinder(ft_brick_edge_length, d = screw_head_diameter + 1, $fn = 60);
    }
}

module stepper_motor_holder() {
    translate([0, m_axis_height])
        linear_extrude(m_front_plate_height, convexity = 10) m_plate_2d();
            
    special_brick();
    
    connector();
}

stepper_motor_holder();
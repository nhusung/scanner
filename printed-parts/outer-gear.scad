circle_resolution = 120;

module flat_inner_gear(cog_count, cog_length, cog_outer_width, cog_width, inner_circle_diameter, outer_circle_diameter) {
    difference() {
        circle(d = outer_circle_diameter, $fn = circle_resolution);
        circle(d = inner_circle_diameter, $fn = circle_resolution);
    }
    
    /* inner cogs */
    for (i = [0:cog_count - 1]) {
        rotate(i * 360 / cog_count, [0, 0, 1]) translate([0, inner_circle_diameter / 2, 0]) {
            square([inner_circle_diameter * PI / cog_count * cog_length, cog_width * 2], true);
        }
    }
    
    /* outer cogs */
    for (i = [0:cog_count - 1]) {
        rotate(i * 360 / cog_count, [0, 0, 1]) translate([0, outer_circle_diameter / 2, 0]) {
            square([outer_circle_diameter * PI / cog_count * cog_length, cog_outer_width * 2], true);
        }
    }
}

module flat_outer_cog() {
    polygon([[-1.2, -3], [-1.2, 1.5], [-0.7, 3], [0.7, 3], [1.2, 1.5], [1.2, -3]]);
}

module flat_outer_gear(cog_count, cog_length, cog_width, inner_circle_diameter, outer_circle_diameter) {
    difference() {
        circle(d = outer_circle_diameter, $fn = circle_resolution);
        circle(d = inner_circle_diameter, $fn = circle_resolution);
    }
    
    for (i = [0:cog_count - 1]) {
        rotate(i * 360 / cog_count, [0, 0, 1]) translate([0, outer_circle_diameter / 2, 0]) {
            flat_outer_cog();
        }
    }
}

linear_extrude(height = 6) {
    difference() {
        flat_outer_gear(20, 0.5, 2, 15, 27);
        flat_inner_gear(20, 0.4, 1, 0.3, 13, 20);
    }
}
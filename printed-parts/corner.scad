// Eckverbinder

w = 45; // Schenkellänge der Grunfläche vor Abschneiden
h = 2.5; // Bodenplatte
h2 = 10; // Höhe Klemmung
d = 2; // Wandstärke Klemmung
d2 = 4; // Dicke des Plexiglases
$fn = 32;

module corner() { // 4x
  intersection() {
    union() {
      translate([w, 0, 0]) 
        rotate([0, 0, 45])
          difference() {
            rotate([0, 0, 45]) cube([w, w, h]);
            translate([0, 0, -1]) cube([w*2,w*2,h+2]);
          };
      cube([w, d, h+h2]); 
      translate([d+d2, d+d2, 0]) cube([w, d, h+h2]);
      cube([d, w, h+h2]);
      translate([d+d2, d+d2, 0]) cube([d, w, h+h2]);
    };
    translate([0, 0, -1]) cube([w-(d+d2+d),w-(d+d2+d),h+h2+2]);
  };  
}

cylinder(0.6, 10, 10); // Teller
corner();
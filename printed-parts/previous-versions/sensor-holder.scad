screw_hole_diameter = 2.5;
screw_head_diameter = 5.4;

// sensor
s_height = 13.5 + 0.5;
s_part_height = 4.5;
s_depth = 10.5;
s_gap_depth = 8.3 + 0.5;
s_space_before_gap = s_depth - s_gap_depth;

// board
b_thickness = 1.5;
b_width = 20.6 + 0.5;
b_hole_distance = 2.54;

contact_height = 2 + 0.5;
contacts_width = 5 * b_hole_distance;

ft_brick_edge_length = 15;
ft_axis_diameter = 4;
ft_brick_hole_open_width = 3;

difference() {
    cube([ft_brick_edge_length * 2, ft_brick_edge_length, ft_brick_edge_length]);
    
    upper_space = s_depth - s_gap_depth + b_thickness;
    translate([0, 0, ft_brick_edge_length - upper_space])
        cube([b_width, ft_brick_edge_length, upper_space]);
    
    translate([(b_width - contacts_width) / 2, 0, ft_brick_edge_length - upper_space - contact_height])
        cube([contacts_width, ft_brick_edge_length, contact_height]);
    
    // screw holes
    translate([(b_width - b_hole_distance * 6) / 2, ft_brick_edge_length - s_height / 2 + b_hole_distance, 0])
        cylinder(ft_brick_edge_length, d = screw_hole_diameter, $fn = 60);
    translate([(b_width + b_hole_distance * 6) / 2, ft_brick_edge_length - s_height / 2 + b_hole_distance, 0])
        cylinder(ft_brick_edge_length, d = screw_hole_diameter, $fn = 60);
    
    // ft hole
    translate([ft_brick_edge_length * 1.5, ft_axis_diameter / 2])
        cylinder(ft_brick_edge_length, d = ft_axis_diameter, $fn = 60);
    translate([ft_brick_edge_length + (ft_brick_edge_length - ft_brick_hole_open_width) / 2, 0])
        cube([ft_brick_hole_open_width, ft_axis_diameter / 2, ft_brick_edge_length]);
    
    // ft hole
    translate([ft_brick_edge_length * 1.5, ft_brick_edge_length - ft_axis_diameter / 2])
        cylinder(ft_brick_edge_length, d = ft_axis_diameter, $fn = 60);
    translate([ft_brick_edge_length + (ft_brick_edge_length - ft_brick_hole_open_width) / 2, ft_brick_edge_length - ft_axis_diameter / 2])
        cube([ft_brick_hole_open_width, ft_axis_diameter / 2, ft_brick_edge_length]);
}
// ft nose
translate([ft_brick_edge_length, 0, 0]) {
    translate([ft_brick_edge_length + ft_axis_diameter / 2, ft_brick_edge_length / 2]) {
        difference() {
            cylinder(ft_brick_edge_length, d = ft_axis_diameter - 0.1, $fn = 60);
            translate([(ft_axis_diameter - 0.1) / 4, 0, ft_brick_edge_length / 2])
                cube([(ft_axis_diameter - 0.1) / 2, ft_axis_diameter, ft_brick_edge_length], center = true);
        }
    }
    translate([ft_brick_edge_length, (ft_brick_edge_length - ft_brick_hole_open_width - 0.1) / 2])
        cube([ft_axis_diameter / 2, ft_brick_hole_open_width - 0.1, ft_brick_edge_length]);
}

translate([0, ft_brick_edge_length, 0]) {
    difference() {
        cube(ft_brick_edge_length);
        
        // hole
        translate([ft_axis_diameter / 2, ft_brick_edge_length / 2])
            cylinder(ft_brick_edge_length, d = ft_axis_diameter, $fn = 60);
        translate([0, (ft_brick_edge_length - ft_brick_hole_open_width) / 2])
            cube([ft_axis_diameter / 2, ft_brick_hole_open_width, ft_brick_edge_length]);
    }
    
    // nose
    translate([ft_brick_edge_length + ft_axis_diameter / 2, ft_brick_edge_length / 2]) {
        difference() {
            cylinder(ft_brick_edge_length, d = ft_axis_diameter - 0.1, $fn = 60);
            translate([(ft_axis_diameter - 0.1) / 4, 0, ft_brick_edge_length / 2])
                cube([(ft_axis_diameter - 0.1) / 2, ft_axis_diameter, ft_brick_edge_length], center = true);
        }
    }
    translate([ft_brick_edge_length, (ft_brick_edge_length - ft_brick_hole_open_width - 0.1) / 2])
        cube([ft_axis_diameter / 2, ft_brick_hole_open_width - 0.1, ft_brick_edge_length]);
}
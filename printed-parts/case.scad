PLAT = [99.75, 67.30, 1.50];

LOCH_DIST = 2.54;
LOCH_N = [39, 26];
LOCH_R = (11.20 - 4 * LOCH_DIST) / 2;
function loch_pos(x, y) = [(x - 1) * LOCH_DIST + LOCH_R + 1.10, (y - 1) * LOCH_DIST + LOCH_R + 0.80];
module loecher() {
  for (x = [1 : LOCH_N[0]], y = [1 : LOCH_N[1]]) {
    translate(loch_pos(x, y))
      cylinder(10, r = LOCH_R, center = true, $fn = 10);
  }
}

HOHLB = [14.40, 8.80, 10.85];
HOHLB_POS = [-6.90, PLAT[1] - HOHLB[1] - 10.10 - 1.20, PLAT[2]];
module hohlbuchse() {
  color("DimGrey", 1)
    translate(HOHLB_POS)
      cube(HOHLB);
}

LED_3MM_L = 5.40;
LED_3MM_D1 = 3.00;
LED_3MM_R1 = LED_3MM_D1 / 2;
LED_3MM_D2 = 3.85;
LED_3MM_R2 = LED_3MM_D2 / 2;
LED_3MM_RAND_D = 1.05;
module led_3mm() {
  translate([0, 0, LED_3MM_L - LED_3MM_R1])
    sphere(d = LED_3MM_D1, $fn = 30);
  translate([0, 0, LED_3MM_L - LED_3MM_R1 - 3])
    cylinder(3, d = LED_3MM_D1, $fn = 30);
  cylinder(LED_3MM_RAND_D, d = LED_3MM_D2, $fn = 30);
}

LED_MOTOR_POWER_POS = [LED_3MM_L - 8.00, 39.00 + LED_3MM_R2, 4.20 - LED_3MM_R2];
module led_motor_power() {
  translate(LED_MOTOR_POWER_POS)
    rotate([0, -90, 0])
      color("Red", 1)
        led_3mm();
}

LED_USB_POWER_POS = [LED_3MM_L - 8.00, 26.95 - LED_3MM_R1, 4.20 - LED_3MM_R2];
module led_usb_power() {
  translate(LED_USB_POWER_POS)
    rotate([0, -90, 0])
      color("Red", 1)
        led_3mm();
}

USB = [16.45, 12.00, 11.00];
USB_POS = [-5.90, 9.15, PLAT[2]];
module usbbuchse() {
  color("Silver", 1)
    translate(USB_POS)
      cube(USB);
}

FT232 = [21.00, 23.55, 10.85 + 0.85];
FT232_POS = [14.50, 13.40, PLAT[2]];
module ft232rl() {
  color("Green", 1)
    translate(FT232_POS)
      cube(FT232);
}

LED_TX_POS = [21.70 + LED_3MM_R1, LED_3MM_L - 10.6, 4.40 - LED_3MM_R2];
module led_tx() {
  translate(LED_TX_POS)
    rotate([90, 0, 0])
      color("LimeGreen", 1)
        led_3mm();
}

LED_RX_POS = [30.10 - LED_3MM_R1, LED_3MM_L - 10.6, 4.40 - LED_3MM_R2];
module led_rx() {
  translate(LED_RX_POS)
    rotate([90, 0, 0])
      color("Red", 1)
        led_3mm();
}

UC = [36.75, 10.15, 8.45];
UC_POS = [38.70, 33.50, PLAT[2]];
module atmega328p() {
  color("DimGray", 1)
    translate(UC_POS)
      cube(UC);
}

TASTER = [6.20, 6.20, 3.65];
TASTER_KNOPF_D = 3.50;
TASTER_KNOPF_H = 4.85;
module taster(x, y) {
  mitte = loch_pos(x, y);
  color("Silver", 1)
    translate([mitte[0] - TASTER[0] / 2, mitte[1] - TASTER[0] / 2, PLAT[2]])
      cube(TASTER);
  color("Maroon", 1)
    translate([mitte[0], mitte[1], TASTER_KNOPF_H - 1 + PLAT[2]])
      cylinder(2, d = TASTER_KNOPF_D, center = true, $fn = 20);
}

ULN2004A = [20.35, 10.00, 7.95];
ULN2004A_POS = [61.45, 15.75, PLAT[2]];
module uln2004a() {
  color("DimGray", 1)
    translate(ULN2004A_POS)
      cube(ULN2004A);
}

TRAN = [10.35, 4.50, 19.40];
TRAN_POS = [50.30, 5.10, PLAT[2]];
module transistor() {
  color("DimGray", 1)
    translate(TRAN_POS)
      cube(TRAN);
}

LED_5MM_L = 8.75;
LED_5MM_D1 = 5.00;
LED_5MM_R1 = LED_5MM_D1 / 2;
LED_5MM_D2 = 5.70;
LED_5MM_R2 = LED_5MM_D2 / 2;
LED_5MM_RAND_D = 1.05;
module led_5mm() {
  translate([0, 0, LED_5MM_L - LED_5MM_R1])
    sphere(d = LED_5MM_D1, $fn = 30);
  translate([0, 0, LED_5MM_L - LED_5MM_R1 - 6])
    cylinder(6, d = LED_5MM_D1, $fn = 30);
  cylinder(LED_5MM_RAND_D, d = LED_5MM_D2, $fn = 30);
}

LED_STATUS_POS = [74.15 + LED_5MM_R1, LED_5MM_L - 9.95, 3.40 + PLAT[2] - LED_5MM_R2];
module led_status() {
  translate(LED_STATUS_POS)
    rotate([90, 0, 0])
      color("Gold", 1)
        led_5mm();
}

module loetpunkt(x, y) {
  translate(loch_pos(x, y))
    square(2.5, true);
}
module loetpunkte() {
  color("Silver", 1.0) {
    translate([0, 0, -3]) {
      linear_extrude(3) {
        loetpunkt(9, 1);
        loetpunkt(9, 2);
        loetpunkt(10, 1);
        loetpunkt(10, 2);
        loetpunkt(11, 1);
        loetpunkt(11, 2);
        loetpunkt(12, 1);
        loetpunkt(12, 2);

        loetpunkt(16, 2);
        
        loetpunkt(20, 1);
        loetpunkt(20, 2);
        loetpunkt(21, 2);
        loetpunkt(22, 2);
        loetpunkt(23, 2);
        loetpunkt(24, 1);
        loetpunkt(24, 2);
        loetpunkt(25, 2);
        
        loetpunkt(29, 2);
        loetpunkt(30, 2);
        loetpunkt(30, 1);
        loetpunkt(31, 1);
        loetpunkt(32, 1);
        
        
        loetpunkt(2, 4);
        loetpunkt(2, 7);
        loetpunkt(2, 8);
        loetpunkt(2, 9);
        loetpunkt(1, 10);
        loetpunkt(2, 10);
        loetpunkt(1, 11);
        loetpunkt(1, 12);
        
        loetpunkt(1, 16);
        loetpunkt(2, 16);
        loetpunkt(1, 17);
        loetpunkt(2, 17);
        loetpunkt(2, 18);
        loetpunkt(1, 19);
        loetpunkt(2, 19);
        loetpunkt(1, 20);
        loetpunkt(1, 21);
      }
    }
  }
  
  color("Red", 1)
    translate([60, 30, -5])
      cube([2, 10, 5]);
}

module platine() {
  color("Tan", 1) {
    difference() {
      cube(PLAT);
      //loecher();
    }
  }
  loetpunkte();
  
  hohlbuchse();
  led_motor_power();
  led_usb_power();
  usbbuchse();
  ft232rl();
  led_tx();
  led_rx();
  taster(14, 18);
  atmega328p();
  taster(18, 8);
  uln2004a();
  transistor();
  led_status();
}


WANDS = 2;
ABS_V = -LED_RX_POS[1] + LED_3MM_RAND_D + 0.3;
ABS_H = 1;
ABS_L = -HOHLB_POS[0] - WANDS - 0.1;
ABS_R = 1;
ABS_O = TRAN[2] + 1;
ABS_U = 6;
SPIEL = 0.3;
HALTER_ABS = 0;
HALTER_D = WANDS;
HALTER_L = 5;
ZAPFKLOTZ_H = 12;
ZAPFKLOTZ_L = 6;
ZAPFEN_D = 3.3;
ZAPFEN_SPIEL = 0.1;
STIFT_POS = loch_pos(18, 8);
STIFT_D = 5;
STIFT_FLANSCH_H = 2;
STIFT_KNOPF_H = 1;
STIFT_H = ABS_O - TASTER_KNOPF_H + WANDS + STIFT_KNOPF_H;
FUEHRUNG_D1 = STIFT_D + 1;
FUEHRUNG_D2 = FUEHRUNG_D1 + 4;
FUEHRUNG_H = 12;
SCHRIFT = "Cantarell:style=Bold";
SCHRIFT_TIEFE = 0.2;

module ausschnitt_cube(seite, dim, pos, spiel) {
  if (seite == "LINKS") {
    translate([-(WANDS + ABS_L + 1), pos[1] - spiel, pos[2] - spiel])
      cube([WANDS + 2, dim[1] + 2 * spiel, dim[2] + 2 * spiel]);
  } else if (seite == "HINTEN") {
    translate([pos[0] - spiel, PLAT[1] + ABS_H - 1, pos[2] - spiel])
      cube([dim[0] + 2 * spiel, WANDS + 2, dim[2] + 2 * spiel]);
  }
}

module ausschnitt_cylinder(seite, r, pos, spiel) {
  if (seite == "LINKS") {
    translate([-(WANDS + ABS_L + 1), pos[1], pos[2]])
      rotate([0, 90, 0])
        cylinder(WANDS + 2, r = r + spiel);
  } else if (seite == "VORNE") {
    translate([pos[0], -(WANDS + ABS_V + 1), pos[2]])
      rotate([-90, 0, 0])
        cylinder(WANDS + 2, r = r + spiel);
  } else if (seite == "OBEN") {
    translate([pos[0], pos[1], PLAT[2] + ABS_O - 1])
      cylinder(WANDS + 2, r = r + spiel);
  }
}

module beschriftung(seite, x, y) {
  if (seite == "LINKS") {
    translate([SCHRIFT_TIEFE - ABS_L - WANDS, x, y])
      rotate([90, 0, -90])
        linear_extrude(SCHRIFT_TIEFE * 2)
          children();
  } else if (seite == "VORNE") {
    translate([x, SCHRIFT_TIEFE - ABS_V - WANDS, y])
      rotate([90, 0, 0])
        linear_extrude(SCHRIFT_TIEFE * 2)
          children();
  } else if (seite == "OBEN") {
    translate([x, y, PLAT[2] + ABS_O + WANDS - SCHRIFT_TIEFE])
      linear_extrude(SCHRIFT_TIEFE * 2)
        children();
  }
}

module halter(seite, start, ende) {
  breite = ende - start;
  w_2 = WANDS / 2;
  h_m = PLAT[2] + 2 * HALTER_ABS + HALTER_D;
  tz_o = PLAT[2] + HALTER_ABS;
  tz_m = -(HALTER_D / 2 + HALTER_ABS);
  tz_u = -(HALTER_D + HALTER_ABS);
  
  if (seite == "LINKS") {
    tx = -ABS_L - w_2;
    l_ou = ABS_L + HALTER_L + w_2;
    translate([tx, start, tz_o])
      cube([l_ou, breite, HALTER_D]);
    translate([tx, start, tz_m])
      cube([ABS_L + w_2 - HALTER_ABS, breite, h_m]);
    translate([tx, start, tz_u])
      cube([l_ou, breite, HALTER_D]);
  } else if (seite == "VORNE") {
    ty = -ABS_V - w_2;
    l_ou = ABS_V + HALTER_L + w_2;
    translate([start, ty, tz_o])
      cube([breite, l_ou, HALTER_D]);
    translate([start, ty, tz_m])
      cube([breite, ABS_V + w_2 - HALTER_ABS, h_m]);
    translate([start, ty, tz_u])
      cube([breite, l_ou, HALTER_D]);
  } else if (seite == "RECHTS") {
    tx_ou = PLAT[0] - HALTER_L;
    l_ou = ABS_R + HALTER_L + w_2;
    translate([tx_ou, start, tz_o])
      cube([l_ou, breite, HALTER_D]);
    translate([PLAT[0] + HALTER_ABS, start, tz_m])
      cube([ABS_R + w_2 - HALTER_ABS, breite, h_m]);
    translate([tx_ou, start, tz_u])
      cube([l_ou, breite, HALTER_D]);
  } else if (seite == "HINTEN") {
    ty_ou = PLAT[1] - HALTER_L;
    l_ou = ABS_H + HALTER_L + w_2;
    translate([start, ty_ou, tz_o])
      cube([breite, l_ou, HALTER_D]);
    translate([start, PLAT[1] + HALTER_ABS, tz_m])
      cube([breite, ABS_H + w_2 - HALTER_ABS, h_m]);
    translate([start, ty_ou, tz_u])
      cube([breite, l_ou, HALTER_D]);
  }
}

module zapfklotz() {
  difference() {
    cube(ZAPFKLOTZ_L);
    translate([-1, ZAPFKLOTZ_L / 2, ZAPFKLOTZ_L / 2])
      rotate([0, 90, 0])
        cylinder(ZAPFKLOTZ_L + 2, d = ZAPFEN_D + SPIEL, $fn = 20);
  }
}

module polung() {
  scale(1.5) {
    translate([-2.7, 0])
      square([1.5, 0.4], true);
    difference() {
      circle(d = 3, $fn = 20);
      circle(d = 2.2, $fn = 20);
      translate([0, -2])
        square(4);
    }
    circle(d = 1, $fn = 20);
    translate([1.8, 0]) {
      square([1.5, 0.4], true);
      square([0.4, 1.5], true);
    }
  }
}

module links() {
  difference() {
    translate([-(WANDS + ABS_L), -ABS_V, -ABS_U])
      cube([WANDS, ABS_H + PLAT[1] + ABS_V, ABS_U + PLAT[2] + ABS_O]);
    
    ausschnitt_cube("LINKS", HOHLB, HOHLB_POS, SPIEL);
    ausschnitt_cylinder("LINKS", LED_3MM_R1, LED_MOTOR_POWER_POS, SPIEL, $fn = 20);
    ausschnitt_cylinder("LINKS", LED_3MM_R1, LED_USB_POWER_POS, SPIEL, $fn = 20);
    ausschnitt_cube("LINKS", USB, USB_POS, SPIEL);
    
    beschriftung("LINKS", HOHLB_POS[1] + HOHLB[1] + 5, HOHLB_POS[2] + HOHLB[2] / 2)
      polung();
    
    beschriftung("LINKS", 49.5, -3.5)
      text("Stromversorgung", font = SCHRIFT, size = 3, $fn = 32);
  }
  halter("LINKS", 1, 7);
  halter("LINKS", 31, 38);
  halter("LINKS", 58, 66);
  
  translate([-(ABS_L + WANDS / 2), ZAPFKLOTZ_L / 2 - ABS_V, ZAPFKLOTZ_L / 2 + ZAPFKLOTZ_H])
    rotate([0, 90, 0])
      cylinder(ZAPFKLOTZ_L + WANDS / 2, d = ZAPFEN_D, $fn = 20);
  translate([-(ABS_L + WANDS / 2), PLAT[1] + ABS_H - ZAPFKLOTZ_L / 2, ZAPFKLOTZ_L / 2 + ZAPFKLOTZ_H])
    rotate([0, 90, 0])
      cylinder(ZAPFKLOTZ_L + WANDS / 2, d = ZAPFEN_D, $fn = 20);
}

module vorne() {
  difference() {
    translate([-(WANDS + ABS_L), -(ABS_V + WANDS), -ABS_U])
      cube([ABS_L + PLAT[0] + ABS_R + 2 * WANDS, WANDS, ABS_U + PLAT[2] + ABS_O]);
    
    ausschnitt_cylinder("VORNE", LED_3MM_R1, LED_RX_POS, SPIEL, $fn = 20);
    ausschnitt_cylinder("VORNE", LED_3MM_R1, LED_TX_POS, SPIEL, $fn = 20);
    ausschnitt_cylinder("VORNE", LED_5MM_R1, LED_STATUS_POS, SPIEL, $fn = 20);
    
    beschriftung("VORNE", 20.2, -3.8)
      text("TX RX", font = SCHRIFT, size = 3, $fn = 32);
    beschriftung("VORNE", 70.5, -3.8)
      text("Status", font = SCHRIFT, size = 3, $fn = 32);
  }
  halter("VORNE", 6, 16);
  halter("VORNE", 84, 94);
  
  translate([-ABS_L, -ABS_V, ZAPFKLOTZ_H])
    zapfklotz();
  translate([PLAT[0] + ABS_H - ZAPFKLOTZ_L, -ABS_V, ZAPFKLOTZ_H])
    zapfklotz();
}

module rechts() {
  translate([PLAT[0] + ABS_R, -ABS_V, -ABS_U])
    cube([WANDS, ABS_H + PLAT[1] + ABS_V, ABS_U + PLAT[2] + ABS_O]);
  halter("RECHTS", 1, 11);
  halter("RECHTS", 56, 66);
  
  translate([PLAT[0] + ABS_R - ZAPFKLOTZ_L, ZAPFKLOTZ_L / 2 - ABS_V, ZAPFKLOTZ_L / 2 + ZAPFKLOTZ_H])
    rotate([0, 90, 0])
      cylinder(ZAPFKLOTZ_L + WANDS / 2, d = ZAPFEN_D, $fn = 20);
  translate([PLAT[0] + ABS_R - ZAPFKLOTZ_L, PLAT[1] + ABS_H - ZAPFKLOTZ_L / 2, ZAPFKLOTZ_L / 2 + ZAPFKLOTZ_H])
    rotate([0, 90, 0])
      cylinder(ZAPFKLOTZ_L + WANDS / 2, d = ZAPFEN_D, $fn = 20);
}

module hinten() {
  difference() {
    translate([-(WANDS + ABS_L), PLAT[1] + ABS_H, -ABS_U])
      cube([ABS_L + PLAT[0] + ABS_R + 2 * WANDS, WANDS, ABS_U + PLAT[2] + ABS_O]);
    
    ausschnitt_cube("HINTEN", [40, 0, 8], [(PLAT[0] - 40) / 2, 0, ABS_O + PLAT[2] - 7], 0);
  }
  halter("HINTEN", 6, 16);
  halter("HINTEN", 84, 94);

  translate([-ABS_L, PLAT[1] + ABS_H - ZAPFKLOTZ_L, ZAPFKLOTZ_H])
    zapfklotz();
  translate([PLAT[0] + ABS_R - ZAPFKLOTZ_L, PLAT[1] + ABS_H - ZAPFKLOTZ_L, ZAPFKLOTZ_H])
    zapfklotz();
}

module oben() {
  2_w = 2 * WANDS;
  difference() {
    union() {
      translate([-(ABS_L + WANDS), -(ABS_V + WANDS), PLAT[2] + ABS_O])
        cube([ABS_L + PLAT[0] + ABS_R + 2_w, ABS_V + PLAT[1] + ABS_H + 2_w, WANDS]);
      translate([STIFT_POS[0], STIFT_POS[1], PLAT[2] + ABS_O - FUEHRUNG_H])
        cylinder(FUEHRUNG_H + WANDS / 2, d = FUEHRUNG_D2, $fn = 30);
    }
    
    translate([STIFT_POS[0], STIFT_POS[1], PLAT[2] + ABS_O - FUEHRUNG_H - 1])
      cylinder(FUEHRUNG_H + WANDS + 2, d = FUEHRUNG_D1, $fn = 30);
    
    beschriftung("OBEN", 50, STIFT_POS[1])
      text("Notaus", font = SCHRIFT, size = 5, $fn = 32, valign = "center");
    beschriftung("OBEN", 60, 55)
      text("SCANNER", font = SCHRIFT, size = 5, $fn = 32);
  }
  
  translate([-ABS_L, -ABS_V, PLAT[2] + ABS_O - 2])
    cube([4, 4, 2]);
  translate([PLAT[0] + ABS_R - 4, -ABS_V, PLAT[2] + ABS_O - 2])
    cube([4, 4, 2]);
  translate([PLAT[0] + ABS_R - 4, PLAT[1] + ABS_H - 4, PLAT[2] + ABS_O - 2])
    cube([4, 4, 2]);
  translate([-ABS_L, PLAT[1] + ABS_H - 4, PLAT[2] + ABS_O - 2])
    cube([4, 4, 2]);
}

module stift() {
  cylinder(STIFT_FLANSCH_H, d = FUEHRUNG_D2, $fn = 40);
  translate([0, 0, STIFT_FLANSCH_H / 2])
    cylinder(STIFT_H - STIFT_FLANSCH_H / 2, d = STIFT_D, $fn = 40);
}

/* Modi:
 *   DESIGN
 *   DRUCK LINKS
 *   DRUCK VORNE
 *   DRUCK RECHTS
 *   DRUCK HINTEN
 *   DRUCK OBEN
 *   DRUCK STIFT
 */
MOD = "DRUCK OBEN";

if (MOD == "DESIGN") {
  platine();
  
  links();
  vorne();
  rechts();
  hinten();
  oben();
  translate([STIFT_POS[0], STIFT_POS[1], TASTER_KNOPF_H + PLAT[2]])
    stift();
} else if (MOD == "DRUCK LINKS") {
  rotate([0, -90, -90])
    translate([ABS_L + WANDS, ABS_V, ABS_U])
      links();
  cylinder(0.6, r = 10);
  translate([0, 28, 0])
    cylinder(0.6, r = 10);
  translate([75, 0, 0])
    cylinder(0.6, r = 10);
  translate([75, 28, 0])
    cylinder(0.6, r = 10);
} else if (MOD == "DRUCK VORNE") {
  rotate([90, 0, 90])
    translate([ABS_L + WANDS, ABS_V + WANDS, ABS_U])
      vorne();
  translate([3, 4, 0])
    cylinder(0.6, r = 10);
  translate([28 - 3, 4, 0])
    cylinder(0.6, r = 10);
  translate([3, 110 - 4, 0])
    cylinder(0.6, r = 10);
  translate([28 - 3, 110 - 4, 0])
    cylinder(0.6, r = 10);
} else if (MOD == "DRUCK RECHTS") {
  rotate([0, 90, 0])
    translate([-PLAT[0] - ABS_R - WANDS, ABS_V, ABS_U])
      rechts();
  cylinder(0.6, r = 10);
  translate([28, 0, 0])
    cylinder(0.6, r = 10);
  translate([0, 75, 0])
    cylinder(0.6, r = 10);
  translate([28, 75, 0])
    cylinder(0.6, r = 10);
} else if (MOD == "DRUCK HINTEN") {
  rotate([-90, 0, 0])
    translate([ABS_L + WANDS, -PLAT[1] - ABS_H - WANDS, ABS_U])
      hinten();
  translate([4, 3, 0])
    cylinder(0.6, r = 10);
  translate([4, 28 - 3, 0])
    cylinder(0.6, r = 10);
  translate([110 - 4, 0, 0])
    cylinder(0.6, r = 10);
  translate([110 - 4, 28 - 3, 0])
    cylinder(0.6, r = 10);
} else if (MOD == "DRUCK OBEN") {
  rotate([180, 0, 90])
    translate([ABS_L + WANDS, ABS_V + WANDS, -(ABS_O + PLAT[2] + WANDS)])
      oben();
  translate([4, 4, 0])
    cylinder(0.6, r = 10);
  translate([74, 4, 0])
    cylinder(0.6, r = 10);
  translate([74, 104, 0])
    cylinder(0.6, r = 10);
  translate([4, 104, 0])
    cylinder(0.6, r = 10);
} else if (MOD == "DRUCK STIFT") {
  stift();
}
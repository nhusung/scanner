/*
 * ViewController.swift
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCapturePhotoCaptureDelegate,
                      RCamDelegate {
  var captureSession: AVCaptureSession!
  var output: AVCapturePhotoOutput!
  var previewLayer: AVCaptureVideoPreviewLayer!
  var server: RCamServer? = nil
  var connection: RCamConnection? = nil

  var oldBrightness: CGFloat!

  @IBOutlet var previewView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()

    captureSession = AVCaptureSession()
    captureSession.sessionPreset = AVCaptureSession.Preset.photo
    output = AVCapturePhotoOutput()

    guard let device = AVCaptureDevice.default(for: AVMediaType.video) else {
      handleError("Could not get camera device")
      return
    }

    guard let input = try? AVCaptureDeviceInput(device: device) else {
      handleError("Could not get camera device input")
      return
    }

    if !captureSession.canAddInput(input) {
      handleError("Could not add camera input to session")
      return
    }
    captureSession.addInput(input)

    if !captureSession.canAddOutput(output) {
      handleError("Could not add photoOutput to session")
      return
    }
    captureSession.addOutput(output)

    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    previewLayer.frame = previewView.bounds
    previewView.layer.addSublayer(previewLayer)
    captureSession.startRunning()

    server = RCamServer(delegate: self, supportedFormats: [.HEIF, .JPEG])
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  func handleError(_ error: String) {
    let alertController = UIAlertController(
      title: "Error",
      message: error,
      preferredStyle: .alert
    )
    alertController.addAction(UIAlertAction(title: "Ok", style: .default,
                                            handler: nil))
    present(alertController, animated: true, completion: nil)
  }

  func askPermission(forConnection conn: RCamConnection) {
    if connection == nil {
      let alertController = UIAlertController(
        title: "Allow access?",
        message: "The client \(conn.clientName) tries to connect to this camera.",
        preferredStyle: .alert
      )
      alertController.addAction(UIAlertAction(title: "Allow", style: .default) {
        (alertAction: UIAlertAction) in
        self.connection = conn
        conn.accept()
        UIApplication.shared.isIdleTimerDisabled = true
        self.oldBrightness = UIScreen.main.brightness
        UIScreen.main.brightness = 0.0
      })
      alertController.addAction(UIAlertAction(title: "Deny", style: .default) {
        (alertAction: UIAlertAction) in
        conn.close()
      })
      present(alertController, animated: true, completion: nil)
    } else {
      conn.close()
    }
  }

  func capture(forConnection conn: RCamConnection) {
    var codec: AVVideoCodecType
    if conn.captureFormat == .HEIF {
      codec = .hevc
    } else if conn.captureFormat == .JPEG {
      codec = .jpeg
    } else {
      conn.captureFinished(data: nil)
      return
    }
    let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: codec])
    let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
    settings.previewPhotoFormat = [
      kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
      kCVPixelBufferWidthKey as String: 160,
      kCVPixelBufferHeightKey as String: 160
    ]
    output.capturePhoto(with: settings, delegate: self)
  }

  func photoOutput(_ output: AVCapturePhotoOutput,
                   didFinishProcessingPhoto photo: AVCapturePhoto,
                   error: Error?) {
    if let error = error {
      handleError(error.localizedDescription)
      connection!.captureFinished(data: nil)
      return
    }

    if let imageData = photo.fileDataRepresentation() {
      connection!.captureFinished(data: imageData)
    } else {
      handleError("Could not get photo.fileDataRepresentation()")
      connection!.captureFinished(data: nil)
    }
  }

  func notifyClosing(forConnection conn: RCamConnection) {
    if connection == conn {
      connection = nil
      UIApplication.shared.isIdleTimerDisabled = false
      UIScreen.main.brightness = oldBrightness
    }
  }
}

/*
 * SData.m
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#import "SData.h"

@implementation SData

@synthesize ptr;
@synthesize capacity;
@synthesize count;
@synthesize freePtr;

- (id)initWithCapacity:(NSUInteger)bytes {
  if ((self = [super init])) {
    ptr = malloc(bytes);
    capacity = bytes;
    count = 0;
    freePtr = YES;
  }
  return self;
}

- (id)initWithPointer:(uint8_t *)ptr length:(NSUInteger)length free:(BOOL)free {
  if ((self = [super init])) {
    self.ptr = ptr;
    self.capacity = length;
    self.count = length;
    self.freePtr = free;
  }
  return self;
}

- (id)initWithCopiedData:(NSData *)data {
  if ((self = [super init])) {
    capacity = data.length;
    count = data.length;
    ptr = malloc(count);
    bcopy(data.bytes, ptr, count);
    freePtr = YES;
  }
  return self;
}

- (void)dealloc {
  if (freePtr)
    free(ptr);
}

- (void)copyMemoryFrom:(void *)ptr byteCount:(NSUInteger)count {
  bcopy(ptr, self.ptr + self.count, count);
  self.count += count;
}

- (SData *)addUInt8:(uint8_t)d {
  ptr[count] = d;
  count++;
  return self;
}

- (SData *)addUInt16:(uint16_t)d {
  *((uint16_t *) &ptr[count]) = d;
  count += 2;
  return self;
}

- (SData *)addUInt32:(uint32_t)d {
  *((uint32_t *) &ptr[count]) = d;
  count += 4;
  return self;
}

- (SData *)addUInt64:(uint64_t)d {
  *((uint64_t *) &ptr[count]) = d;
  count += 8;
  return self;
}

- (uint8_t)getUInt8At:(NSUInteger)pos {
  return ptr[pos];
}
- (uint16_t)getUInt16At:(NSUInteger)pos {
  return *((uint16_t *) &ptr[pos]);
}
- (uint32_t)getUInt32At:(NSUInteger)pos {
  return *((uint32_t *) &ptr[pos]);
}
- (uint64_t)getUInt64At:(NSUInteger)pos {
  return *((uint64_t *) &ptr[pos]);
}


- (NSString *)description {
  if (count > 32) {
    return [[NSString alloc] initWithFormat:@"%lu B", count];
  } else {
    char str[6 + count * 3];
    sprintf(str, "%lu B:", count);
    for (int i = 0; i < count; i++) {
      sprintf(str + 5 + i * 3, " %02x", ptr[i]);
    }
    return [[NSString alloc] initWithCString:str encoding:NSUTF8StringEncoding];
  }
}

@end

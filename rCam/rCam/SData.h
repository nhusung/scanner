/*
 * SData.h
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>

@interface SData : NSObject

@property uint8_t *ptr;
@property NSUInteger capacity;
@property NSUInteger count;
@property BOOL freePtr;

- (id)initWithCapacity:(NSUInteger)bytes;
- (id)initWithPointer:(uint8_t *)ptr length:(NSUInteger)length free:(BOOL)free;
- (id)initWithCopiedData:(NSData *)data;

- (void)copyMemoryFrom:(void *)ptr byteCount:(NSUInteger)count;

- (SData *)addUInt8:(uint8_t)d;
- (SData *)addUInt16:(uint16_t)d;
- (SData *)addUInt32:(uint32_t)d;
- (SData *)addUInt64:(uint64_t)d;

- (uint8_t)getUInt8At:(NSUInteger)pos;
- (uint16_t)getUInt16At:(NSUInteger)pos;
- (uint32_t)getUInt32At:(NSUInteger)pos;
- (uint64_t)getUInt64At:(NSUInteger)pos;

- (NSString *)description;

@end

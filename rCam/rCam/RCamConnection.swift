/*
 * RCamConnection.swift
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

enum RCamFormat: UInt8 {
  case invalid
  case JPEG
  case PNG
  case TIFF
  case HEIF
}
let RCamFormatStrings = ["JPEG", "PNG", "TIFF", "HEIF"]

enum RCamCommand: UInt8 {         // <bytes:argument name>
  case NONE                 = 0x00
  case VERSION              = 0x01 // <1:version number>
  case SEND_NAME            = 0x10 // <?(<32):name>
  case SUPPORTED_FORMATS    = 0x11 // <1:length> <1:format> [...]
  case ACCEPT               = 0x1f
  case CAPTURE              = 0x20 // <1:format>
  case CAPTURE_FINISHED     = 0x2e // <4:id> <8:size>
  case CAPTURE_FAILED       = 0x2f
  case GET                  = 0x30 // <4:id> <8:offset> <4:length>
  case SEND                 = 0x31 // <4:id> <8:offset> <4:length> <length:data>
  case DELETE_RESOURCE      = 0x32 // <4:id>
  case RESOURCE_UNAVAILABLE = 0x3f // <4:id>
}

protocol RCamDelegate {
  func askPermission(forConnection: RCamConnection)
  func capture(forConnection: RCamConnection)
  func notifyClosing(forConnection: RCamConnection)
  func handleError(_: String)
}

let inputBufSize = 256
class RCamConnection: NSObject, StreamDelegate {
  static var protocolVersion: UInt8 = 2

  private var server: RCamServer?
  var delegate: RCamDelegate

  private var inputStream: InputStream
  private var outputStream: OutputStream

  private(set) var accepted = false
  private var capturing = false
  private(set) var clientName = ""
  private var resources: [UInt32: SData] = [:]
  private var captureId: UInt32 = 0
  private var supportedFormats: [RCamFormat]
  private(set) var captureFormat: RCamFormat = .invalid
  private var connVer: UInt8 = 0

  init(withServer aServer: RCamServer, inputStream: InputStream,
       outputStream: OutputStream, supportedFormats: [RCamFormat]) {
    self.server = aServer
    self.delegate = aServer.delegate
    self.inputStream = inputStream
    self.outputStream = outputStream
    self.supportedFormats = supportedFormats
    super.init()

    inputStream.delegate = self
    outputStream.delegate = self
    inputStream.schedule(in: .current, forMode: .commonModes)
    outputStream.schedule(in: .current, forMode: .commonModes)
    inputStream.open()
    outputStream.open()

    Timer.scheduledTimer(withTimeInterval: 10, repeats: false) { (_: Timer) in
      if self.clientName.count == 0 {
        self.close() // force name to be sent within 10 s
      }
    }
  }
  deinit {
    close()
    inputBuf.deallocate()
  }

  func close() {
    inputStream.close()
    outputStream.close()
    delegate.notifyClosing(forConnection: self)
    server?.remove(connection: self)
  }

  func accept() {
    if server == nil {
      print("accept() is for servers only")
      return
    }
    accepted = true
    write(SData(capacity: 1).add(RCamCommand.ACCEPT.rawValue))
  }

  func captureFinished(data: Data?) {
    if server == nil {
      print("captureFinished() is for servers only")
      return
    }
    capturing = false
    if let data = data {
      resources[captureId] = SData(copiedData: data)
      write(SData(capacity: 13)
        .add(RCamCommand.CAPTURE_FINISHED.rawValue)
        .add(captureId)
        .add(NSSwapHostLongLongToBig(UInt64(data.count))))
      captureId += 1
    } else {
      write(SData(capacity: 1).add(RCamCommand.CAPTURE_FAILED.rawValue))
    }
  }

  private func send(id: UInt32, offset: UInt64, length: UInt32) {
    if server == nil {
      print("send() is for servers only")
      return
    }
    if let data = resources[id] {
      var l: UInt = 0
      if offset <= data.count {
        l = min(data.count - UInt(offset), UInt(min(length, 512 * 1024)))
      }
      write(SData(capacity: 17)
        .add(RCamCommand.SEND.rawValue)
        .add(id)
        .add(NSSwapHostLongLongToBig(offset))
        .add(NSSwapHostIntToBig(UInt32(l))))
      if l > 0 {
        write(SData(pointer: data.ptr + Int(offset), length: l, free: false))
      }
    } else {
      write(SData(capacity: 5)
        .add(RCamCommand.RESOURCE_UNAVAILABLE.rawValue)
        .add(id))
    }
  }

  private var currentCmd: RCamCommand = .NONE
  private var argumentBuf: SData! = SData(capacity: 32) // 32 -> clientName
  private var inputBuf = UnsafeMutableRawPointer.allocate(
    byteCount: inputBufSize,
    alignment: 1
  )
  private func streamRead() {
    while inputStream.hasBytesAvailable {
      let len = inputStream.read(
        inputBuf.bindMemory(to: UInt8.self, capacity: inputBufSize),
        maxLength: inputBufSize
      )
      var i = 0
      while i < len {
        if currentCmd == .NONE {
          currentCmd = RCamCommand(
            rawValue: inputBuf.load(fromByteOffset: i,
                                    as: UInt8.self)) ?? .NONE
          i += 1
        }
        switch currentCmd {
        case .VERSION: // both sides
          if i < len {
            connVer = min(inputBuf.load(fromByteOffset: i, as: UInt8.self),
                          RCamConnection.protocolVersion)
            if connVer != 2 {
              delegate.handleError("A client was rejected due to version incompatiblities (protocol version: \(RCamConnection.protocolVersion), common version: \(connVer))")
              close()
              break
            }
            i += 1
            currentCmd = .NONE
          }
          break

        case .SEND_NAME: // server side
          while i < len {
            let char = inputBuf.load(fromByteOffset: i, as: UInt8.self)
            i += 1
            if char == 0x00 || argumentBuf.count == 31 {
              if argumentBuf.count == 0 {
                close() // force a human readable name
                return
              }
              argumentBuf.add(UInt8(0))
              clientName = String(cString: argumentBuf.ptr)
              delegate.askPermission(forConnection: self)
              currentCmd = .NONE
              argumentBuf.count = 0
              break
            }
            argumentBuf.add(char)
          }
          break

        case .SUPPORTED_FORMATS: // client side
          print("< SUPPORTED FORMATS")
          if server != nil {
            delegate.handleError("The client is badly implemented (SUPPORTED FORMATS is disallowed for clients). Closing connection.")
            close()
            return
          }
          // TODO: client side implementation
          break

        case .ACCEPT: // client side
          print("< ACCEPT")
          if server != nil {
            delegate.handleError("The client is badly implemented (ACCEPT is disallowed for clients). Closing connection.")
            close()
            return
          }
          accepted = true
          currentCmd = .NONE
          break

        case .CAPTURE: // server side
          if !accepted {
            close()
          }
          if i < len {
            captureFormat = RCamFormat(
              rawValue: inputBuf.load(fromByteOffset: i, as: UInt8.self)
            ) ?? .invalid
            i += 1
            if !capturing {
              capturing = true
              // format checks are done in delegate.capture
              delegate.capture(forConnection: self)
            }
            currentCmd = .NONE
          }
          break

        case .CAPTURE_FINISHED: // client side
          print("< CAPTURE FINISHED")
          if server != nil {
            delegate.handleError("The client is badly implemented (CAPTURE FINISHED is disallowed for clients). Closing connection.")
            close()
            return
          }
          // TODO: client side implementation
          break

        case .CAPTURE_FAILED: // client side
          print("< CAPTURE FAILED")
          if server != nil {
            delegate.handleError("The client is badly implemented (CAPTURE FAILED is disallowed for clients). Closing connection.")
            close()
            return
          }
          delegate.handleError("Capture failed!")
          currentCmd = .NONE
          break

        case .GET: // server side
          // 16 B arguments
          if i < len {
            let bytes = min(len - i, 16 - Int(argumentBuf.count))
            argumentBuf.copyMemory(from: inputBuf + i, byteCount: UInt(bytes))
            i += bytes
          }
          if argumentBuf.count == 16 {
            send(
              id: argumentBuf.getUInt32(at: 0),
              offset: NSSwapBigLongLongToHost(argumentBuf.getUInt64(at: 4)),
              length: NSSwapBigIntToHost(argumentBuf.getUInt32(at: 12))
            )
            currentCmd = .NONE
            argumentBuf.count = 0
          }
          break

        case .SEND: // client side
          print("< SEND")
          if server != nil {
            delegate.handleError("The client is badly implemented (SEND is disallowed for clients). Closing connection.")
            close()
            return
          }
          // TODO: client side implementation
          break

        case .DELETE_RESOURCE: // server side
          // 4 B arguments
          if i < len {
            let bytes = min(len - i, 4 - Int(argumentBuf.count))
            argumentBuf.copyMemory(from: inputBuf + i, byteCount: UInt(bytes))
            i += bytes
          }
          if argumentBuf.count == 4 {
            resources.removeValue(
              forKey: argumentBuf.getUInt32(at: 0)
            )
            currentCmd = .NONE
            argumentBuf.count = 0
          }
          break

        case .RESOURCE_UNAVAILABLE: // client side
          print("< RESOURCE UNAVAILABLE")
          if server != nil {
            delegate.handleError("The client is badly implemented (RESOURCE UNAVAILABLE is disallowed for clients). Closing connection.")
            close()
            return
          }
          // 4 B arguments
          if i < len {
            let bytes = min(len - i, 4 - Int(argumentBuf.count))
            argumentBuf.copyMemory(from: inputBuf + i, byteCount: UInt(bytes))
            i += bytes
          }
          if argumentBuf.count == 4 {
            delegate.handleError("Resource unavailable: "
              + String(argumentBuf.getUInt32(at: 0)))
            currentCmd = .NONE
            argumentBuf.count = 0
          }
          break

        default:
          delegate.handleError("The client is badly implemented, sending unspecified codes (\(currentCmd.rawValue)) is disallowed. Closing connection.")
          close()
          return
        }
      }
    }
  }

  private var outputQueue = Queue<SData>()
  private func write(_ data: SData) {
    outputQueue.enqueue(newElement: data)
    if outputStream.hasSpaceAvailable {
      streamWrite()
    }
  }
  private var writtenBytes = 0
  private func streamWrite() {
    while let out = outputQueue.peek() {
      if out.count == writtenBytes {
        outputQueue.drop()
        writtenBytes = 0
        continue
      }
      let written = outputStream.write(out.ptr + writtenBytes,
                                       maxLength: Int(out.count) - writtenBytes)
      if written > 0 {
        writtenBytes += written
      }
      return
    }
  }

  func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
    if eventCode == .hasBytesAvailable {
      streamRead()
    } else if eventCode == .hasSpaceAvailable {
      streamWrite()
    } else if eventCode == .endEncountered {
      close()
    } else if eventCode == .openCompleted {
      if aStream == outputStream {
        // TODO: client side
        let bytes = SData(capacity: UInt(4 + supportedFormats.count))
          .add(RCamCommand.VERSION.rawValue)
          .add(RCamConnection.protocolVersion)
          .add(RCamCommand.SUPPORTED_FORMATS.rawValue)
          .add(UInt8(supportedFormats.count))!
        supportedFormats.forEach { bytes.add($0.rawValue) }
        write(bytes)
      }
    } else if eventCode == .errorOccurred {
      if aStream == inputStream {
        if let msg = inputStream.streamError?.localizedDescription {
          delegate.handleError("input stream error: " + msg)
        }
      } else if aStream == outputStream {
        if let msg = outputStream.streamError?.localizedDescription {
          delegate.handleError("output stream error: " + msg)
        }
      }
    }
  }
}

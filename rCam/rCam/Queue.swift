/*
 * Queue.swift
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

private final class QueueNode<T> {
  var value: T
  var next: QueueNode<T>? = nil

  init(value: T) {
    self.value = value
  }
}

final class Queue<T> {
  private var head: QueueNode<T>?
  private var tail: QueueNode<T>?

  init() {}

  func enqueue(newElement: T) {
    let oldTail = tail
    tail = QueueNode(value: newElement)
    if head == nil {
      head = tail
    } else {
      oldTail!.next = tail
    }
  }

  func dequeue() -> T? {
    if let head = head {
      self.head = head.next
      if self.head == nil {
        self.tail = nil
      }
      return head.value
    }
    return nil
  }

  func drop() {
    if let head = head {
      self.head = head.next
      if self.head == nil {
        self.tail = nil
      }
    }
  }

  func peek() -> T? {
    return head?.value
  }
}

/*
 * RCamServer.swift
 * This file is part of Scanner
 *
 * Copyright (C) 2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

class RCamServer: NSObject, NetServiceDelegate {
  var delegate: RCamDelegate
  var netService = NetService(domain: "local.", type: "_rcam._tcp.",
                              name: "", port: 1708)
  private var connections: [RCamConnection] = []
  private var supportedFormats: [RCamFormat]

  init?(delegate: RCamDelegate, supportedFormats: [RCamFormat]) {
    self.delegate = delegate
    self.supportedFormats = supportedFormats

    super.init()

    // publish the service via Bonjour/Zeroconf
    netService.publish(options: [.listenForConnections])
    netService.schedule(in: .main, forMode: .defaultRunLoopMode)
    netService.delegate = self
  }

  func netService(_ sender: NetService,
                  didNotPublish errorDict: [String : NSNumber]) {
    delegate.handleError(
      "Could not publish service: \(errorDict[NetService.errorCode]!)"
    )
  }

  func netService(_ sender: NetService,
                  didAcceptConnectionWith inputStream: InputStream,
                  outputStream: OutputStream) {
    connections.append(RCamConnection(
      withServer: self,
      inputStream: inputStream,
      outputStream: outputStream,
      supportedFormats: supportedFormats
    ))
  }

  func remove(connection: RCamConnection) {
    if let index = connections.index(of: connection) {
      connections.remove(at: index)
    }
  }
}

# Scanner

Digitalize your documents!

![screenshot](doc/screenshot.jpg)

This is the whole source tree of the Scanner project.  The project consists of a
transport system for the paper and a camera which can be remotely controlled.
Latter can currently be a digital camera that supports capturing via the Picture
Transfer Protocol or a smartphone running the self-written rCam app.  The motors
and the paper sensor themselves are coordinated by a microcontroller which is
Arduino compatible.  The firmware running on it can be found in the firmware
subdirectory.  To build it, Arduino IDE is required.

Since the microcontroller is unable to control the camera, an application for a
computer is needed.  It is written in C and uses GTK+ for the graphical user
interface.  To communicate with the camera it uses GPhoto.  Among handling the
camera and the transport system, _Scanner_ shows you the pictures that were
taken so you can name and tag them.  The app has been tested on Linux
(Fedora 28) and should work on other Unix systems too.  Building requires CMake
as well as a compiler that supports the gnu dialect (gcc or clang) and the
development libraries of gtk+-3.0 and libgphoto2.  Then the build process is
straightforward:

    cmake .
    make

In order for glib to find the GSettings scheme you have to set an environment
variable before launching the app:

    XDG_DATA_DIRS=$PWD:$XDG_DATA_DIRS ./scanner

Otherwise you can simply install the files using `make install`.

The complete documentation in German is available in the “doc” directory.  Note
that there still is some development so some parts might have changed a little.

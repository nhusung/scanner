/*
 * settings-window.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "settings-window.h"
#include "settings-agent.h"
#include <string.h> /* strlen() */
#include <glib/gstdio.h>
#include "app-window.h"
#include <math.h> /* pow() */
#include "document-handler.h"
#include "messages.h"

GtkWidget *dialog, *captures_folder_entry, *tty_entry,
  *default_rotation_combo_box, *counter_digits_spin, *history_length_spin;


static void settings_dialog_cancel(G_GNUC_UNUSED GtkWidget *widget,
                                   G_GNUC_UNUSED gpointer data) {
  gtk_widget_destroy(dialog);
}

static void settings_dialog_ok(G_GNUC_UNUSED GtkWidget *widget,
                               G_GNUC_UNUSED gpointer data) {
  validate_status captures_folder_status, tty_status;
  const gchar *tmp_captures_folder, *tmp_tty;

  /* validation */
  tmp_captures_folder = gtk_entry_get_text(GTK_ENTRY(captures_folder_entry));
  captures_folder_status = validate_folder(tmp_captures_folder);

  tmp_tty = gtk_entry_get_text(GTK_ENTRY(tty_entry));

  /* Does only work if hardware is attached
   *tty_status = validate_tty(tmp_tty); */
  tty_status = VALIDATE_OK;

  if (captures_folder_status != VALIDATE_OK || tty_status != VALIDATE_OK)
    goto settings_dialog_ok_error; /* others don't need validation */

  /* capture folder */
  g_free(settings.capture_folder);
  settings.capture_folder = g_strdup(tmp_captures_folder);
  /* g_strdup -> else memory would be freed (destroy) */
  settings.capture_filename_length = strlen(settings.capture_folder)
                                     + ADD_PATH_LENGTH;

  /* tty */
  g_free(settings.tty);
  settings.tty = g_strdup(tmp_tty); /* else memory would be freed (destroy) */

  /* default rotation */
  settings.default_rotation
      = gtk_combo_box_get_active(GTK_COMBO_BOX(default_rotation_combo_box));
  if (settings.default_rotation == (gchar) -1)
    settings.default_rotation = 0;
  /* set the rotation of empty document */
  if (doc_cur.id == settings.next_capture_id - 1)
    doc_cur.rotation = settings.default_rotation;

  /* counter digits */
  settings.counter_digits
      = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(counter_digits_spin));
  if (settings.counter_digits == 10)
    settings.counter_max = G_MAXUINT32;
  else
    settings.counter_max = pow(10, settings.counter_digits) - 1;
  gtk_spin_button_set_range(GTK_SPIN_BUTTON(counter_entry),
                            0, settings.counter_max);

  /* history length */
  resize_history(
      gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(history_length_spin)));

  gtk_widget_destroy(dialog);

  save_settings_of_dialog();
  return;

settings_dialog_ok_error: {
    gchar *message1 = NULL, *message2 = NULL;
    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                               GTK_DIALOG_MODAL,
                                               GTK_MESSAGE_ERROR,
                                               GTK_BUTTONS_CLOSE,
                                               "Validation failed!");

    if (captures_folder_status == VALIDATE_ERR_NO_DIR)
      message1 = " - No directory for captures at";
    else if (captures_folder_status == VALIDATE_ERR_NO_ACCESS)
      message1 = " - No write access for captures at";

    if (tty_status == VALIDATE_ERR_NO_CHAR_SPECIAL)
      message2 = " - No serial terminal for hardware at";
    else if (tty_status == VALIDATE_ERR_NO_ACCESS)
      message2 = " - No write access for serial communication with Arduino at";

    if (message1) {
      if (message2) {
        gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
                                                 "%s %s\n%s %s",
                                                 message1, tmp_captures_folder,
                                                 message2, tmp_tty);
      } else {
        gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
                                                 "%s %s",
                                                 message1, tmp_captures_folder);
      }
    } else if (message2) {
      gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
                                               "%s %s", message2, tmp_tty);
    }

    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
}

void choose_captures_folder(GtkWidget *widget, G_GNUC_UNUSED gpointer data) {
  GtkWidget *d
      = gtk_file_chooser_dialog_new(MSG_CAPT_DIR,
                                    GTK_WINDOW(window.widget),
                                    GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                    "_Cancel", GTK_RESPONSE_CANCEL,
                                    "_Open", GTK_RESPONSE_ACCEPT,
                                    NULL);

  gtk_window_set_icon_name(GTK_WINDOW(d), ICON_NAME);

  if (widget != NULL) { /* called from settings dialog */
    const gchar *path = gtk_entry_get_text(GTK_ENTRY(captures_folder_entry));
    if (validate_folder(path) == VALIDATE_OK)
      gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(d), path);
  }

  if (gtk_dialog_run(GTK_DIALOG(d)) == GTK_RESPONSE_ACCEPT) {
    char *path = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(d));
    if (widget == NULL) { /* called at startup */
      g_free(settings.capture_folder);
      g_settings_set_string(settings.g, "capture-folder", path);
      settings.capture_folder = path;
      settings.capture_filename_length = strlen(settings.capture_folder)
                                         + ADD_PATH_LENGTH;
    } else {
      gtk_entry_set_text(GTK_ENTRY(captures_folder_entry), path);
      g_free(path);
    }
  }

  gtk_widget_destroy(d);
}

void open_settings_dialog(G_GNUC_UNUSED GtkWidget *widget,
                          G_GNUC_UNUSED gpointer data) {
  GtkWidget *header_bar, *cancel_btn, *ok_btn, *grid, *label, *segment_grid,
      *choose_save_folder_button;
  GtkStyleContext *style_context;

  /* window
   *
   * TOPLEVEL means that the window is a real window, not a tooltip, for
   * example. */
  dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(window.widget));
  gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
  gtk_window_set_icon_name(GTK_WINDOW(dialog), ICON_NAME);

  /* header bar */
  header_bar = gtk_header_bar_new();
  gtk_header_bar_set_title(GTK_HEADER_BAR(header_bar), "Settings");
  gtk_window_set_titlebar(GTK_WINDOW(dialog), header_bar);

  /* header bar -> cancel button */
  cancel_btn = gtk_button_new_with_label("Cancel");
  g_signal_connect(cancel_btn, "clicked", G_CALLBACK(settings_dialog_cancel),
                   NULL);
  gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), cancel_btn);

  /* header bar -> ok button */
  ok_btn = gtk_button_new_with_label("OK");
  style_context = gtk_widget_get_style_context(ok_btn);
  gtk_style_context_add_class(style_context, "suggested-action");
  g_signal_connect(ok_btn, "clicked", G_CALLBACK(settings_dialog_ok), NULL);
  gtk_header_bar_pack_end(GTK_HEADER_BAR(header_bar), ok_btn);

  /* main grid */
  grid = gtk_grid_new();
  gtk_container_set_border_width(GTK_CONTAINER(grid), 16);
  gtk_grid_set_row_spacing(GTK_GRID(grid), 16);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 16);
  gtk_container_add(GTK_CONTAINER(dialog), grid);

  /* captures directory -> label */
  label = gtk_label_new(LBL_CAPT_DIR);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_tooltip_text(label, DESC_CAPT_DIR);
  gtk_grid_attach(GTK_GRID(grid), label, 0, 0, 1, 1);

  /* captures directory -> segment grid */
  segment_grid = gtk_grid_new();
  style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(style_context, "linked");
  gtk_widget_set_hexpand(segment_grid, TRUE);
  gtk_grid_attach(GTK_GRID(grid), segment_grid, 1, 0, 1, 1);

  /* captures directory -> segment grid -> captures folder entry */
  captures_folder_entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(captures_folder_entry), settings.capture_folder);
  /* ok without lock here, no write operation may be pending */
  gtk_widget_set_tooltip_text(captures_folder_entry, DESC_CAPT_DIR);
  gtk_widget_set_hexpand(captures_folder_entry, TRUE);
  gtk_container_add(GTK_CONTAINER(segment_grid), captures_folder_entry);

  /* captures directory -> segment grid -> choose folder button */
  choose_save_folder_button
      = gtk_button_new_from_icon_name("folder-open", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(choose_save_folder_button, MSG_CHOOSE_DIR);
  g_signal_connect(choose_save_folder_button, "clicked",
                   G_CALLBACK(choose_captures_folder), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), choose_save_folder_button);

  /* tty -> label */
  label = gtk_label_new(LBL_INO_PATH);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_tooltip_text(label, DESC_INO_PATH);
  gtk_grid_attach(GTK_GRID(grid), label, 0, 1, 1, 1);

  /* tty -> entry */
  tty_entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(tty_entry), settings.tty);
  gtk_widget_set_tooltip_text(tty_entry, DESC_INO_PATH);
  gtk_widget_set_hexpand(tty_entry, TRUE);
  gtk_grid_attach(GTK_GRID(grid), tty_entry, 1, 1, 1, 1);

  /* default rotation -> label */
  label = gtk_label_new(LBL_DEFAULT_ROT);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_tooltip_text(label, DESC_DEFAULT_ROT);
  gtk_grid_attach(GTK_GRID(grid), label, 0, 2, 1, 1);

  /* default rotation -> combo box */ {
    GtkListStore *model;
    GtkTreeIter iter;
    GtkCellRenderer *renderer;

    model = gtk_list_store_new(1, G_TYPE_STRING);

    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter, 0, "0°", -1);
    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter, 0, "90° counterclockwise", -1);
    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter, 0, "180°", -1);
    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter, 0, "90° clockwise", -1);

    default_rotation_combo_box
        = gtk_combo_box_new_with_model((GtkTreeModel *) model);
    g_object_unref(model);

    renderer = gtk_cell_renderer_text_new();
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(default_rotation_combo_box),
                               renderer, FALSE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(default_rotation_combo_box),
                                   renderer, "text", 0, NULL);

    gtk_combo_box_set_active(GTK_COMBO_BOX(default_rotation_combo_box),
                             settings.default_rotation);
    gtk_widget_set_tooltip_text(default_rotation_combo_box, DESC_DEFAULT_ROT);
    gtk_widget_set_hexpand(default_rotation_combo_box, TRUE);
    gtk_grid_attach(GTK_GRID(grid), default_rotation_combo_box, 1, 2, 1, 1);
  }

  /* conter digits -> label */
  label = gtk_label_new(LBL_COUNTER_DGTS);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_tooltip_text(label, DESC_COUNTER_DGTS);
  gtk_grid_attach(GTK_GRID(grid), label, 0, 3, 1, 1);

  /* conter digits -> spin button */
  counter_digits_spin = gtk_spin_button_new_with_range(1, 10, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(counter_digits_spin),
                            settings.counter_digits);
  gtk_widget_set_tooltip_text(counter_digits_spin, DESC_COUNTER_DGTS);
  gtk_widget_set_hexpand(counter_digits_spin, TRUE);
  gtk_grid_attach(GTK_GRID(grid), counter_digits_spin, 1, 3, 1, 1);

  /* history length -> label */
  label = gtk_label_new(LBL_HIST_LEN);
  gtk_widget_set_halign(label, GTK_ALIGN_START);
  gtk_widget_set_tooltip_text(label, DESC_HIST_LEN);
  gtk_grid_attach(GTK_GRID(grid), label, 0, 4, 1, 1);

  /* history length -> spin button */
  history_length_spin = gtk_spin_button_new_with_range(settings.hist_sel + 1,
                                                       G_MAXUINT16, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(history_length_spin),
                            settings.hist_max_length);
  gtk_widget_set_tooltip_text(history_length_spin, DESC_HIST_LEN);
  gtk_grid_attach(GTK_GRID(grid), history_length_spin, 1, 4, 1, 1);

  gtk_widget_show_all(dialog);
}

/*
 * settings-agent.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SETTINGS_AGENT_H__
#define __SETTINGS_AGENT_H__

#include <glib.h>
#include "glib-gsettings-extension.h"

#define ADD_PATH_LENGTH 14 /* /XXXXXXXX.jpg\0 */

/* Get the index of the nth newest document in settings.hist (n = 0 for the
 * newest)
 *
 * Note: this macro requires n <= settings.hist_max_length */
#define HIST_INDEX(n) (((guint32) settings.hist_max_length \
                        + settings.hist_newest - (n))      \
                       % settings.hist_max_length)

struct settings_s {
  GSettings *g;

  gchar *volatile capture_folder;
  volatile guint capture_filename_length;
  volatile guint next_capture_id;

  gchar *tty;
  gchar default_rotation;
  guint8 counter_digits;
  guint32 counter_max;

  guint16 hist_max_length;
  guint16 hist_length;
  guint16 hist_newest;
  guint16 hist_sel;
  GVariant **hist; /* NOTE: New history elements must be NULL! */
};
extern struct settings_s settings;

extern void load_settings(void);

extern void load_document(void);
extern void save_history(void);

extern void resize_history(guint length);

extern void save_settings_of_dialog(void);

typedef enum {
  VALIDATE_OK,
  VALIDATE_ERR_NO_DIR,
  VALIDATE_ERR_NO_CHAR_SPECIAL,
  VALIDATE_ERR_NO_ACCESS
} validate_status;
extern validate_status validate_folder(const gchar *path);
extern validate_status validate_tty(const gchar *path);

#endif /* __SETTINGS_AGENT_H__ */

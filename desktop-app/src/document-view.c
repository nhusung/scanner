/*
 * document-view.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "document-view.h"
#include "settings-agent.h"
#include "app-window.h"
#include "document-handler.h"
#include "settings-agent.h"
#include <stdlib.h> /* exit() */
#include <glib/gprintf.h>
#include <glib/gstdio.h> /* g_access() */
#include <errno.h>
#include "messages.h"

gchar wait_image_path[] = PKGDATADIR "/wait.png";

GtkWidget *doc_scrolled_window, *doc_image_view;
GtkAdjustment *adj_x, *adj_y;
gdouble scroll_x, scroll_y, scroll_x_old, scroll_y_old;
struct image_s image;
const GdkRGBA white_color = {1.0, 1.0, 1.0, 1.0};

static void doc_scrolled_window_size_allocate(GtkWidget *widget,
                                              GdkRectangle *allocation,
                                              gpointer user_data);
static gboolean doc_image_view_redraw(GtkWidget *widget, cairo_t *cr,
                                      gpointer data);

static void doc_image_view_size_allocate(G_GNUC_UNUSED GtkWidget *widget,
                                         G_GNUC_UNUSED GdkRectangle *allocation,
                                         G_GNUC_UNUSED gpointer user_data) {
  if (scroll_x != scroll_x_old) {
    gtk_adjustment_set_value(adj_x, scroll_x);
    scroll_x_old = scroll_x;
  }
  if (scroll_y != scroll_y_old) {
    gtk_adjustment_set_value(adj_y, scroll_y);
    scroll_y_old = scroll_y;
  }
}


GtkWidget *create_doc_view(void) {
  doc_scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_size_request(doc_scrolled_window, 400, -1);
  g_signal_connect(doc_scrolled_window, "size-allocate",
                   G_CALLBACK(doc_scrolled_window_size_allocate), NULL);

  /* get adjustments */ {
    GtkScrolledWindow *w = GTK_SCROLLED_WINDOW(doc_scrolled_window);
    adj_x = gtk_scrolled_window_get_hadjustment(w);
    adj_y = gtk_scrolled_window_get_vadjustment(w);
  }

  /* document scrolled window -> image */
  doc_image_view = gtk_drawing_area_new();
  doc_image_view_load_image();
  g_signal_connect(doc_image_view, "draw", G_CALLBACK(doc_image_view_redraw),
                   NULL);
  gtk_container_add(GTK_CONTAINER(doc_scrolled_window), doc_image_view);
  g_signal_connect(doc_scrolled_window, "size-allocate",
                   G_CALLBACK(doc_image_view_size_allocate), NULL);

  return doc_scrolled_window;
}

void doc_image_view_load_image(void) {
  gchar *fn;
  GError *error = NULL;

  /* get the filename */
  if (doc_cur.id == settings.next_capture_id) {
    /* “shown” image has not been captured */
    fn = wait_image_path;
  } else {
    fn = g_alloca(settings.capture_filename_length);
    g_sprintf(fn, "%s/%08x.jpg", settings.capture_folder, doc_cur.id);
  }

  while (g_access(fn, R_OK) == -1) {
    GtkWidget *d, *content_area, *label;
    gchar *msg;
    int err = errno;

    if (fn == wait_image_path) {
      d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                 GTK_DIALOG_MODAL,
                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                 ERR_INST, fn, g_strerror(err));
      gtk_dialog_run(GTK_DIALOG(d));
      gtk_widget_destroy(d);

      exit(EXIT_FAILURE);
    }
    d = gtk_dialog_new_with_buttons("Error",
                                    GTK_WINDOW(window.widget),
                                    GTK_DIALOG_MODAL,
                                    "_Delete", GTK_RESPONSE_ACCEPT,
                                    "_Retry", GTK_RESPONSE_REJECT,
                                    NULL);

    gtk_window_set_icon_name(GTK_WINDOW(d), ICON_NAME);
    gtk_dialog_set_default_response(GTK_DIALOG(d), GTK_RESPONSE_ACCEPT);

    content_area = gtk_dialog_get_content_area(GTK_DIALOG(d));
    msg = g_strdup_printf("Error accessing “%s”: %s", fn, g_strerror(err));
    label = gtk_label_new(msg);
    g_free(msg);

    gtk_container_add(GTK_CONTAINER(content_area), label);
    gtk_container_set_border_width(GTK_CONTAINER(content_area), 40);
    gtk_widget_show_all(d);

    if (gtk_dialog_run(GTK_DIALOG(d)) == GTK_RESPONSE_ACCEPT) {
      doc_remove();

      if (doc_cur.id == settings.next_capture_id)
        fn = wait_image_path;
      else
        g_sprintf(fn, "%s/%08x.jpg", settings.capture_folder, doc_cur.id);
    }
    gtk_widget_destroy(d);
  }

  /* load the image */
  if (image.pixbuf)
    g_object_unref(image.pixbuf);
  if ((image.pixbuf = gdk_pixbuf_new_from_file(fn, &error)) == NULL) {
    GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                          GTK_DIALOG_MODAL,
                                          GTK_MESSAGE_ERROR,
                                          GTK_BUTTONS_OK,
                                          "Fatal error reading “%s”: %s",
                                          fn, error->message);
    gtk_dialog_run(GTK_DIALOG(d));
    gtk_widget_destroy(d);

    exit(EXIT_FAILURE); /* else there will be drawing issues */
  }
  image.width = gdk_pixbuf_get_width(image.pixbuf);
  image.height = gdk_pixbuf_get_height(image.pixbuf);

  image.fit = TRUE;

  /* rotation */
  if (fn != wait_image_path) {
    gchar r = doc_cur.rotation;
    if (r != 0) {
      GdkPixbuf *pbuf_tmp = gdk_pixbuf_rotate_simple(image.pixbuf, r * 90);
      g_object_unref(image.pixbuf);
      image.pixbuf = pbuf_tmp;

      if (r != 2) {
        guint size_tmp = image.height;
        image.height = image.width;
        image.width = size_tmp;
      }
    }
  }

  /* force redrawing */
  image.old_view_width = image.old_view_height = -1;
  doc_image_view_size_calc();
}

void doc_image_view_size_calc(void) {
  const gint view_width = gtk_widget_get_allocated_width(doc_scrolled_window);
  const gint view_height = gtk_widget_get_allocated_height(doc_scrolled_window);
  gint projection_width, projection_height;

  if (image.fit) {
    image.scale = MIN((double) view_width / image.width,
                      (double) view_height / image.height);
  }

  if (view_width == image.old_view_width
      && view_height == image.old_view_height
      && image.scale == image.old_scale
      && doc_cur.rotation <= ROTATION_VALUE_MASK) {
    return; /* don't redraw */
  }

  projection_width = image.width * image.scale;
  projection_height = image.height * image.scale;

  /* set the widget's size */
  gtk_widget_set_size_request(doc_image_view, MAX(view_width, projection_width),
                              MAX(view_height, projection_height));


  if (image.scale != image.old_scale) {
    /* recalculate the scrollbars on scale change */
    if (projection_width > view_width) { /* is the image now scrollable? */
      gdouble hscroll_abs;
      const gdouble view_width_2 = view_width / 2;

      if (image.width * image.old_scale > view_width) {
        /* image was scrollable */
        hscroll_abs = (gtk_adjustment_get_value(adj_x) + view_width_2)
                      / image.old_scale;
      } else {
        hscroll_abs = image.width / 2; /* centered by default */
      }

      scroll_x = hscroll_abs * image.scale - view_width_2;
    }

    if (projection_height > view_height) { /* is the image now scrollable? */
      gdouble vscroll_abs;
      const gdouble view_height_2 = view_height / 2;

      if (image.height * image.old_scale > view_height) {
        /* image was scrollable */
        vscroll_abs = (gtk_adjustment_get_value(adj_y) + view_height_2)
                      / image.old_scale;
      } else {
        vscroll_abs = image.height / 2; /* centered by default */
      }

      scroll_y = vscroll_abs * image.scale - view_height_2;
    }
  }

  image.old_view_width = view_width;
  image.old_view_height = view_height;
  image.old_scale = image.scale;
  doc_cur.rotation &= ROTATION_VALUE_MASK;

  image.offset_left = MAX((view_width - projection_width), 0)
                      / (2 * image.scale);
  image.offset_top = MAX((view_height - projection_height), 0)
                     / (2 * image.scale);

  gtk_widget_queue_draw(doc_image_view); /* redraw */
}

/*
 * This function mustn't be called directly!
 */
gboolean doc_image_view_redraw(G_GNUC_UNUSED GtkWidget *widget,
                               cairo_t *cr,
                               G_GNUC_UNUSED gpointer data) {
  cairo_scale(cr, image.scale, image.scale);

  /* background color */
  gdk_cairo_set_source_rgba(cr, &white_color);
  cairo_paint(cr);

  gdk_cairo_set_source_pixbuf(cr, image.pixbuf, image.offset_left,
                              image.offset_top);

  cairo_rectangle(cr, image.offset_left, image.offset_top,
                  image.width,
                  image.height);
  cairo_fill(cr);

  return FALSE;
}

void doc_scrolled_window_size_allocate(G_GNUC_UNUSED GtkWidget *widget,
                                       G_GNUC_UNUSED GdkRectangle *allocation,
                                       G_GNUC_UNUSED gpointer user_data) {
  doc_image_view_size_calc();
}

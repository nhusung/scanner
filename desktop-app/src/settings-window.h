/*
 * settings-window.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SETTINGS_WINDOW_H__
#define __SETTINGS_WINDOW_H__

#include <gtk/gtk.h>

extern void open_settings_dialog(GtkWidget *widget, gpointer data);
extern void choose_captures_folder(GtkWidget *widget, gpointer data);

#endif /* __SETTINGS_WINDOW_H__ */

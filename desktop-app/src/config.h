#ifndef __CONFIG_H__
#define __CONFIG_H__

#ifdef NDEBUG

#define DEBUG 0
#define debug_print(fmt, ...)

#else /* ! NDEBUG */

#define DEBUG 1

#define STRINGIFY_ARG(s) #s
#define STRINGIFY(s) STRINGIFY_ARG(s)
#define STRLOC __FILE__ ":" STRINGIFY(__LINE__)

#include <stdio.h>
#define debug_print(fmt, ...) fprintf(stderr, "[" STRLOC ":%s] " fmt "\n", \
                                      __FUNCTION__, ##__VA_ARGS__)

#endif /* ! NDEBUG */

#endif /* __CONFIG_H__ */

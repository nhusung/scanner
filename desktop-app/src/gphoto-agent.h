/*
 * gphoto-agent.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GPHOTO_AGENT_H__
#define __GPHOTO_AGENT_H__

#include <glib.h>
#include "messages.h"

extern void gphoto_capture(GSourceFunc callback);

extern error_t *gphoto_series_start(GSourceFunc capture_finished_callback);
extern error_t *gphoto_series_end(void);

extern void gphoto_wait_finish(void);

#endif /* __GPHOTO_AGENT_H__ */

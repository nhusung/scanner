/*
 * document-handler.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "document-handler.h"
#include "gphoto-agent.h"
#include "settings-agent.h"
#include "app-window.h"
#include <glib/gstdio.h> /* g_remove(), g_file_test() */
#include <gio/gio.h> /* g_file_move() */
#include <time.h>
#include <utime.h>
#include <errno.h>
#include "messages.h"

doc_t doc_cur;

struct finalize_save_data_s {
  gchar *path;
  time_t secs;
};

static void finalize_save(G_GNUC_UNUSED GObject *source_object,
                          GAsyncResult *res, gpointer user_data) {
  GError *err = NULL;
  struct finalize_save_data_s *data = user_data;

  if (!gdk_pixbuf_save_to_stream_finish(res, &err)) {
    GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                          GTK_DIALOG_MODAL,
                                          GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                          ERR_SAVE, data->path, err->message);
    gtk_dialog_run(GTK_DIALOG(d));
    gtk_widget_destroy(d);
    g_error_free(err);
  }

  /* set date with utime */ {
    struct utimbuf buf;
    const gchar *warning = NULL;

    buf.actime = data->secs;
    buf.modtime = data->secs;

    if (g_utime(data->path, &buf) != 0)
      warning = g_strerror(errno);

    if (warning) {
      GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_WARNING,
                                            GTK_BUTTONS_OK,
                                            WARN_UTIME, warning);
      gtk_dialog_run(GTK_DIALOG(d));
      gtk_widget_destroy(d);
    }
  }
  g_free(data->path);
}

static inline gchar *calc_save_name(void) {
  gchar *path;
  if (doc_cur.use_counter) {
    path = g_strdup_printf("%s/%0*d-%s.png", doc_cur.path,
                            doc_cur.counter_digits, doc_cur.counter,
                            doc_cur.name);
  } else {
    path = g_strconcat(doc_cur.path, "/", doc_cur.name, ".png", NULL);
  }
  return path;
}

const gchar *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                          "Aug", "Sep", "Oct", "Nov", "Dec" };
gchar time_string[12];
gboolean doc_save(GdkPixbuf *pbuf) {
  gchar *path;
  GError *err = NULL;

  if (!doc_cur.require_save)
    return TRUE;

  doc_cur.counter_digits = settings.counter_digits;

  /* validation */
  if (validate_folder(doc_cur.path) != VALIDATE_OK) {
    GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                          GTK_DIALOG_MODAL,
                                          GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                          ERR_WRITE_DIR, doc_cur.path);
    gtk_dialog_run(GTK_DIALOG(d));
    gtk_widget_destroy(d);
    return FALSE;
  }
  if (doc_cur.name == NULL || doc_cur.name[0] == '\0') {
    GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                          GTK_DIALOG_MODAL,
                                          GTK_MESSAGE_ERROR,
                                          GTK_BUTTONS_OK,
                                          ERR_EMPTY_NAME);
    gtk_dialog_run(GTK_DIALOG(d));
    gtk_widget_destroy(d);
    return FALSE;
  }

  path = calc_save_name();
  if (doc_cur.require_save & SAVE_METHOD_MOVE) {
    if (g_file_test(path, G_FILE_TEST_EXISTS)) {
      /* warn before overwriting
       *
       * This also works with files that have to be rewritten and get a new name
       * because doc_cur.require_save is a set of flags. */
      gint res;
      GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_QUESTION,
                                            GTK_BUTTONS_YES_NO,
                                            MSG_EXIST, path);
      res = gtk_dialog_run(GTK_DIALOG(d));
      gtk_widget_destroy(d);
      if (res != GTK_RESPONSE_YES) {
        g_free(path);
        return FALSE;
      }
    }
    if (!doc_cur.old_path
        || !g_file_test(doc_cur.old_path, G_FILE_TEST_EXISTS)) {
      /* unable to move file because it doesn't exist – rewrite it instead
       *
       * Why we don't check whether doc_cur.old_path is set:
       * Per definition doc_cur.old_path has to be set here since only for new
       * documents this is not set and these have the SAVE_METHOD_MOVE flag. */
      doc_cur.require_save = SAVE_METHOD_REWRITE;
    }
  }

  if (doc_cur.require_save & SAVE_METHOD_REWRITE) {
    GOutputStream *stream;
    GFile *file = g_file_new_for_path(path);
    struct finalize_save_data_s *data
        = g_malloc(sizeof (struct finalize_save_data_s));

    if (doc_cur.old_path != NULL && doc_cur.old_path[0] != '\0')
      g_remove(doc_cur.old_path);

    stream = G_OUTPUT_STREAM(g_file_replace(file, NULL, FALSE,
                                            G_FILE_CREATE_NONE, NULL, &err));
    g_object_unref(file);
    if (!stream) {
      GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                            ERR_SAVE, path, err->message);
      gtk_dialog_run(GTK_DIALOG(d));
      gtk_widget_destroy(d);
      g_free(path);
      g_error_free(err);
      return FALSE;
    }

    /* calculate time */ {
      struct tm date;
      const gchar *warning = NULL;

      if (doc_cur.year < 1970)
        warning = WARN_TOO_OLD;

      date.tm_mday = doc_cur.day;
      date.tm_mon = doc_cur.month;
      date.tm_year = doc_cur.year - 1900;
      date.tm_hour = 12;
      date.tm_min = 0;
      date.tm_sec = 0;
      date.tm_isdst = 0;

      errno = 0;
      data->secs = mktime(&date); /* ignores tm_wday & tm_yday */
      if (errno)
        warning = g_strerror(errno);

      if (warning) {
        GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                              GTK_DIALOG_MODAL,
                                              GTK_MESSAGE_WARNING,
                                              GTK_BUTTONS_OK,
                                              WARN_UTIME, warning);
        gtk_dialog_run(GTK_DIALOG(d));
        gtk_widget_destroy(d);
      }
    }

    data->path = g_strdup(path);
    g_snprintf(time_string, sizeof(time_string), "%02u %s %04u",
               doc_cur.day, months[doc_cur.month], doc_cur.year);
    gdk_pixbuf_save_to_stream_async(pbuf, stream, "png", NULL,
                                    finalize_save, data,
                                    "tEXt::Creation Time", time_string, NULL);
    g_object_unref(stream);
  } else {
    GFile *src = g_file_new_for_path(doc_cur.old_path);
    GFile *dest = g_file_new_for_path(path);

    if (!g_file_move(src, dest,
                     G_FILE_COPY_OVERWRITE | G_FILE_COPY_ALL_METADATA, NULL,
                     NULL, NULL, &err)) {
      GtkWidget *d = gtk_message_dialog_new(GTK_WINDOW(window.widget),
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                            ERR_SAVE, path, err->message);
      gtk_dialog_run(GTK_DIALOG(d));
      gtk_widget_destroy(d);
      g_free(path);
      g_error_free(err);
      g_object_unref(src);
      g_object_unref(dest);
      return FALSE;
    }

    g_object_unref(src);
    g_object_unref(dest);
  }

  g_free(doc_cur.old_path);
  doc_cur.old_path = path;
  doc_cur.require_save = 0;
  return TRUE;
}


void doc_prev(void) {
  save_history();

  settings.hist_sel++;
  g_assert_cmpuint(settings.hist_sel, <, settings.hist_length);
  load_document();
}

void doc_next(void) {
  save_history();

  if (settings.hist_sel == 0) { /* create a new document */
    settings.hist_newest = HIST_INDEX(-1);

    /* initialize new document */
    doc_cur.id++;
    doc_cur.rotation = settings.default_rotation;
    if (doc_cur.counter == settings.counter_max)
      doc_cur.counter = 1;
    else
      doc_cur.counter++;
    doc_cur.require_save = SAVE_METHOD_REWRITE | SAVE_METHOD_MOVE;
    /* SAVE_METHOD_MOVE -> file gets a new name, warn before overwriting */
    g_free(doc_cur.old_path);
    doc_cur.old_path = NULL;

    if (settings.hist_length < settings.hist_max_length)
      settings.hist_length++;
  } else {
    settings.hist_sel--;
    load_document();
  }
}

void doc_remove(void) {
  guint i = HIST_INDEX(settings.hist_sel);

  /* don't remove images that have not been scanned yet */
  g_assert_cmpuint(doc_cur.id, !=, settings.next_capture_id);

  if (settings.hist_sel == 0) { /* newest document was removed */
    /* initialize new document */
    doc_cur.id++;
    doc_cur.rotation = settings.default_rotation;
    doc_cur.require_save = SAVE_METHOD_REWRITE;
    g_free(doc_cur.old_path);
    doc_cur.old_path = NULL;
    return;
  }

  g_variant_unref(settings.hist[i]);
  while (i != settings.hist_newest) {
    guint next = (i + 1) % settings.hist_max_length;
    settings.hist[i] = settings.hist[next];
    i = next;
  }

  settings.hist[i] = NULL;
  settings.hist_length--;
  settings.hist_newest--;
  settings.hist_sel--;

  load_document();
}

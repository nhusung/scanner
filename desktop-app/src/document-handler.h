/*
 * document-handler.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DOCUMENT_HANDLER_H__
#define __DOCUMENT_HANDLER_H__

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define SAVE_METHOD_MOVE    0b01
#define SAVE_METHOD_REWRITE 0b10

typedef struct doc_s {
  gchar *name; /* NOTE: name and path have to be NULL or valid pointers! */
  gchar *path;
  guint32 id;

  guint8 day; /* 1 - 31 */
  guint8 month; /* 0 - 11 */
  guint16 year;

  gchar rotation;
  gchar old_rotation;

  gboolean use_counter;
  guint counter;
  guint8 counter_digits; /* in case global counter_digits gets changed */

  gchar require_save; /* 0b01 -> move, 0b10 -> rewrite */
  gchar *old_path;
} doc_t;

extern doc_t doc_cur;

extern gboolean doc_save(GdkPixbuf *pbuf);

extern void doc_prev(void);
extern void doc_next(void);
extern void doc_remove(void);

#endif /* __DOCUMENT_HANDLER_H__ */

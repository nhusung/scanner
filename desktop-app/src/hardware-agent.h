/*
 * hardware-agent.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HARDWARE_AGENT_H__
#define __HARDWARE_AGENT_H__

#include <glib.h>

#define HWS_PERFORMING 0b10000000
#define HWS_STOP       0b00000001
#define HWS_START      0b00000010
#define HWS_PAUSE      0b00000100
#define HWS_PHOTO      0b00001000

#define HW_IS_STOPPED(s) ((s & (HWS_PERFORMING | HWS_STOP)) == HWS_STOP)
#define HW_IS_STARTED(s) ((s & (HWS_PERFORMING | HWS_START)) == HWS_START)
#define HW_IS_PAUSED(s)  ((s & (HWS_PERFORMING | HWS_PAUSE)) == HWS_PAUSE)

extern void hw_start(GSourceFunc hw_cb, GSourceFunc cpt_cb, GSourceFunc dl_cb);
extern void hw_pause(void);
extern void hw_unpause(void);
extern void hw_continue(void);
extern void hw_stop(gboolean wait);

extern guint8 hw_get_state(void);

#endif /* __HARDWARE_AGENT_H__ */

/*
 * glib-gsettings-extension.h
 * This file is part of Scanner
 *
 * Copyright (C) 2016 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>

guchar g_settings_get_byte(GSettings *settings, const gchar *key);
gboolean g_settings_set_byte(GSettings *settings, const gchar *key, guchar value);

gint16 g_settings_get_int16(GSettings *settings, const gchar *key);
gboolean g_settings_set_int16(GSettings *settings, const gchar *key, gint16 value);

guint16 g_settings_get_uint16(GSettings *settings, const gchar *key);
gboolean g_settings_set_uint16(GSettings *settings, const gchar *key, guint16 value);


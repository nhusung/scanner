/*
 * document-view.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DOCUMENT_VIEW_H__
#define __DOCUMENT_VIEW_H__

#include <gtk/gtk.h>

#define SCALE_STEP 0.05
#define ROTATION_VALUE_MASK 0b00000011

struct image_s {
  GdkPixbuf *pixbuf;

  guint width;
  guint height;

  gboolean fit;
  gdouble scale;
  gdouble old_scale;

  guint offset_left;
  guint offset_top;

  gint old_view_width;
  gint old_view_height;
};
extern struct image_s image;

/**
 * @brief  Create a document view GtkWidget
 * @return The new widget
 */
extern GtkWidget *create_doc_view(void);

/**
 * @brief Load the image of the current document
 */
extern void doc_image_view_load_image(void);

/**
 * @brief Calculate the content's size and redraw if necessary
 */
extern void doc_image_view_size_calc(void);

#endif /* __DOCUMENT_VIEW_H__ */

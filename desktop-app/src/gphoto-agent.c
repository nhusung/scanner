/*
 * gphoto-agent.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gphoto-agent.h"
#include <gphoto2-camera.h>
#include <glib/gstdio.h>
#include <fcntl.h> /* open() constants */
#include <errno.h>
#include "settings-agent.h" /* file names */
#include "app-window.h"
#include "messages.h"

G_LOCK_DEFINE(camera_lock);

Camera *gphoto_cam;
GPContext *gphoto_context;
CameraFilePath camera_file_path;

error_t error;
gchar msg[1024];

gboolean series = FALSE;
gchar *filename = NULL;
guint filename_length;

GSourceFunc cpt_callback;

static void gphoto_err(GPContext *context, const char *msg, void *data);
static void gphoto_msg(GPContext *context, const char *msg, void *data);
static gboolean gphoto_init_if_needed(void);
static gpointer gphoto_thread_main(G_GNUC_UNUSED gpointer data);

/* For future versions: provide a way to select the camera. */


void gphoto_err(G_GNUC_UNUSED GPContext *context, const char *m,
                G_GNUC_UNUSED void *data) {
  debug_print("%s", msg);
  g_strlcpy(msg, m, 1024);
  error.msg = "GPhoto error";
  error.filename = NULL;
  error.reason = msg;
}
void gphoto_msg(G_GNUC_UNUSED GPContext *context, const char *m,
                G_GNUC_UNUSED void *data) {
  debug_print("%s", msg);
  g_strlcpy(msg, m, 1024);
  error.msg = "GPhoto message";
  error.filename = NULL;
  error.reason = msg;
}


gboolean gphoto_init_if_needed(void) {
  int res;
  if (gphoto_cam == NULL) {
    debug_print("gphoto_cam = NULL");
    int res = gp_camera_new(&gphoto_cam);
    if (res != GP_OK) {
      error.msg = ERR_CAM_CONN;
      error.reason = gp_port_result_as_string(res);
      error.filename = NULL;

      gphoto_cam = NULL;
      return FALSE;
    }
  }
  if (gphoto_context == NULL) {
    debug_print("gphoto_context = NULL");
    if ((gphoto_context = gp_context_new()) == NULL) {
      error.msg = ERR_CAM_CONN;
      error.reason = gp_port_result_as_string(GP_ERROR_NO_MEMORY);
      error.filename = NULL;
      return FALSE;
    }

    gp_context_set_error_func(gphoto_context, gphoto_err, NULL);
    gp_context_set_message_func(gphoto_context, gphoto_msg, NULL);

    if ((res = gp_camera_init(gphoto_cam, gphoto_context)) != GP_OK) {
      if (!error.msg) {
        error.msg = ERR_CAM_CONN;
        error.reason = gp_port_result_as_string(res);
        error.filename = NULL;
      }
      return FALSE;
    }
  }
  debug_print("success");
  return TRUE; /* Success */
}


error_t *gphoto_series_start(GSourceFunc capture_finished_callback) {
  /* here we are in main thread */
  G_LOCK(camera_lock);
  if (series) {
    error.msg = ERR_MULT_SERIES;
    error.filename = NULL;
    error.reason = NULL;
    G_UNLOCK(camera_lock);
    return &error;
  }

  if (!gphoto_init_if_needed()) {
    G_UNLOCK(camera_lock);
    return &error;
  }

  g_free(filename);
  filename_length = settings.capture_filename_length;
  filename = g_malloc(filename_length);
  g_sprintf(filename, "%s/00000000.jpg", settings.capture_folder);
  /* zeros get overwritten */

  series = TRUE;
  cpt_callback = capture_finished_callback;
  G_UNLOCK(camera_lock);

  return NULL; /* success */
}

error_t *gphoto_series_end(void) {
  G_LOCK(camera_lock);
  /* In case of an error this function could be called although the series has
   * already ended.  Thus we have to do the following check. */
  if (series) {
    series = FALSE;
    cpt_callback = NULL;
    if (gp_camera_exit(gphoto_cam, gphoto_context) != GP_OK) {
      G_UNLOCK(camera_lock);
      return &error;
    }
  }
  G_UNLOCK(camera_lock);
  return NULL; /* success */
}


void gphoto_capture(GSourceFunc callback) {
  g_assert(callback != NULL);

  G_LOCK(camera_lock);
  if (!series) {
    /* here we are in main thread */

    if (!gphoto_init_if_needed()) {
      series = FALSE;
      cpt_callback = NULL;
      G_UNLOCK(camera_lock);
      callback(&error);
      return;
    }

    g_free(filename);
    filename_length = settings.capture_filename_length;
    filename = g_malloc(filename_length);
    g_sprintf(filename, "%s/%08x.jpg", settings.capture_folder,
              settings.next_capture_id);
  } else {
    g_sprintf(&filename[filename_length - 13], "%08x.jpg",
              settings.next_capture_id); /* 8 * x + .jpg + \0 = 13 */
  }
  g_thread_unref(g_thread_new("gphoto", gphoto_thread_main, callback));
}

gpointer gphoto_thread_main(gpointer callback) {
  int fd, res;
  CameraFile *file;

  if (gp_camera_capture(gphoto_cam, GP_CAPTURE_IMAGE, &camera_file_path,
                        gphoto_context) != GP_OK)
    goto gphoto_err;

  /* Start download in a new thread */
  if (cpt_callback)
    g_idle_add(cpt_callback, NULL);

  /* open a file on computer */
  if ((fd = g_open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0644)) == -1) {
    error.msg = ERR_W_FILE;
    error.filename = filename;
    error.reason = g_strerror(errno);
    goto gphoto_err;
  }
  if ((res = gp_file_new_from_fd(&file, fd)) != GP_OK) {
    error.msg = ERR_DL;
    error.filename = NULL;
    error.reason = gp_port_result_as_string(res);
    goto gphoto_err;
  }

  /* copy photo */
  if (gp_camera_file_get(gphoto_cam, camera_file_path.folder,
                         camera_file_path.name, GP_FILE_TYPE_NORMAL, file,
                         gphoto_context) != GP_OK)
    goto gphoto_err;
  /* delete photo on camera */
  if (gp_camera_file_delete(gphoto_cam, camera_file_path.folder,
                            camera_file_path.name, gphoto_context) != GP_OK)
    goto gphoto_err;

  gp_file_free(file); /* no success checks here */

  /* wait for camera to complete task */ {
    int waittime = 2000;
    CameraEventType type;
    void *data;
    while (1) {
      gp_camera_wait_for_event(gphoto_cam, waittime, &type, &data,
                               gphoto_context); /* no success checks here */
      if (type == GP_EVENT_TIMEOUT) {
        break;
      } else if (type == GP_EVENT_CAPTURE_COMPLETE) {
        /* wait for a shorter amount of time and break this loop then */
        waittime = 100;
      } else if (type != GP_EVENT_UNKNOWN) {
        /* unexpected event */
      }
    }
  }

  if (!series) {
    /* release the camera, it will be automatically reinitialized */
    if (gp_camera_exit(gphoto_cam, gphoto_context) != GP_OK)
      goto gphoto_err;
  }

  settings.next_capture_id++;
  G_UNLOCK(camera_lock);
  g_idle_add((GSourceFunc) callback, NULL);

  return NULL;

gphoto_err: {
    series = FALSE;
    cpt_callback = NULL;
    G_UNLOCK(camera_lock);
    g_idle_add((GSourceFunc) callback, &error);
    return NULL;
  }
}

void gphoto_wait_finish(void) {
  G_LOCK(camera_lock);
  G_UNLOCK(camera_lock);
}

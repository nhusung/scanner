/*
 * glib-gsettings-extension.c
 * This file is part of Scanner
 *
 * Copyright (C) 2016 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "glib-gsettings-extension.h"

guchar g_settings_get_byte(GSettings *settings, const gchar *key) {
  GVariant *value;
  guchar result;

  value = g_settings_get_value(settings, key);
  result = g_variant_get_byte(value);
  g_variant_unref(value);

  return result;
}

gboolean g_settings_set_byte(GSettings *settings, const gchar *key,
                             guchar value) {
  return g_settings_set_value(settings, key, g_variant_new_byte(value));
}

gint16 g_settings_get_int16(GSettings *settings, const gchar *key) {
  GVariant *value;
  gint16 result;

  value = g_settings_get_value(settings, key);
  result = g_variant_get_int16(value);
  g_variant_unref(value);

  return result;
}

gboolean g_settings_set_int16(GSettings *settings, const gchar *key,
                              gint16 value) {
  return g_settings_set_value(settings, key, g_variant_new_int16(value));
}

guint16 g_settings_get_uint16(GSettings *settings, const gchar *key) {
  GVariant *value;
  guint16 result;

  value = g_settings_get_value(settings, key);
  result = g_variant_get_uint16(value);
  g_variant_unref(value);

  return result;
}

gboolean g_settings_set_uint16(GSettings *settings, const gchar *key,
                               guint16 value) {
  return g_settings_set_value(settings, key, g_variant_new_uint16(value));
}

/*
 * app-window.h
 * This file is part of Scanner
 *
 * Copyright (C) 2016-2017 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __APP_WINDOW_H__
#define __APP_WINDOW_H__

#include <gtk/gtk.h>

extern GtkWidget *counter_entry;

struct window_s {
  GtkWidget *widget;

  gint width;
  gint height;
};
extern struct window_s window;

extern void activate(GtkApplication *app, gpointer user_data);

#endif /* __APP_WINDOW_H__ */

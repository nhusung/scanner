/*
 * messages.h
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MESSAGES_H__
#define __MESSAGES_H__

#include <glib.h>

typedef struct error_s {
  const gchar *msg;
  const gchar *filename;
  const gchar *reason;
} error_t;

#define MSG_CHOOSE_DIR "Choose a folder"

#define MSG_SAVE_DIR "Choose a folder where to save your edited documents:"
#define MSG_CAPT_DIR "Choose a folder where to temporarily store captured\
 images: "

#define LBL_CAPT_DIR "Captures folder: "
#define DESC_CAPT_DIR "Directory to temporarily store captured photos in"

#define LBL_INO_PATH "Arduino device path: "
#define DESC_INO_PATH "Path to the serial terminal associated with the Arduino\
 (e.g. /dev/ttyACM0)"

#define LBL_DEFAULT_ROT "Default rotation: "
#define DESC_DEFAULT_ROT "Rotate every new picture"

#define LBL_COUNTER_DGTS "Counter digits: "
#define DESC_COUNTER_DGTS "Digits to prepend when counter is active"

#define LBL_HIST_LEN "History length: "
#define DESC_HIST_LEN "Maximum count of documents kept in history"


#define ERR_N_CAPT_DIR "Folder for temporarily storing captures does not exist!"
#define ERR_A_CAPT_DIR "No write access in captures directory!"

#define ERR_WRITE_DIR "Error: cannot not write at “%s”!"
#define ERR_EMPTY_NAME "Error: document name mustn't be empty!"
#define MSG_EXIST "“%s” already exists. Do you want to replace it?\n\n\
Attention: it is unlikely that the file you are overwriting is just an older\
 version of your current file, so you might want to change the name."
#define ERR_SAVE "Error saving “%s”: %s"
#define WARN_UTIME "Warning: couldn't set file modification date.\n%s"
#define WARN_TOO_OLD "The date is too old."

#define ERR_CAM_CONN "The connection to your camera couldn't be established!"
#define ERR_W_FILE "Could not open file “%s” for writing!"
#define ERR_DL "Could not download photo from camera!"
#define ERR_MULT_SERIES "Multiple series are not allowed!"

#define ERR_HW_OPEN "An error occured while opening connection to the hardware!"
#define ERR_HW_SETUP "An error occured while setting up the serial\
 communication with the hardware!"
#define ERR_HW_SELECT "An error occured while waiting for data from the\
 hardware!"
#define ERR_HW_READ "An error occured while reading data from the hardware!"
#define ERR_HW_WRITE "An error occured while writing data to the hardware!"
#define ERR_HW_TIMEOUT "Establishing the connection to the hardware timed out!"

#define ERR_INST "Fatal error loading file “%s”: %s\n\n\
It seems that scanner is not installed correctly."


#endif /* __MESSAGES_H__ */

/*
 * hardware-agent.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "hardware-agent.h"
#include <glib/gstdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include "settings-agent.h"
#include "gphoto-agent.h"
#include "messages.h"

#define SERIAL_BAUD B9600

volatile guint8 hw_state = HWS_STOP;
G_LOCK_DEFINE(mutex);
GThread *thread;

int dev_fd;

const char CONN_READY = 0x0f;
const char CONN_EXIT  = 0x00;
const char CONN_PAUSE = 0x01;
const char CONN_OK    = 0x02;
const char CONN_PHOTO = 0x0a;

error_t error = { NULL, NULL, NULL };

GSourceFunc download_cb;

static gpointer hw_thread_main(gpointer data);

/* connection to GUI */
void hw_start(GSourceFunc hw_cb, GSourceFunc cpt_cb, GSourceFunc dl_cb) {
  error_t *err;

  debug_print("");

  G_LOCK(mutex);
#if DEBUG == 1
  if (!HW_IS_STOPPED(hw_state)) {
    debug_print("ERROR: state = %x", hw_state);
    G_UNLOCK(mutex);
    return;
  }
#endif

  if ((err = gphoto_series_start(cpt_cb))) {
    G_UNLOCK(mutex);
    hw_cb(err);
    return;
  }
  download_cb = dl_cb;
  thread = g_thread_new("hardware", hw_thread_main, hw_cb);
  hw_state = HWS_START | HWS_PERFORMING;
}

void hw_pause(void) {
  G_LOCK(mutex);
#if DEBUG == 1
  if (!HW_IS_STARTED(hw_state)) {
    debug_print("ERROR: state = %x", hw_state);
    G_UNLOCK(mutex);
    return;
  }
#endif

  hw_state |= HWS_PAUSE | HWS_PERFORMING;
  hw_state &= ~HWS_START;
  write(dev_fd, &CONN_PAUSE, 1);
  G_UNLOCK(mutex);
}

void hw_unpause(void) {
  G_LOCK(mutex);
#if DEBUG == 1
  if (!HW_IS_PAUSED(hw_state)) {
    debug_print("ERROR: state = %x", hw_state);
    G_UNLOCK(mutex);
    return;
  }
#endif

  hw_state |= HWS_START | HWS_PERFORMING;
  hw_state &= ~HWS_PAUSE;
  write(dev_fd, &CONN_PAUSE, 1);
  G_UNLOCK(mutex);
}

void hw_continue(void) {
  G_LOCK(mutex);
#if DEBUG == 1
  if (!(hw_state & HWS_PHOTO)) {
    debug_print("ERROR: state = %x", hw_state);
    G_UNLOCK(mutex);
    return;
  }
#endif

  hw_state &= ~HWS_PHOTO;
  write(dev_fd, &CONN_OK, 1);
  G_UNLOCK(mutex);
}

void hw_stop(gboolean wait) {
  debug_print("state = %x", hw_state);

  G_LOCK(mutex);
  if (!HW_IS_STOPPED(hw_state)) {
    hw_state = HWS_STOP | HWS_PERFORMING;
    write(dev_fd, &CONN_EXIT, 1);
  }
  G_UNLOCK(mutex);

  if (wait)
    g_thread_join(thread);
  else
    g_thread_unref(thread);
}


gpointer hw_thread_main(gpointer callback) {
  struct termios config;

  error.msg = NULL;

  /* open device */
  if ((dev_fd = open(settings.tty, O_RDWR | O_NOCTTY)) == -1) {
    error.msg = ERR_HW_OPEN;
    error.reason = g_strerror(errno);
    goto scan_end;
  }

  /* wait for the Arduino to reboot */
  usleep(3500000);

  /* Arduino settings: 8 bits, no parity, no stop bits */
  config.c_cflag = CS8 | CREAD | CLOCAL;
  /* raw mode */
  config.c_iflag = 0;
  config.c_oflag = 0;
  config.c_lflag = 0;
  config.c_cc[VMIN] = 1;
  config.c_cc[VTIME] = 0;

  /* set baud on input and output, commit the serial port settings */
  if (cfsetispeed(&config, SERIAL_BAUD) == -1
      || cfsetospeed(&config, SERIAL_BAUD) == -1
      || tcsetattr(dev_fd, TCSANOW, &config) == -1) {
    error.msg = ERR_HW_SETUP;
    error.reason = g_strerror(errno);

    close(dev_fd);
    goto scan_end;
  }

  /* wait for connection to establish */ {
    char buf;
    fd_set set;
    int rv, tries_left = 100;
    struct timeval timeout;

    while (tries_left > 0) {
      tries_left--;

      /* see if there are new bytes in the pipe */
      FD_ZERO(&set);
      FD_SET(dev_fd, &set);
      timeout.tv_sec = 0;
      timeout.tv_usec = 100000;
      rv = select(dev_fd + 1, &set, NULL, NULL, &timeout);

      if (rv == -1) { /* error */
        close(dev_fd);

        error.msg = ERR_HW_SELECT;
        error.reason = g_strerror(errno);
        goto scan_end;
      }
      if (rv == 0) /* timeout */
        continue;

      if (read(dev_fd, &buf, 1) < 0) {
        error.msg = ERR_HW_READ;
        error.reason = g_strerror(errno);
        close(dev_fd);
        goto scan_end;
      }

      if (buf == CONN_READY) {
        if (write(dev_fd, &CONN_READY, 1) < 0) {
          error.msg = ERR_HW_WRITE;
          error.reason = g_strerror(errno);
          close(dev_fd);
          goto scan_end;
        }
        hw_state &= ~HWS_PERFORMING;
        g_idle_add((GSourceFunc) callback, NULL);
        goto scan_main; /* connection established */
      }
    }

    /* the connection could not be established within 100 tries -> exit */
    close(dev_fd);
    error.msg = ERR_HW_TIMEOUT;
    error.reason = NULL;
    goto scan_end;
  }

scan_main: {
    char buf;

    G_UNLOCK(mutex);
    while (read(dev_fd, &buf, 1)) {
      G_LOCK(mutex);
      if (buf == CONN_EXIT) {
        write(dev_fd, &CONN_OK, 1);
        break;
      }
      if (buf == CONN_PAUSE) {
        write(dev_fd, &CONN_OK, 1);
        if (HW_IS_PAUSED(hw_state)) { /* unpause */
          hw_state |= HWS_START;
          hw_state &= ~HWS_PAUSE;
          g_idle_add((GSourceFunc) callback, NULL);
        } else { /* pasue */
          hw_state |= HWS_PAUSE;
          hw_state &= ~HWS_START;
          g_idle_add((GSourceFunc) callback, NULL);
        }
      } else if (buf == CONN_OK) {
        hw_state &= ~HWS_PERFORMING;
        g_idle_add((GSourceFunc) callback, NULL);
      } else if (buf == CONN_PHOTO) {
        hw_state |= HWS_PHOTO;
        gphoto_capture(download_cb);
      }
      G_UNLOCK(mutex);
    }
  }

  close(dev_fd);

scan_end:
  hw_state = HWS_STOP;

  /* end photo series */ {
    error_t *err = gphoto_series_end();
    if (err)
      g_idle_add((GSourceFunc) callback, err);
  }

  if (error.msg)
    g_idle_add((GSourceFunc) callback, &error);
  else
    g_idle_add((GSourceFunc) callback, NULL);
  G_UNLOCK(mutex);

  return NULL;
}

guint8 hw_get_state(void) {
  guint8 val;

  G_LOCK(mutex);
  val = hw_state;
  G_UNLOCK(mutex);

  return val;
}

/*
 * settings-agent.c
 * This file is part of Scanner
 *
 * Copyright (C) 2017-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "settings-agent.h"
#include "gphoto-agent.h"
#include "app-window.h"
#include "document-view.h"
#include "document-handler.h"
#include <glib/gstdio.h>
#include <string.h> /* strlen() */
#include <time.h> /* get current date */
#include <math.h> /* pow() */

struct settings_s settings;


void load_settings(void) {
  settings.g = g_settings_new("de.husung.scanner");

  /* g_strdup() strings to be able to free() the results */
  settings.capture_folder
      = g_strdup(g_settings_get_string(settings.g, "capture-folder"));
  settings.capture_filename_length
      = strlen(settings.capture_folder) + ADD_PATH_LENGTH;
  settings.tty = g_strdup(g_settings_get_string(settings.g, "tty"));
  settings.default_rotation
      = g_settings_get_byte(settings.g, "default-rotation");
  settings.counter_digits = g_settings_get_byte(settings.g, "counter-digits");
  if (settings.counter_digits == 10)
    settings.counter_max = G_MAXUINT32;
  else
    settings.counter_max = pow(10, settings.counter_digits) - 1;

  settings.next_capture_id = g_settings_get_uint(settings.g, "next-capture-id");

  window.width = g_settings_get_uint16(settings.g, "window-width");
  window.height = g_settings_get_uint16(settings.g, "window-height");

  /* history
   *
   * If scanner is force killed (or crashes), data saved in GSettings can be
   * corrupted.  But due to some restrictions to the history “system” many
   * issues can be fixed, so the user doesn't have to deal with coredumps.
   */
  settings.hist_sel = g_settings_get_uint16(settings.g, "history-selected");

  settings.hist_max_length
      = g_settings_get_uint16(settings.g, "history-length");
  if (settings.hist_max_length <= settings.hist_sel)
    settings.hist_max_length = settings.hist_sel + 1;

  settings.hist = g_malloc0(settings.hist_max_length * sizeof (GVariant *));
  /* get settings.hist values */ {
    GVariant *gv_hist = g_settings_get_value(settings.g, "history");

    settings.hist_length = g_variant_n_children(gv_hist);
    for (guint i = 0; i < settings.hist_length; i++)
      settings.hist[i] = g_variant_get_child_value(gv_hist, i);

    g_variant_unref(gv_hist);
  }

  if (settings.hist_length == 0) {
    time_t secs = time(NULL);
    struct tm *date = localtime(&secs);

    settings.hist_length = 1;
    settings.hist_newest = 0;

    /* initialize new document */
    doc_cur.id = settings.next_capture_id;
    doc_cur.name = NULL;
    doc_cur.path = NULL;
    doc_cur.year = date->tm_year + 1900;
    doc_cur.month = date->tm_mon;
    doc_cur.day = date->tm_mday;
    doc_cur.rotation = settings.default_rotation;
    doc_cur.counter = 1;
    doc_cur.require_save = SAVE_METHOD_REWRITE;
    doc_cur.old_path = NULL;
  } else {
    settings.hist_newest = g_settings_get_uint16(settings.g, "history-newest");
    if (settings.hist_newest >= settings.hist_length)
      settings.hist_newest = settings.hist_length - 1;

    /* don't load inexistent documents */
    if (settings.hist_sel >= settings.hist_length)
      settings.hist_sel = settings.hist_length - 1;
    load_document();
  }
}

void load_document(void) {
  GVariant *gv_doc = settings.hist[HIST_INDEX(settings.hist_sel)];

  GVariant *gv_val = g_variant_get_child_value(gv_doc, 0);
  doc_cur.id = g_variant_get_uint32(gv_val);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 1);
  g_free(doc_cur.path);
  doc_cur.path = g_variant_dup_string(gv_val, NULL);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 2);
  g_free(doc_cur.name);
  doc_cur.name = g_variant_dup_string(gv_val, NULL);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 3);
  doc_cur.rotation = g_variant_get_byte(gv_val);
  doc_cur.old_rotation = doc_cur.rotation;
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 4);
  doc_cur.year = g_variant_get_uint16(gv_val);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 5);
  if ((doc_cur.month = g_variant_get_byte(gv_val)) > 11)
    doc_cur.month = 11;
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 6);
  if ((doc_cur.day = g_variant_get_byte(gv_val)) == 0) { /* current date */
    time_t secs = time(NULL);
    struct tm *date = localtime(&secs);
    doc_cur.year = date->tm_year + 1900;
    doc_cur.month = date->tm_mon;
    doc_cur.day = date->tm_mday;
  } else { /* correct date */
    /* Functions in gdate.h use GDateMonth enum, there January is 1, not 0. */
    guint8 monlen = g_date_get_days_in_month(doc_cur.month + 1, doc_cur.year);
    if (doc_cur.day > monlen)
      doc_cur.day = monlen;
  }
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 7);
  doc_cur.use_counter = g_variant_get_boolean(gv_val);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 8);
  if ((doc_cur.counter = g_variant_get_uint32(gv_val)) > settings.counter_max)
    doc_cur.counter = 1;
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 9);
  doc_cur.counter_digits = g_variant_get_byte(gv_val);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 10);
  doc_cur.require_save = g_variant_get_byte(gv_val);
  g_variant_unref(gv_val);

  gv_val = g_variant_get_child_value(gv_doc, 11);
  g_free(doc_cur.old_path);
  doc_cur.old_path = g_variant_dup_string(gv_val, NULL);
  g_variant_unref(gv_val);
}

void save_history(void) {
  debug_print("");

  g_settings_set_uint16(settings.g, "history-selected", settings.hist_sel);
  g_settings_set_uint16(settings.g, "history-newest", settings.hist_newest);

  /* save document */ {
    const gchar *empty = "";
    guint16 i = HIST_INDEX(settings.hist_sel);
    GVariant *gv_doc[] = {
      g_variant_new_uint32(doc_cur.id),
      g_variant_new_string(doc_cur.path ? doc_cur.path : empty),
      g_variant_new_string(doc_cur.name ? doc_cur.name : empty),
      g_variant_new_byte(doc_cur.rotation & ROTATION_VALUE_MASK),
      g_variant_new_uint16(doc_cur.year),
      g_variant_new_byte(doc_cur.month),
      g_variant_new_byte(doc_cur.day),
      g_variant_new_boolean(doc_cur.use_counter),
      g_variant_new_uint32(doc_cur.counter),
      g_variant_new_byte(doc_cur.counter_digits),
      g_variant_new_byte(doc_cur.require_save),
      g_variant_new_string(doc_cur.old_path ? doc_cur.old_path : empty)
    };

    if (settings.hist[i])
      g_variant_unref(settings.hist[i]);

    settings.hist[i]
        = g_variant_ref_sink(g_variant_new_tuple(gv_doc, G_N_ELEMENTS(gv_doc)));
  }

  g_settings_set_value(settings.g, "history",
                       g_variant_new_array(NULL, settings.hist,
                                           settings.hist_length));
}

void resize_history(guint length) {
  if (length == settings.hist_max_length)
    return; /* nothing changed */

  /* The “new” history has to include at least the documents from the current
   * one to the newest one! */
  g_assert_cmpuint(length, >, settings.hist_sel);

  if (settings.hist_newest == settings.hist_length - 1
      && length > settings.hist_length) {
    /* All entries are placed in a way that the oldest one is the first element
     * of the array and relatively to the used part of the history we are
     * expanding.  That gives us the ability to use g_realloc() here. */
    settings.hist = g_realloc(settings.hist, length * sizeof (GVariant *));
  } else {
    /* In every other case we need to realign the history: */
    GVariant **tmp = g_malloc(length * sizeof (GVariant *));
    /* Start with the oldest document (-1) if all fit into the new array or the
     * oldest possible (length - 1) if this is not the case. */
    guint iin = HIST_INDEX(length >= settings.hist_length ?
                           -1 : (gint) length - 1);
    guint iout = 0;
    while (iout < settings.hist_length) {
      tmp[iout] = settings.hist[iin];
      iin = (iin + 1) % settings.hist_max_length;
      iout++;
    }

    g_free(settings.hist);
    settings.hist = tmp;
  }
  /* Initialize new memory to NULL so that we can decide wheter a
   * g_variant_unref() is needed. */
  memset(&settings.hist[settings.hist_length], 0,
         (length - settings.hist_length) * sizeof (GVariant *));

  settings.hist_max_length = length;
  g_settings_set_uint16(settings.g, "history-length", length);
}



void save_settings_of_dialog(void) {
  g_settings_set_string(settings.g, "capture-folder", settings.capture_folder);
  g_settings_set_string(settings.g, "tty", settings.tty);
  g_settings_set_byte(settings.g, "default-rotation",
                      settings.default_rotation);
  g_settings_set_byte(settings.g, "counter-digits", settings.counter_digits);
}


validate_status validate_folder(const gchar *path) {
  GStatBuf stat_buf;
  if (path == NULL)
    return VALIDATE_ERR_NO_DIR;
  if (g_stat(path, &stat_buf) != 0 || (stat_buf.st_mode & S_IFMT) != S_IFDIR)
    return VALIDATE_ERR_NO_DIR;
  if (g_access(path, R_OK | W_OK | X_OK) != 0)
    return VALIDATE_ERR_NO_ACCESS;
  return VALIDATE_OK;
}

validate_status validate_tty(const gchar *path) {
  GStatBuf stat_buf;
  if (g_stat(path, &stat_buf) != 0 || (stat_buf.st_mode & S_IFMT) != S_IFCHR)
    return VALIDATE_ERR_NO_CHAR_SPECIAL;
  if (g_access(path, R_OK | W_OK) != 0)
    return VALIDATE_ERR_NO_ACCESS;
  return VALIDATE_OK;
}

/*
 * app-window.c
 * This file is part of Scanner
 *
 * Copyright (C) 2016-2018 - Nils Husung
 *
 * Scanner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scanner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scanner. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "app-window.h"
#include "settings-agent.h"
#include "gphoto-agent.h"
#include <math.h> /* pow() */
#include <stdlib.h> /* exit() */
#include "document-view.h"
#include "document-handler.h"
#include "settings-window.h"
#include "settings-agent.h"
#include "hardware-agent.h"
#include "messages.h"

GtkWidget *prev_btn, *next_btn,
  *rotate_left_btn, *rotate_right_btn,
  *zoom_orig_btn, *zoom_in_btn, *zoom_out_btn, *zoom_fit_btn,
  *delete_btn,
  *start_pause_btn, *stop_btn, *capture_single_picture_btn,
  *settings_btn,
  *use_counter_btn,
  *save_folder_entry, *doc_name_entry, *calendar, *counter_entry;

gboolean exiting = FALSE;

#define HW_START_IMG gtk_image_new_from_icon_name("media-playback-start", \
                                                  GTK_ICON_SIZE_BUTTON)
#define HW_PAUSE_IMG gtk_image_new_from_icon_name("media-playback-pause", \
                                                  GTK_ICON_SIZE_BUTTON)

struct window_s window;

static void store_metadata(void);
static gboolean on_window_close(GtkWidget *widget, GdkEvent *e, gpointer data);

static gboolean handle_hardware_event(gpointer err);
static gboolean capture_single_picture_finished(gpointer err);

/* MARK: utilities */
static void set_tools_sensitivity(gboolean state) {
  gtk_widget_set_sensitive(next_btn, state);
  gtk_widget_set_sensitive(delete_btn, state);
  gtk_widget_set_sensitive(rotate_left_btn, state);
  gtk_widget_set_sensitive(rotate_right_btn, state);
  gtk_widget_set_sensitive(zoom_orig_btn, state);
  gtk_widget_set_sensitive(zoom_in_btn, state);
  gtk_widget_set_sensitive(zoom_out_btn, state);
  gtk_widget_set_sensitive(zoom_fit_btn, state);
}

static void reset_hw_buttons(void) {
  gtk_widget_set_sensitive(start_pause_btn, TRUE);
  gtk_widget_set_sensitive(stop_btn, FALSE);
  gtk_widget_set_sensitive(capture_single_picture_btn, TRUE);
}

static void display_error(error_t *err) {
  GtkWidget *d;
  debug_print("error: msg = %s, file = %s, reason = %s",
              err->msg, err->filename, err->reason);

  if (err->filename)
    d = gtk_message_dialog_new(GTK_WINDOW(window.widget), GTK_DIALOG_MODAL,
                               GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                               err->msg, err->filename);
  else
    d = gtk_message_dialog_new(GTK_WINDOW(window.widget), GTK_DIALOG_MODAL,
                               GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                               "%s", err->msg);

  if (err->reason)
    gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(d),
                                             "%s", err->reason);

  gtk_dialog_run(GTK_DIALOG(d));
  gtk_widget_destroy(d);
}


/* MARK: zooming */
static void zoom_orig(G_GNUC_UNUSED GtkWidget *widget,
                      G_GNUC_UNUSED gpointer data) {
  image.scale = 1;
  image.fit = FALSE;
  doc_image_view_size_calc();
}

static void zoom_in(G_GNUC_UNUSED GtkWidget *widget,
                    G_GNUC_UNUSED gpointer data) {
  image.scale += SCALE_STEP;
  image.fit = FALSE;
  doc_image_view_size_calc();
}

static void zoom_out(G_GNUC_UNUSED GtkWidget *widget,
                     G_GNUC_UNUSED gpointer data) {
  image.scale -= SCALE_STEP;
  image.fit = FALSE;
  doc_image_view_size_calc();
}

static void zoom_fit(G_GNUC_UNUSED GtkWidget *widget,
                     G_GNUC_UNUSED gpointer data) {
  image.fit = TRUE;
  doc_image_view_size_calc();
}


/* MARK: document history */
static void doc_changed_ui_update(void) {
  if (doc_cur.path)
    gtk_entry_set_text(GTK_ENTRY(save_folder_entry), doc_cur.path);
  if (doc_cur.name)
    gtk_entry_set_text(GTK_ENTRY(doc_name_entry), doc_cur.name);
  gtk_calendar_select_month(GTK_CALENDAR(calendar),
                            doc_cur.month, doc_cur.year);
  gtk_calendar_select_day(GTK_CALENDAR(calendar), doc_cur.day);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_counter_btn),
                               doc_cur.use_counter);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(counter_entry), doc_cur.counter);
  doc_image_view_load_image();
}

static void prev_doc(G_GNUC_UNUSED GtkWidget *widget,
                     G_GNUC_UNUSED gpointer data) {
  store_metadata();
  doc_prev();
  if (settings.hist_sel >= settings.hist_length - 1)
    gtk_widget_set_sensitive(prev_btn, FALSE);
  set_tools_sensitivity(TRUE);

  doc_changed_ui_update();
}

static void next_doc(G_GNUC_UNUSED GtkWidget *widget,
                     G_GNUC_UNUSED gpointer data) {
  store_metadata();
  if (!doc_save(image.pixbuf))
    return; /* saving wasn't successful */

  doc_next();
  if (doc_cur.id == settings.next_capture_id)
    set_tools_sensitivity(FALSE);
  gtk_widget_set_sensitive(prev_btn, TRUE);

  doc_changed_ui_update();
}

static void delete_doc(G_GNUC_UNUSED GtkWidget *widget,
                       G_GNUC_UNUSED gpointer data) {
  doc_remove();
  doc_changed_ui_update();
  if (doc_cur.id == settings.next_capture_id) /* newest document was deleted */
    set_tools_sensitivity(FALSE); /* don't show tools for wait.png */
}


/* MARK: rotating */
static void rotate_left(G_GNUC_UNUSED GtkWidget *widget,
                        G_GNUC_UNUSED gpointer data) {
  gint size_tmp;
  GdkPixbuf *pbuf_tmp;

  pbuf_tmp = gdk_pixbuf_rotate_simple(image.pixbuf,
                                      GDK_PIXBUF_ROTATE_COUNTERCLOCKWISE);
  g_object_unref(image.pixbuf);
  image.pixbuf = pbuf_tmp;
  doc_cur.rotation += 5; /* changed range = 4+ */

  size_tmp = image.height;
  image.height = image.width;
  image.width = size_tmp;

  doc_image_view_size_calc();
}

static void rotate_right(G_GNUC_UNUSED GtkWidget *widget,
                         G_GNUC_UNUSED gpointer data) {
  gint size_tmp;
  GdkPixbuf *pbuf_tmp;

  pbuf_tmp = gdk_pixbuf_rotate_simple(image.pixbuf,
                                      GDK_PIXBUF_ROTATE_CLOCKWISE);
  g_object_unref(image.pixbuf);
  image.pixbuf = pbuf_tmp;
  doc_cur.rotation += 7; /* changed range 4+ */

  size_tmp = image.height;
  image.height = image.width;
  image.width = size_tmp;

  doc_image_view_size_calc();
}


/* MARK: hardware control */
static gboolean capture_series_picture_finished(G_GNUC_UNUSED gpointer data) {
  hw_continue();
  return G_SOURCE_REMOVE;
}
static gboolean download_series_picture_finished(gpointer err) {
  if (err) {
    if (!HW_IS_STOPPED(hw_get_state()))
      hw_stop(FALSE);
    else
      reset_hw_buttons();
    display_error(err);
  } else {
    if (doc_cur.id == settings.next_capture_id - 1) {
      /* Before capturing, the id of the current document pointed to an empty
       * document, but now we can display it! */
      doc_image_view_load_image();
      set_tools_sensitivity(TRUE);
    }
    g_settings_set_uint(settings.g, "next-capture-id",
                        settings.next_capture_id);
  }
  return G_SOURCE_REMOVE;
}

static void start_pause_scan(GtkWidget *widget, G_GNUC_UNUSED gpointer data) {
  guint8 hw_state = hw_get_state();
  if (HW_IS_STOPPED(hw_state)) {
    gtk_widget_set_sensitive(capture_single_picture_btn, FALSE);
    gtk_widget_set_sensitive(widget, FALSE);

    hw_start(handle_hardware_event, capture_series_picture_finished,
             download_series_picture_finished);
  } else if (HW_IS_STARTED(hw_state)) {
    gtk_widget_set_sensitive(widget, FALSE);

    hw_pause();
  } else if (HW_IS_PAUSED(hw_state)) {
    gtk_widget_set_sensitive(widget, FALSE);

    hw_unpause();
  }
}

static void stop_scan(GtkWidget *widget, G_GNUC_UNUSED gpointer data) {
  gtk_widget_set_sensitive(widget, FALSE);
  gtk_widget_set_sensitive(start_pause_btn, FALSE);

  hw_stop(FALSE);
}

gboolean handle_hardware_event(gpointer err) {
  guint8 hw_state;

  if (exiting) /* widgets might not exist anymore */
    return G_SOURCE_REMOVE; /* just quit */

  hw_state = hw_get_state();

  if (err) /* no need to check stopped here */
    display_error(err);

  if (HW_IS_STOPPED(hw_state)) {
    gtk_button_set_image(GTK_BUTTON(start_pause_btn), HW_START_IMG);
    reset_hw_buttons();
  } else if (HW_IS_PAUSED(hw_state)) {
    gtk_button_set_image(GTK_BUTTON(start_pause_btn), HW_START_IMG);
    gtk_widget_set_sensitive(start_pause_btn, TRUE);
  } else if (HW_IS_STARTED(hw_state)) {
    gtk_button_set_image(GTK_BUTTON(start_pause_btn), HW_PAUSE_IMG);
    gtk_widget_set_sensitive(start_pause_btn, TRUE);
    gtk_widget_set_sensitive(stop_btn, TRUE);
  }

  return G_SOURCE_REMOVE; /* remove from loop */
}

static void capture_single_picture(G_GNUC_UNUSED GtkWidget *widget,
                                   G_GNUC_UNUSED gpointer data) {
  gtk_widget_set_sensitive(start_pause_btn, FALSE);
  gtk_widget_set_sensitive(capture_single_picture_btn, FALSE);
  gphoto_capture(capture_single_picture_finished);
}
gboolean capture_single_picture_finished(gpointer err) {
  if (err)
    display_error(err);
  else
    g_settings_set_uint(settings.g, "next-capture-id",
                        settings.next_capture_id);

  gtk_widget_set_sensitive(start_pause_btn, TRUE);
  gtk_widget_set_sensitive(capture_single_picture_btn, TRUE);

  if (doc_cur.id == settings.next_capture_id - 1) {
    /* Before capturing, the id of the current document pointed to an empty
     * document, but now we can display it! */
    doc_image_view_load_image();
    set_tools_sensitivity(TRUE);
  }
  return G_SOURCE_REMOVE; /* remove from loop */
}


/* MARK: document properties */
static void choose_save_folder(G_GNUC_UNUSED GtkWidget *widget,
                               G_GNUC_UNUSED gpointer data) {
  const gchar *tmp;
  GtkWidget *d
      = gtk_file_chooser_dialog_new(MSG_SAVE_DIR,
                                    GTK_WINDOW(window.widget),
                                    GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                    "_Cancel", GTK_RESPONSE_CANCEL,
                                    "_Open", GTK_RESPONSE_ACCEPT, NULL);


  gtk_window_set_icon_name(GTK_WINDOW(d), ICON_NAME);

  tmp = gtk_entry_get_text(GTK_ENTRY(save_folder_entry));
  if (validate_folder(tmp) == VALIDATE_OK)
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(d), tmp);

  if (gtk_dialog_run(GTK_DIALOG(d)) == GTK_RESPONSE_ACCEPT) {
    tmp = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(d));
    if (tmp) /* file chooser can return NULL */
      gtk_entry_set_text(GTK_ENTRY(save_folder_entry), tmp);
  }

  gtk_widget_destroy(d);
}

static void on_use_counter_toggled(GtkWidget *w, G_GNUC_UNUSED gpointer data) {
  gtk_widget_set_sensitive(counter_entry,
                           gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w)));
}

static void store_metadata(void) {
  /* path */ {
    const gchar *tmp = gtk_entry_get_text(GTK_ENTRY(save_folder_entry));
    if (g_strcmp0(tmp, doc_cur.path) != 0) {
      doc_cur.path = g_strdup(tmp);
      doc_cur.require_save |= SAVE_METHOD_MOVE;
    }
  }
  /* name */ {
    const gchar *tmp = gtk_entry_get_text(GTK_ENTRY(doc_name_entry));
    if (g_strcmp0(tmp, doc_cur.name) != 0) {
      doc_cur.name = g_strdup(tmp);
      doc_cur.require_save |= SAVE_METHOD_MOVE;
    }
  }

  /* calendar */ {
    guint day;
    guint month;
    guint year;
    gtk_calendar_get_date(GTK_CALENDAR(calendar), &year, &month, &day);

    if (year != doc_cur.year) {
      doc_cur.year = year;
      doc_cur.require_save |= SAVE_METHOD_REWRITE;
    }
    if (month != doc_cur.month) {
      doc_cur.month = month;
      doc_cur.require_save |= SAVE_METHOD_REWRITE;
    }
    if (day != doc_cur.day) {
      doc_cur.day = day;
      doc_cur.require_save |= SAVE_METHOD_REWRITE;
    }
  }

  /* rotation */
  if (doc_cur.old_rotation != doc_cur.rotation)
    doc_cur.require_save |= SAVE_METHOD_REWRITE;

  /* use counter */ {
    gboolean tmp
        = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_counter_btn));
    if (tmp != doc_cur.use_counter) {
      doc_cur.use_counter = tmp;
      doc_cur.require_save |= SAVE_METHOD_MOVE;
    }
  }
  /* counter value */ {
    guint c = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(counter_entry));
    if (c != doc_cur.counter) {
      doc_cur.counter = c;
      if (doc_cur.use_counter)
        doc_cur.require_save |= SAVE_METHOD_MOVE;
    }
  }
}

void chk_capture_dir(void) {
  while (TRUE) {
    GtkWidget *dialog, *content_area, *label;
    gint res;
    gint status;

    if ((status = validate_folder(settings.capture_folder)) == VALIDATE_OK)
      break;

    dialog = gtk_dialog_new_with_buttons("Scanner", GTK_WINDOW(window.widget),
                                         GTK_DIALOG_MODAL,
                                         "_Select a folder",
                                         GTK_RESPONSE_ACCEPT,
                                         "_Retry",
                                         GTK_RESPONSE_REJECT,
                                         "_Cancel",
                                         GTK_RESPONSE_CANCEL,
                                         NULL);

    gtk_window_set_icon_name(GTK_WINDOW(dialog), ICON_NAME);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_ACCEPT);

    content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

    if (status == VALIDATE_ERR_NO_DIR)
      label = gtk_label_new(ERR_N_CAPT_DIR);
    else
      label = gtk_label_new(ERR_A_CAPT_DIR);

    gtk_container_add(GTK_CONTAINER(content_area), label);
    gtk_container_set_border_width(GTK_CONTAINER(content_area), 40);
    gtk_widget_show_all(dialog);

    res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_CANCEL)
      exit(EXIT_FAILURE);
    if (res == GTK_RESPONSE_ACCEPT)
      choose_captures_folder(NULL, NULL);

    gtk_widget_destroy(dialog);
  }
}

/* MARK: window lifecycle */
void activate(GtkApplication *app, G_GNUC_UNUSED gpointer data) {
  GtkWidget *header_bar, *segment_grid, *aside_vbox, *main_hpaned,
      *choose_save_folder_button;
  GtkStyleContext *segment_grid_style_context;

  image.pixbuf = NULL;

  /* app window */
  window.widget = gtk_application_window_new(app);
  gtk_window_set_icon_name(GTK_WINDOW(window.widget), ICON_NAME);
  gtk_window_set_default_size(GTK_WINDOW(window.widget), window.width,
                              window.height);
  g_signal_connect(window.widget, "delete-event", G_CALLBACK(on_window_close),
                   NULL);

  chk_capture_dir();

  /* header bar */
  header_bar = gtk_header_bar_new();
  gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header_bar), TRUE);
  gtk_header_bar_set_title(GTK_HEADER_BAR(header_bar), "Scanner");
  gtk_window_set_titlebar(GTK_WINDOW(window.widget), header_bar);

  /* header bar -> nav segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_container_add(GTK_CONTAINER(header_bar), segment_grid);

  /* header bar -> nav segment grid -> prev button */
  prev_btn = gtk_button_new_from_icon_name("go-previous", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(prev_btn, "Previous document");
  g_signal_connect(prev_btn, "clicked", G_CALLBACK(prev_doc), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), prev_btn);

  /* header bar -> nav segment grid -> next button */
  next_btn = gtk_button_new_from_icon_name("go-next", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(next_btn, "Next document");
  g_signal_connect(next_btn, "clicked", G_CALLBACK(next_doc), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), next_btn);

  /* header bar -> rotate segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_container_add(GTK_CONTAINER(header_bar), segment_grid);

  /* header bar -> rotate segment grid -> rotate left button */
  rotate_left_btn = gtk_button_new_from_icon_name("object-rotate-left",
                                                  GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(rotate_left_btn, "Rotate 90° counterclockwise");
  g_signal_connect(rotate_left_btn, "clicked", G_CALLBACK(rotate_left), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), rotate_left_btn);

  /* header bar -> rotate segment grid -> rotate right button */
  rotate_right_btn = gtk_button_new_from_icon_name("object-rotate-right",
                                                   GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(rotate_right_btn, "Rotate 90° clockwise");
  g_signal_connect(rotate_right_btn, "clicked", G_CALLBACK(rotate_right), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), rotate_right_btn);

  /* header bar -> zoom segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_container_add(GTK_CONTAINER(header_bar), segment_grid);

  /* header bar -> zoom segment grid -> zoom orig button */
  zoom_orig_btn = gtk_button_new_from_icon_name("zoom-original",
                                                GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(zoom_orig_btn, "Zoom original");
  g_signal_connect(zoom_orig_btn, "clicked", G_CALLBACK(zoom_orig), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), zoom_orig_btn);

  /* header bar -> zoom segment grid -> zoom in button */
  zoom_in_btn = gtk_button_new_from_icon_name("zoom-in", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(zoom_in_btn, "Zoom in");
  g_signal_connect(zoom_in_btn, "clicked", G_CALLBACK(zoom_in), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), zoom_in_btn);

  /* header bar -> zoom segment grid -> zoom out button */
  zoom_out_btn = gtk_button_new_from_icon_name("zoom-out",
                                               GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(zoom_out_btn, "Zoom out");
  g_signal_connect(zoom_out_btn, "clicked", G_CALLBACK(zoom_out), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), zoom_out_btn);

  /* header bar -> zoom segment grid -> zoom fit button */
  zoom_fit_btn = gtk_button_new_from_icon_name("zoom-fit-best",
                                               GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(zoom_fit_btn, "Zoom fit best");
  g_signal_connect(zoom_fit_btn, "clicked", G_CALLBACK(zoom_fit), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), zoom_fit_btn);

  /* header bar -> delete button */
  delete_btn = gtk_button_new_from_icon_name("user-trash",
                                             GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(delete_btn, "Delete current document");
  g_signal_connect(delete_btn, "clicked", G_CALLBACK(delete_doc), NULL);
  gtk_container_add(GTK_CONTAINER(header_bar), delete_btn);

  /* header bar -> control segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_container_add(GTK_CONTAINER(header_bar), segment_grid);

  /* header bar -> control segment grid -> start / pause button */
  start_pause_btn = gtk_button_new_from_icon_name("media-playback-start",
                                                  GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(start_pause_btn, "Start");
  g_signal_connect(start_pause_btn, "clicked", G_CALLBACK(start_pause_scan),
                   NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), start_pause_btn);

  /* header bar -> control segment grid -> stop button */
  stop_btn = gtk_button_new_from_icon_name("media-playback-stop",
                                           GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(stop_btn, "Stop");
  g_signal_connect(stop_btn, "clicked", G_CALLBACK(stop_scan), NULL);
  gtk_widget_set_sensitive(stop_btn, FALSE);
  gtk_container_add(GTK_CONTAINER(segment_grid), stop_btn);

  /* header bar -> control segment grid -> pause button */
  capture_single_picture_btn
      = gtk_button_new_from_icon_name("camera-photo", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(capture_single_picture_btn,
                              "Capture single picture");
  g_signal_connect(capture_single_picture_btn, "clicked",
                   G_CALLBACK(capture_single_picture), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), capture_single_picture_btn);

  /* header bar -> preferences button */
  settings_btn = gtk_button_new_from_icon_name("preferences-system",
                                               GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(settings_btn, "Settings");
  g_signal_connect(settings_btn, "clicked", G_CALLBACK(open_settings_dialog),
                   NULL);
  gtk_header_bar_pack_end(GTK_HEADER_BAR(header_bar), settings_btn);

  /* main */
  main_hpaned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add(GTK_CONTAINER(window.widget), main_hpaned);

  /* aside */
  aside_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 16);
  gtk_container_set_border_width(GTK_CONTAINER(aside_vbox), 16);
  gtk_paned_pack1(GTK_PANED(main_hpaned), aside_vbox, FALSE, FALSE);

  /* aside -> file chooser segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_box_pack_start(GTK_BOX(aside_vbox), segment_grid, FALSE, FALSE, 0);

  /* aside -> file chooser segment grid -> entry */
  save_folder_entry = gtk_entry_new();
  gtk_widget_set_hexpand(save_folder_entry, TRUE);
  if (doc_cur.path)
    gtk_entry_set_text(GTK_ENTRY(save_folder_entry), doc_cur.path);
  gtk_entry_set_placeholder_text(GTK_ENTRY(save_folder_entry),
                                 "Folder to save documents in");
  gtk_widget_set_tooltip_text(save_folder_entry, "Folder to save documents in");
  gtk_container_add(GTK_CONTAINER(segment_grid), save_folder_entry);

  /* aside -> file chooser segment grid -> button */
  choose_save_folder_button
      = gtk_button_new_from_icon_name("folder-open", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text(choose_save_folder_button, "Choose a folder");
  g_signal_connect(choose_save_folder_button, "clicked",
                   G_CALLBACK(choose_save_folder), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), choose_save_folder_button);

  /* aside -> document name */
  doc_name_entry = gtk_entry_new();
  if (doc_cur.name)
    gtk_entry_set_text(GTK_ENTRY(doc_name_entry), doc_cur.name);
  gtk_entry_set_placeholder_text(GTK_ENTRY(doc_name_entry), "Document name");
  gtk_widget_set_tooltip_text(doc_name_entry, "Document name");
  gtk_box_pack_start(GTK_BOX(aside_vbox), doc_name_entry, FALSE, FALSE, 0);

  /* aside -> calendar */
  calendar = gtk_calendar_new();
  gtk_calendar_select_month(GTK_CALENDAR(calendar), doc_cur.month,
                            doc_cur.year);
  gtk_calendar_select_day(GTK_CALENDAR(calendar), doc_cur.day);
  gtk_box_pack_start(GTK_BOX(aside_vbox), calendar, FALSE, FALSE, 0);

  /* aside -> counter segment grid */
  segment_grid = gtk_grid_new();
  segment_grid_style_context = gtk_widget_get_style_context(segment_grid);
  gtk_style_context_add_class(segment_grid_style_context, "linked");
  gtk_box_pack_start(GTK_BOX(aside_vbox), segment_grid, FALSE, FALSE, 0);

  /* aside -> counter segment grid -> use counter */
  use_counter_btn = gtk_toggle_button_new_with_label("123");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_counter_btn),
                               doc_cur.use_counter);
  gtk_widget_set_tooltip_text(use_counter_btn, "Use a counter?");
  g_signal_connect(use_counter_btn, "clicked",
                   G_CALLBACK(on_use_counter_toggled), NULL);
  gtk_container_add(GTK_CONTAINER(segment_grid), use_counter_btn);

  /* aside -> counter segment grid -> count entry */
  counter_entry
      = gtk_spin_button_new_with_range(0, settings.counter_max, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(counter_entry), doc_cur.counter);
  gtk_widget_set_hexpand(counter_entry, TRUE);
  gtk_widget_set_sensitive(counter_entry, doc_cur.use_counter);
  gtk_container_add(GTK_CONTAINER(segment_grid), counter_entry);

  /* set aside min width */
  gtk_widget_set_size_request(aside_vbox,
                              gtk_widget_get_allocated_width(aside_vbox), -1);

  /* document scrolled window */
  gtk_paned_pack2(GTK_PANED(main_hpaned), create_doc_view(), TRUE, FALSE);

  /* sensitivity */
  if (settings.hist_sel >= settings.hist_length - 1)
    gtk_widget_set_sensitive(prev_btn, FALSE);
  if (doc_cur.id == settings.next_capture_id)
    set_tools_sensitivity(FALSE);

  gtk_widget_show_all(window.widget);
}

gboolean on_window_close(G_GNUC_UNUSED GtkWidget *widget,
                     G_GNUC_UNUSED GdkEvent *e,
                     G_GNUC_UNUSED gpointer data) {
  exiting = TRUE;
  if (!HW_IS_STOPPED(hw_get_state()))
    hw_stop(TRUE);
  gphoto_wait_finish();

  gtk_window_get_size(GTK_WINDOW(window.widget), &window.width, &window.height);
  g_settings_set_uint16(settings.g, "window-width", window.width);
  g_settings_set_uint16(settings.g, "window-height", window.height);

  store_metadata();
  save_history();

  g_settings_sync();

  gtk_widget_destroy(window.widget);
  return TRUE; /* this is the only handler */
}

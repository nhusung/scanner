# rCam Protocol

The rCam Protocol allows a computer to remotely control a camera and download
the captured images from it.  The protocol is based on TCP sockets and the
default server port is `1708`.

Cameras being remotely controlled can have a huge impact on privacy.
Canonically the camera would be described as server: a computer can send its
requests to the camera and receive the images on the back channel.  But if any
computer could access the camera it would be possible to spy a person out.
Therefore the implementation on the camera's side *must* ask the user for
permission.  In order for the user to identify the remote the client's IP
address should be displayed.  Furthermore the client should send a human
readable identification string.

Another important topic is TLS encryption, unfortunably there has not beed any
time to develop this feature.

In general, the protocol specifies several codes for operations.  An operation
may send additional data.  In that case it *must* be clear when the data
transmission is completed.  Version 2 specifies the following codes:

| Code | Name                 | Attributes                            | User   |
| ---- | -------------------- | ------------------------------------- | ------ |
| 0x00 | NONE                 |                                       | both   |
| 0x01 | VERSION              | `<version number>`                    | both   |
| 0x10 | SEND NAME            | `<name>`                              | client |
| 0x11 | SUPPORTED FORMATS    | `<length>` `<format>` `[…]`           | server |
| 0x1f | ACCEPT               |                                       | server |
| 0x20 | CAPTURE              | `<format>`                            | client |
| 0x2e | CAPTURE FINISHED     | `<id>` `<size>`                       | server |
| 0x2f | CAPTURE FAILED       |                                       | server |
| 0x30 | GET                  | `<id>` `<offset>` `<length>`          | client |
| 0x31 | SEND                 | `<id>` `<offset>` `<length>` `<data>` | server |
| 0x32 | DELETE RESOURCE      | `<id>`                                | client |
| 0x3f | RESOURCE UNAVAILABLE | `<id>`                                | server |

If not otherwise specified a number is unsigned and binary encoded in network
byte order.

“NONE” (`0x00`) should be ignored on both sides and its only intention is to
simplify implementations.

The only attribute of the version command `0x01` is the version number that
indicates, which operations are supported.  The version number is a natural
number coded as a byte (example: version 2 = `0x02`).  Directly after the TCP
connection is established both the client and the server send the highest
version number they support.  The minimum of these two numbers is the version
being used. Neiter the client nor the server have to support every old version
but if one does not the connection *has to* be closed.  The client as well as
the server may use this command only once.

The “SEND NAME” code `0x10` can only be sent once by a client when the
connection is not established yet.  The attribute is a UTF-8 encoded and NULL
terminated string.  It should be at least one printable character and at most
32 bytes long.  Furthermore it should not contain newlines or carriage returns.
A server can require the clients to send their name and close the connection
after a timeout if they do not.

In order to find an image format that both sides can use there is the “SUPPORTED
FORMATS” command `0x11`.  The first attribute contains how many formats are
supported, the following are the format codes.  Each format code is one byte
long. Currently the following codes are defined:

| Code | Format  |
| ---- | ------- |
| 0x00 | invalid |
| 0x01 | JPEG    |
| 0x02 | PNG     |
| 0x03 | TIFF    |
| 0x04 | HEIF    |

The server *must* send a list of its supported formats before it can accept the
connection.  The format code “invalid” is not a format itself but is inteded to
make the client side implementation easier.

If the server accepts a client it sends the accept code `0x1f`.  This command
has no attributes and can only be sent once by the server.

The capture command `0x20` invokes the camera to capture media in the format
specified by the first attribute, which again is an 8 bit integer.  Unless the
client has receives the “CAPTURE FINISHED” signal, it *must not* send another
capture command; the server should ignore that attempt.

If the capture is successfully finished, the server sends the “CAPTURE FINISHED”
signal `0x2e`.  The attributes are the resource identifier, the image format and
the filesize.  The resource identifier is four bytes long.  It does not
necessarily have to be a number and thus there is no specific byte ordering.
The image format is one of the formats from above and thus one byte long.  The
filesize is a 64 bit integer.

In case an error occured during the capture, the server sends the
“CAPTURE FAILED” error code `0x2f`.  Reasons for that could also be a
misbehavior of the client, such as requesting an unsupported format.

Using the “GET” command `0x30` the client can download a file.  The file itself
can be split into multiple parts, which should not be longer than 512 kB.  The
resource identifier is the first attribute and again four bytes long.

If the resource exists, the server should respond with the “SEND” command
`0x31`.  The first attribute is again the resource identifier, the second is the
offset and the third the length of the part, all as specified above.  In case
the client requested a region of the file that does not (completely) exist, the
part of the region which exists should be sent with the appropriate length
attribute.  The last attribute is the binary encoded data.  The server *must*
send the parts of a resource in the order they are requested.  In addition the
server *must not* send parts that are not requested.  In particular, the offsets
of request and response *have to* be equal.

In case the resource does not exist, the server should send the
“RESOURCE UNAVAILABLE” error code `0x3f` with the resource identifier.  The
second attribute is the offset in bytes as an 64 bit integer, the third the
requested length of the part, an 32 bit integer.

In a session lots of resources can be produced so it is recommended to delete
files after the complete download.  That can be done using the “DELETE RESOURCE”
command `0x32`.  The only attribute of this command is the resource identifier,
again as specified above.  If the resource has already been deleted the attempt
can be ignored.